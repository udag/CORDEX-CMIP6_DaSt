CORDEX-CMIP6 CMORisation
========================

In case of questions or problems please do not hesitate to [create an issue](https://gitlab.dkrz.de/udag/CORDEX-CMIP6_DaSt/-/issues/new) so we can offer support.


Installation and Preparation of the CMORisation
-----------------------------------------------

### General

On the DKRZ HPC levante, the correct CDO version and CMOR are installed. On other systems, we offer two ways to make use of CDO + CMOR, which are described in the following.
Apart from CDO + CMOR, there are the following prerequisites:
- [NCO's](https://nco.sourceforge.net/) `ncatted` has to be installed
- [GNU make](https://www.gnu.org/software/make/) has to be installed
- The CMORisation script is written in `ksh`, so `ksh93` in a recent version is required. On levante we use `ksh93` in version `93u+`. Type `ksh --version` in your shell to see if `ksh` is available.

### Clone the repository and retrieve CMOR tables

To clone the repository:
```
git clone https://gitlab.dkrz.de/udag/CORDEX-CMIP6_DaSt.git
```

The CORDEX-CMIP6 MIP-tables need to be initialized:
```
cd CORDEX-CMIP6_DaSt
git submodule init
git submodule update
```

Please make use of branches when making local changes and create PR (pull/merge requests) to eventually have your necessary local changes integrated into the master branch of the repository. Since we expect irregular updates of the CORDEX-CMIP6 MIP-tables and consequently updates of the CMOR scripting in this repository, please subscribe to repository updates and pull changes to your checked out files by running eg. `git pull`. 

### Installation of CDO + CMOR via conda / mamba

CDO + CMOR can be installed via `conda`/`mamba` from the `conda-forge` channel ([Link](https://conda-forge.org/)):

```
ENVNAME=cdocmor
mamba create -n $ENVNAME -c conda-forge cdo=2.4.4=hbe50dc4_0
source activate $ENVNAME
which cdo
```

The cdo-executable should be provided when submitting the CMORisation scripts (see below).

### Using docker image via udocker

We provide a docker image with the correct CDO + CMOR versions installed. It can be retrieved from levante (
`/work/bb1364/dkrz/cdocmorimage/cdocmor-env-all.tar`) or (at least temporarily) downloaded from the [DKRZ Swift Cloud](https://swift.dkrz.de/v1/dkrz_475f8922-045d-43f4-b661-0b053074407e/docker-images/cdocmor/cdocmor-env-all.tar?temp_url_sig=0a5281c794d4f99452fad0d57c44324849267d48&temp_url_expires=2025-01-03T12:28:29Z).

In the following you find the instructions to set up `udocker` to load the image and create the container. It is necessary to create the container with the name `cdocmor`. `udocker` does not need any root privileges and does not have root priviliges in the executed docker container:
- Install `udocker`
```
wget https://github.com/indigo-dc/udocker/releases/download/1.3.17/udocker-1.3.17.tar.gz
tar zxvf udocker-1.3.17.tar.gz
```
- Add the `udocker` installation directory to your `PATH` (make sure to add the command to your `~/.bashrc`, `~/.profile` or whatever file is used to initialize your terminal - [Link for further info](https://docs.dkrz.de/doc/levante/access-and-environment.html#default-login-shell)):
```
# Add to your ~/.profile to have it loaded anytime you login to levante or other HPCs
export PATH=<udocker_install_dir>/udocker-1.3.17/udocker:$PATH
```
- `udocker` stores containers etc. under `$HOME/.udocker` per default. This can quickly cause problems due to storage volume constraints in this directories on your HPC. Required are at least a few GB for the cdocmor container. So you should change the `UDOCKER_DIR` environment variable to point to a different directory. Again, add this to your `~/.profile` or similar:
```
export UDOCKER_DIR=/<my_path_to_udocker_resources>/udocker
mkdir -p $UDOCKER_DIR
chmod 700 $UDOCKER_DIR
```
- For these changes to take effect, relogin or run
```
. ~/.profile
# or
. ~/.bashrc
# whatever applies
```
- Now `load` the image - the command will return the ID of the image, which is required for the next command
```
udocker load -i cdocmor-env-all.tar

# Optionally list the udocker images to get the image ID
udocker images
```
- Finally, `create` the `cdocmor` container. This may take a few minutes. It is important that the container is given the name `cdocmor`:
```
# Replace the image ID with yours
udocker create --name=cdocmor "b76ce44ffaaaca60:latest"
```
- The container should appear when running
```
# List the containers
udocker ps
```
- Now you should be able to submit job-scripts with `jailmode` enabled, which will have the CMORisation job make use of the set up container `cdocmor` to execute CDO commands (see below).
- (Optional) If you want to peep into the container or make sure that it works, you may run:
```
udocker run cdocmor /bin/bash

# To mount local directories you can run
#  (udocker creates the container path if it does not exist,
#   so you can conveniently map /local/path to the same
#   location inside of the container)
udocker run -v /local/path:/container/path cdocmor /bin/bash

# cdo is installed under the following container path:
/opt/conda/envs/cdocmor/bin/cdo
```

Optionally check completeness of ICON-CLM Post output
-----------------------------------------------------

Before submitting the CMORisation jobs, optionally conduct checks for missing variables in the ICON-CLM SPICE yearly timeseries. The following script allows to check the SPICE output for missing parameters, the expected frequency and aggregation, and if an entire simulation year is included in each file. It does not notice however if entire simulation years are missing at the beginning or the end of the timeseries.

- display help

`bash ./cmor/scripts/submit_checktime.sh`

- complete timeseries:

```
bash ./cmor/scripts/submit_checktime.sh -t /path/to/timeseries

sbatch -t 02:00:00 --account bb1364 ./cmor/scripts/submit_checktime.sh -t /path/to/timeseries -o /path/to/put/logs
```

- single month of post output

```
bash ./cmor/scripts/submit_checktime_post.sh /path/to/post/output/YYYY_MM

sbatch -t 02:00:00 --account bb1364 ./cmor/scripts/submit_checktime_post.sh /path/to/post/output/YYYY_MM -o /path/to/put/logs
```

Conduct CMORisation
-------------------

To CMORise a simulation, a CMOR control job has to be submitted to the SLURM queue. It will then use `make` to orchestrate the diagnostic and CMORisation processes in up to 9 concurrent jobs for chunks of simulation years. These chunks are prescribed by the [CORDEX-CMIP6 Archive Specifications](https://doi.org/10.5281/zenodo.10961069). Please see below how to address this problems:

On the DKRZ HPC (`levante`) the minimum required arguments to submit a complete CMORisation job are:

```
cd ./cmor/scripts
sbatch -A <ACCOUNT> CORDEX-CMIP6.cmor_ctl -i <INSTITUTE> -e <EXPID>
```

For example:

`sbatch -A bb1364 CORDEX-CMIP6.cmor_ctl -i DWD -e IAEVALL01`

On a HPC running with PBS/qsub, this would look as follows:

`qsub [<PBS parameters>] PBS_wrapper_cmor_ctl.sh <target> -v argv="-i <institute> -e <EXPID>"`

The other arguments are pre-configured (or can be pre-configured). The full set of command line arguments includes:
```
Submit as 'sbatch [<SLURM parameters>] CORDEX-CMIP6.cmor_ctl -i <institute> -e <expid> [-m <model>] [-o <outdir>] [-r <rawdir>] [-a <iniyear>] [-z <finyear>] [-d] [-c] [-g <git_repo>] [-v <version_date>] [-x <cdo_executable>] [-n <ncatted_executable>] [-j] [-t <tasks>] [-s <suffix>] [-B]'
or alternatively as 'qsub [<PBS parameters>] PBS_wrapper_cmor_ctl.sh -v argv="-i <institute> -e <expid> ..."
  -i|--institute: Specify the institution_id excl. 'CLMcom-' prefix, eg. 'DWD'.
  -e|--expid: Specify the simulation acronym, eg. 'IAEVALL01'.
  -m|--model: Specify the model. Default is 'ICON-CLM'. Other options are 'ROAM-NBS', 'CCLM', 'CCLM-URB', 'CCLM-URB-ESG'."
  -o|--outdir: Specify the output directory excl. <expid> sub directory, eg. '/work/bb1149/ESGF_Buff/'. Will create a subfolder <expid> to which intermediate output, CMOR process information, CMORized output and logs will be written to.
  -r|--rawdir: Specify the directory holding the raw model output excl. the <expid> sub directory, eg. '/work/bb1364/production_runs/work/'. Will expect the raw model output in '<rawdir>/<expid>/post/yearly'.
  -a|--iniyear: Optional: Specify the start year of the experiment, eg. 1960 for the evaluation run. Else inferred from simulation acronym <expid>.
  -z|--finyear: Optional: Specify the final year of the experiment, eg. 2024 for the evaluation run. Else inferred from simulation acronym <expid>.
  -d|--diag: Optional - run only the diagnostics.
  -c|--cmor: Optional - run only the CMORisation + NCO fixes.
  -v|--versiondate: Optional - Specify the version_date directory in format vYYYYMMDD (eg. 'v20241206'). Will be ignored if CMORization is not run.
  -j|--jailmode: Optional - Run inside a container. Expects 'udocker' installed and the 'cdocmor' container set up (see above).
  -g|--gitrepo: Optional - Specify the location under which the CORDEX-CMIP6_DaSt git repository was cloned to. Else the default is used.
  -x|--cdo: Optional - Specify the cdo executable. Else the default is used.
  -n|--ncatted: Optional - Specify the ncatted executable. Else the default is used.
  -t|--tasks: Optional - Specify the maximum number of parallel tasks. Else it is not limited.
  -s|--suffix: Optional - Specify a suffix for the Requested_vars.conf configuration. Else, the default is used.
  -B|--rebuild: Optional - Force do-over of CMORisation of an already (partly) CMORized run.

Job scheduler:
SLURM/sbatch: run 'sbatch --help' or 'man sbatch'. Only 'QOS' and 'ACCOUNT' will be forwarded to the CMOR jobs.
PBS/qsub: None are forwarded to the CMOR jobs.
```
For each institute and each simulation, an info table (suffix `.cdocmorinfo`) has to be set up in `./cmor/scripts/it`, eg. `ICON-CLM_CORDEX-CMIP6_IAEVALL01.cdocmorinfo` and `ICON-CLM_CORDEX-CMIP6_DWD.cdocmorinfo`. If you notice, that for your simulation such a file is missing, please create an issue in this repository, so we can set one up, or set one up yourself and make a `merge request`, so it can be included in the master branch of this repository.

For other HPCs than `levante`,  the pre-configuration of `*.cmor_ctl` and other scripts may not be implemented yet. So that an easy submission of CMOR jobs will be possible there as well please open an issue for assistance.

After the CMORisation is complete, the logfiles need to be scanned for errors:
```
# Scan SLURM logs for errors
grep -i error *.log

# Optionally move the SLURM logs to the CMOR logs directory
mv *.log /path/to/ESGF_Buffer/expid/logs/

# Scan diagnostic logs for errors
grep -R ERROR /path/to/ESGF_Buffer/expid/logs/*diag*

# Scan cmor logs for errors
grep -R ERROR /path/to/ESGF_Buffer/expid/logs/*cmor_subdaily
grep -R ERROR /path/to/ESGF_Buffer/expid/logs/*cmor
```

If no errors are encountered, everything should have worked smoothly. But, since for each individual chunk of simulation years, and therein for each variable, an individual DRS directory tree is created (eg. `archive/cmor_1941-1950/ICON-CLM/mon_tas/CORDEX-CMIP6/CMIP6/.....`), all the individual directory trees need to be merged.

This can be done quickly by creating hard links:

```
cd /path/to/ESGF_Buffer/expid
cp -rl archive/cmor*/ICON-CLM/*/CORDEX-CMIP6 .
mv CORDEX-CMIP6 CORDEX
```

The last command, renaming `CORDEX-CMIP6` to `CORDEX` is necessary due to an issue in (cdo) CMOR.It uses the prefix of the CMOR tables (eg. `CORDEX-CMIP6_mon.json` -> `CORDEX-CMIP6`) as `project_id`.

Restart CMORisation
-------------------

If you want to start over with a CMORisation completely, the following steps have to be undertaken:
```
# The intermediate output has to be removed
rm -r /path/to/ESGF_Buffer/expid/work
rm -r /path/to/ESGF_Buffer/expid/processed
rm -r /path/to/ESGF_Buffer/expid/archive
rm -r /path/to/ESGF_Buffer/expid/logs

# The 'make' targets have to be removed
rm ./targets/<expid>*

# Then the CMOR control script can be resubmitted again
```

If you just want to continue a CMORisation job, you can simply re-submit the CMOR control script again. The script should only be submitted when the simulation has been completed, or else the required simulation year chunking of the CMORised output cannot be done.

### Please note

The SLURM settings are optimized for the DKRZ HPC levante. In case you conduct the CMORisation on another HPC, the nodes may not be able to handle executing that many CDO commands concurrently or the jobs may take longer time to run. In that case you might want to update the SLURM settings and [convey the information to us](https://gitlab.dkrz.de/udag/CORDEX-CMIP6_DaSt/-/issues/new), so we can set up optimal default behaviours for other HPCs as well.

Small helper scripts
--------------------

#### `./cmor/scripts/Compare.sh`

Needs to be configured for a certain simulation (update simulation year, simulation identifier / expid, tables path, the directories for source, processed and cmorized data - yet set up for levante), then can be run as:
`bash Compare.sh <CMORvar> <frequency>`, eg. `bash Compare.sh tas mon`.

The script requires X-forwarding (i.e. ssh-login via `ssh -X ...`) and the `ncview` module (on levante: `module load ncview`). It will open
- (1) the original file of the SPICE yearly timeseries,
- (2) potentially an interim result (eg. diagnostic and temporal averaged output) and
- (3) the resulting CMORised file
for a pre-configured simulation year (which should be the first year of the simulation, or else the simulation year chunking may cause no files to be found).

#### `./cmor/scripts/Check_presence_of_CMORized_variables.py`

For other HPCs than `levante`, the `tables` path needs to be adjusted in the scripts. Then can be run as:
`python Check_presence_of_CMORized_variables.py <cmor_path>`
eg.
`python Check_presence_of_CMORized_variables.py /work/bb1149/ESGF_Buff/IAEVALL00/CORDEX/CMIP6/DD/EUR-12/CLMcom-DWD/ERA5/evaluation/r1i1p1f1/ICON-CLM-202407-1-1/v0-r0`
The path needs to be specified up to and including the `version_realization` directory (eg. `v1-r1`). All variables (planned to be published) where no folder can be found will be reported. The script provides further instructions.



CORDEX-CMIP6 Quality Control (QC/QA) of CMORised output
=======================================================

The QC for CORDEX-CMIP6 within UDAG will consist of two parts:
- [IOOS compliance-checker](https://github.com/ioos/compliance-checker) plugin: [cc-plugin-cc6](https://github.com/euro-cordex/cc-plugin-cc6)
  - The [IOOS compliance-checker](https://github.com/ioos/compliance-checker) (CC) allows to check against the [Climate Forecast (CF) conventions](https://cfconventions.org/). Additional checks can be added via plugins.
  - The [cc-plugin-cc6](https://github.com/euro-cordex/cc-plugin-cc6) combines basic `CMIP/CORDEX` checks with specific checks concerning the CORDEX-CMIP6 Archiving Specifications.
  - The CC check results distinguish between `Required`, `Recommended` and `Suggested`.
  - The CC cannot aggregate test results for multiple files and thus presents test results on a file-by-file basis.
  - The plugin is WIP (work in progress) but can already be tested to some extent.
-  QC-Tool aggregating CC test results and conducting additional consistency checks on dataset or simulation level.
   - Currently in development.

#### Installation of the QC

On levante the QC is already pre-installed. Else, it can be installed in a conda-environment:
```
git clone https://github.com/euro-cordex/cc-plugin-cc6.git
cd cc-plugin-cc6
mamba create -n "<envname>" python=3.10
source activate <envname>
pip install -e .[dev]
pip install pooch
```

Check a single file
-------------------

#### Activate conda environment

On levante: `source activate /work/bb1364/dkrz/envs/ioos`

#### Run on a single file

`cchecker.py -t cc6 -t cf:latest /path/to/netCDFfile.nc`

Please provide the full path to the netCDF file, or at least the path from the `CORDEX` directory, so the checker can also consider the directory structure and naming ("DRS"). The checker will download the latest version of the CORDEX-CMIP6 cmor tables and CV to `~/.cc6_metadata`. Please note that the checker is WIP.

#### Example on levante

```
source activate /work/bb1364/dkrz/envs/ioos
cchecker.py -t cc6 -t cf:latest /work/bb1149/ESGF_Buff/IAEVALL00/CORDEX/CMIP6/DD/EUR-12/CLMcom-DWD/ERA5/evaluation/r1i1p1f1/ICON-CLM-202407-1-1/v0-r0/6hr/mrsol/v20240713/mrsol_EUR-12_ERA5_evaluation_r1i1p1f1_CLMcom-DWD_ICON-CLM-202407-1-1_v0-r0_6hr_195001010000-195012311800.nc
```

Complete QC of a simulation
---------------------------

The development did not progress far enough to make this available for testing just yet.
