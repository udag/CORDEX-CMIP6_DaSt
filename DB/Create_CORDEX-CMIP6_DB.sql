CREATE TABLE 'CMORvar' ( -- 1.3 CMOR Variable
  uid varchar(255) primary key -- Record Identifier (aa:st__uid)
, vid varchar(255)
, label varchar(2047) -- CMOR Variable Name (xs:string)
, title varchar(2047) -- Long name (xs:string)
, standardname VARCHAR(1024)
, mipTable varchar(2047) -- The MIP table: each table identifies a collection of variables (xs:string)
, outname varchar(2047) -- Name of the variable in the File
, frequency varchar(2047) -- Frequency of time steps to be archived. (xs:string)
, units varchar(255)
, positive varchar(2047) -- CMOR Directive Positive (xs:string)
, type varchar(255) -- Data value type, e.g. float or double (aa:st__fortranType)
, valid_min numeric -- Minimum expected value for this variable. (xs:float)
, valid_max numeric -- Maximum expected value for this variable. (xs:float)
, ok_min_mean_abs numeric -- Minimum expected value of the global mean absolute value at each point in time (xs:float)
, ok_max_mean_abs numeric -- Maximum expected value of the global mean absolute value at each point in time (xs:float)
, description varchar(4095) -- Description (xs:string)
, processing varchar(2047) -- Processing notes (xs:string)
, var_description varchar(2047)
, var_processing varchar(2047)
, comment varchar(2047) -- (xs:string)
, modeling_realm varchar(2047) -- Modeling Realm (xs:string)
, prov varchar(2047) -- Provenance (xs:string)
, provmip varchar(2047)
, cmt varchar(255) -- (xs:string)
, cms varchar(255) -- (xs:string)
, dims varchar(255) -- (xs:string)
, grid varchar(255) -- (xs:string)
, cdim varchar(255) -- (xs:string)
, ltier integer -- (xs:integer)
, lpriority integer -- (xs:integer)"
, zaxis varchar(255) -- (xs:string)
, reqgrid varchar(255) -- (xs:string)
, expsreq varchar(8000) -- (xs:string)
, mipsreq varchar(255) -- (xs:string)
);

--uid,vid,label,title,standardname,mipTable
--outname,frequency,units,positive,type
--val.min,val.max,min.abs,max.abs
--desc,proc,vdesc,vproc,comment,mod.realm
--prov,provmip,cmt,cms,dims,grid,cdim,tier,prio,zaxis,reqgrid,exps,mips

-- tas Amon
--INSERT OR REPLACE INTO `CMORvar` VALUES('c2tasamon',			'c2tas',		'tas',				'Near-Surface Air Temperature',					'air_temperature',			'Amon',		'tas',			'mon',		'K',		'',	'real',		170,		180,		255,		295,		'',	'normally, the temperature should be reported at the 2 meter height',		'',	'',	'',	'atmos',	'CMIP5',	'CMIP5',	'area: time: mean (Area and Time Mean)',		'area: areacella',		'time  longitude|latitude height2m',			'time/time (double, for time-mean fields) [days since ?]<br>longitude/longitude (double) [degrees_east]<br>latitude/latitude (double) [degrees_north]<br>height2m/height (double, ~2 m standard surface air temperature and surface humidity  height) [m]',				'',		1,	1,	'height2m',	'native',	'Exp0',					'CORE,EUR,AFR');

CREATE TABLE 'mip' ( -- 1.1 Model Intercomparison Project
  title varchar(2047) -- MIP title (xs:string)
, uid varchar(255) primary key -- Record identifier (aa:st__uid)
, label varchar(2047) -- MIP short name (xs:string)
, url varchar(2047)
);

CREATE TABLE 'miptable' ( -- 2.4 MIP tables
  uid varchar(255) primary key -- Record identifier (aa:st__uid)
, label varchar(2047) -- Label (xs:string)
, title varchar(2047) -- Title (xs:string)
, frequency varchar(2047) -- Frequency (xs:string)
, altLabel varchar(2047) -- Alternative Label (xs:string)
, description varchar(4095) -- Description (xs:string)
, comment varchar(2047) -- Comment (xs:string)
);

CREATE TABLE 'experiment' ( -- 1.5 Experiments
  nstart numeric -- Number of start dates (xs:integer)
, yps numeric -- Years per simulation (xs:integer)
, starty varchar(2047) -- Start year (xs:string)
, endy varchar(2047) -- End year (xs:string)
, ensz varchar(2047) -- Ensemble size (aa:st__integerListMonInc)
, ntot numeric -- Total number of years (xs:integer)
, description varchar(4095) -- Description (xs:string)
, title varchar(2047) -- Record Title (xs:string)
, label varchar(2047) -- Record Label (xs:string)
, egid varchar(2047) -- Identifier for experiment group (xs:string)
, tier varchar(2047) -- Tier of experiment (aa:st__integerListMonInc)
, mip varchar(2047) -- MIP defining experiment (xs:string)
, mcfg varchar(2047) -- Model category (xs:string)
, comment varchar(2047) -- Comment (xs:string)
, uid varchar(255) primary key -- Record identifier (aa:st__uid)
);

CREATE TABLE 'grids' ( -- 1.7 Specification of dimensions
  uid varchar(255) primary key -- Identifier (aa:st__uid)
, isGrid varchar(2047) -- grid? (xs:string)
, boundsValues varchar(2047) -- bounds _values (xs:string)
, valid_min numeric -- valid_min (xs:float)
, tolRequested varchar(2047) -- tol_on_requests: variance from requested values that is tolerated (xs:string)
, axis varchar(2047) -- axis (xs:string)
, tables varchar(2047) -- CMOR table(s) (xs:string)
, title varchar(2047) -- long name (xs:string)
, positive varchar(2047) -- positive (xs:string)
, label varchar(2047) -- CMOR dimension (xs:string)
, units varchar(2047) -- units (xs:string)
, altLabel varchar(2047) -- output dimension name (xs:string)
, type varchar(255) -- type (aa:st__fortranType)
, requested varchar(8191) -- requested (xs:string)
, direction varchar(2047) -- stored direction (xs:string)
, description varchar(4095) -- description (xs:string)
, isIndex varchar(2047) -- index axis? (xs:string)
, standardName varchar(2047) -- standard name (xs:string)
, boundsRequested varchar(2047) -- bounds_ requested (aa:st__floatList)
, bounds varchar(2047) -- bounds? (xs:string)
, value varchar(2047) -- value of a scalar coordinate (xs:string)
, coords varchar(2047) -- coords_attrib (xs:string)
, valid_max numeric -- valid_max (xs:float)
);

CREATE TABLE `CMORvar_hist` (
   `flag` integer check(flag=1 or flag=0 or flag=2),
   `changes` VARCHAR(8000),
   `curruid` VARCHAR(255),
   `cv` varchar(255),
   `dv` varchar(255),
   `uid` varchar(255),
   `label` VARCHAR(100),
   `miptable` varchar(20),
   `title` VARCHAR(255),
    primary key (uid)
);

CREATE TABLE `Dreq_Version` (
    `uid` INTEGER PRIMARY KEY CHECK (uid = 0),
    `dreq_version` VARCHAR(10)
);

