#! /usr/bin/bash

for MODEL in cclm iconclm 
do
        echo $MODEL
        sed "
                   s;MODEL;${MODEL};g
        " <createmappingdb.sql> CreateMappingModel.sql
        sqlite3 cordexcmip6db.sqlite < CreateMappingModel.sql
        rm CreateMappingModel.sql
done

