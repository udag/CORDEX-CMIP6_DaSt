Instructions to set up WebGUI database
======================================

Create cordexcmip6.sqlite database
----------------------------------

- Requirements: `python>=3.10, sqlite3, dill`

- Initialize the submodules
```
git submodules init
```

- Make sure you are in the `DB` folder
```
cd DB
```

- Create sqlite database with first tables
```
sqlite3 cordexcmip6.sqlite < Create_CORDEX-CMIP6_DB.sql
```

- Add Mapping Tables
```
bash CreateMappingTables.sh
bash AddWBListsToMapping.sh
```

- Link json-Tables of `../cordex-cmip6-cmor-tables/Tables` directory:
```
ln -s ../cordex-cmip6-cmor-tables/Tables/*.json .
```

- Run the python script to add all MIP variables to the database
```
python Read_CMORvars_xlsx_csv_json.py
```

- Run the python script to create CSV files out of the JSON files for the QC
```
python Create_QC_csv_files.py # only required for QA-DKRZ - so likely not for CORDEX-CMIP6
```

- Initialize the Version Mapping in case of future updates of the MIP tables
```
pip install dill -t . # if dill is not installed
python Version_Mapping_DB.py
```

Update the cordexcmip6.sqlite database
--------------------------------
### after a new version of the CMOR tables releases

- Go to the subfolder for the `cordex-cmip6-cmor-tables` github repository
```
cd cordex-cmip6-cmor-tables
```

- Update the Tables
```
git pull
```


- Go to the "DB" directory
```
cd ../DB
```

- Delete old data from tables
```
sqlite3 cordexcmip6.sqlite
> delete from CMORvar;
> delete from experiment;
> delete from grids;
> delete from mip;
> delete from miptable;
> .quit
```

- Load new data into the database
 (make sure all json files are listed in the python script)
```
python Read_CMORvars_xlsx_csv_json.py
```

- Update attributes in mapping tables
```
python DB_Update_Attributes.py
```

- Or: Create a Version Mapping between the two table versions and update the attributes in the mapping tables
```
pip install dill -t . # if dill is not yet installed
python Version_Mapping_DB.py
```

SQLITE commands
---------------
### For use with the database `cordexcmip6.sqlite`

- Open database:
```
sqlite3 cordexcmip6.sqlite
```

- List all tables
```
> .tables
```

- List table info
```
> .schema tablename
```

- Print content
```
> SELECT * FROM tablename;
```

- Copy mapping information from a different database - example CCLM with COSMO-Mapping-Info from OcMOD:
```
ATTACH DATABASE `ocmoddb.sqlite` AS fromdb;

INSERT INTO  CMORvar_Mapping_cosmo(recipe, flag, flagby, flagt, flagh, flaguid, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, zaxis, positive, dec_check, availability, uid, frequency_mapped, aggregation_mapped) SELECT recipe, flag, flagby, flagt, flagh, flaguid, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, zaxis, positive, dec_check, availability, uid, frequency_mapped, aggregation_mapped FROM fromdb.CMORvar_Mapping_cclm;
```

- Help
```
.help
```

- Exit
```
.quit
```
