#!/usr/bin/python
# -*- coding: utf-8 -*

###################################
#
# Script to read CCLM2CMOR Config
# csv file with mapping information
# and adopt entries for the
# VarMapping WebGUI database.
#
# Specifically to adopt the ICON-CLM
# CCLM2CMOR Mapping for CORDEX-CMIP6
#
###################################
# Martin Schupfner (DKRZ-DM), 2023
###################################

import csv, os, sys, re, datetime
import sqlite3 as lite

###################
# Parameters
###################
csvfile="CORDEX6_CMOR_CCLM_variables_table.csv" # csv file with CCLM2CMOR Config
model="iconclm"                                 # Model label in WebGUI database
db="cordexcmip6db.sqlite"                       # Web GUI database
User="Import (CCLM2CMOR)"                       # Write to Web GUI database as <User>
VN="01.00.00"                                   # Data Request Version
overwrite=False                                 # Overwrite existing mapping information
diagnostics=False                               # Whether SPICE or WebGUI will be used to conduct diagnostics (TOT_PREC etc.)


###################
# Read csv file
###################
if not os.path.isfile(csvfile):
    raise FileNotFoundError(f"File '{csvfile}' cannot be found.")
header=1
numline=0
csvlist=list()
numcolumns=30
with open(csvfile, 'r') as incsv:
    content=csv.reader(incsv, delimiter=";", quotechar='"')
    for line in content:
        numline+=1
        if numline<=header: continue # skip header lines
        if line == []: continue      # skip empty lines
        if len(line) != numcolumns:
            print(f"Invalid field count in line {numline}: {len(line)} columns instead of {numcolumns}.")
            print("; ".join(line))
            sys.exit(1)
        csvlist.append(line)
print()
print(len(csvlist), "variables read.")
print()



###################
# Read database CMORvar
###################
print("Using SQLITE in version", lite.sqlite_version)
print("\nLoading Database ...")



try:
    con = lite.connect(db)
    cur = con.cursor()
    cur.execute('SELECT * FROM CMORvar')
    data_cmorvars=cur.fetchall()
except lite.Error as e:
    print("ERROR %s:" % e.args[0])
    sys.exit(1)

# Add to dict()
CMORvar=dict()
for cmv in data_cmorvars:
    f=cmv[7]
    on=cmv[6]
    if not on in CMORvar: CMORvar[on]=dict()
    if not f in CMORvar[on]: CMORvar[on][f]=dict()
    CMORvar[on][f]["standardname"]=cmv[4]
    CMORvar[on][f]["shortname"]=on
    CMORvar[on][f]["miptable"]=f
    CMORvar[on][f]["cell_methods"]=cmv[23]
    CMORvar[on][f]["frequency"]=f
    CMORvar[on][f]["zaxis"]=cmv[30]
    CMORvar[on][f]["uid"]=cmv[0]
    CMORvar[on][f]["positive"]=cmv[9]
    CMORvar[on][f]["units"]=cmv[8]

"""
'CMORvar' -- CMOR Variable
0  uid varchar(255) primary key -- Record Identifier (aa:st__uid)
1, vid varchar(255)
2, label varchar(2047) -- CMOR Variable Name (xs:string)
3, title varchar(2047) -- Long name (xs:string)
4, standardname VARCHAR(1024)
5, mipTable varchar(2047) -- The MIP table: each table identifies a collection of variables (xs:string)
6, outname varchar(2047) -- Name of the variable in the File
7, frequency varchar(2047) -- Frequency of time steps to be archived. (xs:string)
8, units varchar(255)
9, positive varchar(2047) -- CMOR Directive Positive (xs:string)
10, type varchar(255) -- Data value type, e.g. float or double (aa:st__fortranType)
11, valid_min numeric -- Minimum expected value for this variable. (xs:float)
12, valid_max numeric -- Maximum expected value for this variable. (xs:float)
13, ok_min_mean_abs numeric -- Minimum expected value of the global mean absolute value at each point in time (xs:float)
14, ok_max_mean_abs numeric -- Maximum expected value of the global mean absolute value at each point in time (xs:float)
15, description varchar(4095) -- Description (xs:string)
16, processing varchar(2047) -- Processing notes (xs:string)
17, var_description varchar(2047)
18, var_processing varchar(2047)
19, comment varchar(2047) -- (xs:string)
20, modeling_realm varchar(2047) -- Modeling Realm (xs:string)
21, prov varchar(2047) -- Provenance (xs:string)
22, provmip varchar(2047)
23, cmt varchar(255) -- (xs:string)
24, cms varchar(255) -- (xs:string)
25, dims varchar(255) -- (xs:string)
26, grid varchar(255) -- (xs:string)
27, cdim varchar(255) -- (xs:string)
28, ltier integer -- (xs:integer)
29, lpriority integer -- (xs:integer)"
30, zaxis varchar(255) -- (xs:string)
31, reqgrid varchar(255) -- (xs:string)
32, expsreq varchar(8000) -- (xs:string)
33, mipsreq varchar(255) -- (xs:string)

CMORvar_Mapping_iconclm -- Mapping table
0   `recipe` VARCHAR( 255 ),
1   `flag` integer check(flag=1 or flag=0 or flag=2 or flag=3),
2   `flagby` varchar( 100 ),
3   `flagt` timestamp,
4   `flagh` integer check(flagh=1 or flagh=0),
5   `flaguid`varchar(255),
6   `modvarname` VARCHAR(500),
7   `standardname` VARCHAR(255),
8   `shortname` VARCHAR(100),
9   `miptable` varchar(20),
10   `cell_methods` varchar(100),
11   `modvarcode` varchar(500),
12   `modvarunits` VARCHAR(20),
13   `comment` varchar( 511 ),
14   `note` varchar( 511 ),
15   `username` varchar( 100 ),
16   `changed` timestamp,
17   `frequency` varchar( 100 ),
18   `chardim` varchar( 100 ),
19   `zaxis` varchar ( 255 ),
20   `positive` string check(positive="up" or positive="down" or positive=NULL or positive=0),
21   `dec_check` varchar ( 50 ),
22   `availability` integer check(availability=-1 or availability=1 or availability=NULL or availability=0),
23   `uid`varchar(255),
    FOREIGN KEY(uid) references CMORvar(uid)
    primary key (uid)

0 CCLM variable name;
1 CCLM variable name (in model output file);
2 output variable name;
3 to be derived from;
4 Derotate (CORDEX);
5 Level;
6 Conversion factor 1);
7 Cell Method (additional subdaily,CORDEX);
8 Cell Method (subdaily,CORDEX);
9 Cell Method (daily,CORDEX);
10 Cell Method (monthly,CORDEX);
11 Cell Method (seasonal,CORDEX);
12 Levelvalue (height, plev);
13 units;
14 Additional subdaily frq [1/day];
15 Subdaily frq [1/day];
16 ag;
17 Daily frq [1/day];
18 ag;
19 Monthly frq [1/mon];
20 ag;
21Seasonal frq [1/sem];
22 ag;
23 fx;
24 long_name;
25 comment;
26 standard_name;
27 direction of positive fluxes;
28 realm (not required, however, if included should have the value as in CMIP5);
29 cell-method: area (optional)

#T_2M;T_2M;tas;;;ModelLevel;1;;point;mean;mean;;2;K;;24;i;1;;1;;;;;Near-Surface Air Temperature;;air_temperature;;atmos;
  0     1   2       5       6    8    9    10  12 13 15 16 17 19      24                               26          28
"""

exprops=['isMissval', 'abs', 'acos', 'asin', 'atan', 'atan2', 'cdate', 'cday', 'cdeltat', 'ceil', 'chour', 'clat', 'clev', 'clon', 'cminute', 'cmonth', 'cos', 'csecond', 'ctime', 'ctimestep', 'cyear', 'deg', 'exp', 'fldavg', 'fldmax', 'fldmean', 'fldmin', 'fldstd', 'fldstd1', 'fldsum', 'fldvar', 'fldvar1', 'float', 'floor', 'gridarea', 'int', 'ln', 'log10', 'max', 'min', 'missval', 'ngp', 'nint', 'nlev', 'rad', "sellevel", "sellevidx", "remove","sellevelrange","sellevidxrange", 'sin', 'size', 'sqr', 'sqrt', 'tan', 'vertavg', 'vertmax', 'vertmean', 'vertmin', 'vertstd', 'vertstd1', 'vertsum', 'vertvar', 'vertvar1','zonmin', 'zonmax', 'zonmean', 'zonsum', 'zonavg','zonstd','zonstd1','zonvar','zonvar1']
def RepresentsNumber(s):
    "Does this string represent a number (eg. 5, 2, 2e-5)."
    try:
        float(s)
        return True
    except ValueError:
        return False

def translate_frequency(f):
    "Translate table entry into subdaily frequency or return False if not requested."
    f=f.strip()
    if f=="": return False
    elif f=="4": return "6hr"
    elif f=="8": return "3hr"
    elif f=="24": return "1hr"
    else:
        raise ValueError(f"ERROR: Frequency '{f}' is not supported.")

NotFoundCounter=0
FoundCounter=0
found=list()
mappingwarnings=list()
phptime=int((datetime.datetime.now() - datetime.datetime(1970,1,1)).total_seconds())
pattern = re.compile(r'\s+') # remove whitespace of <string>: re.sub(pattern, '', <string>)
for entry in csvlist:
    modvarnamenew=""
    shortname   = entry[2].strip()
    found.append(shortname)
    modvarname  = f"timeseries/{entry[0].strip()}/{entry[0].strip()}_DATE*.*|{entry[1].strip()}"
    modvarunits = entry[13].strip()
    standardname= entry[26].strip()
    positive    = entry[27].strip()
    changed     = phptime

    # Recipe Part 1 - factor
    recipe=""
    factor=entry[6].strip()
    if factor=="" or not RepresentsNumber(factor):
        raise ValueError(f"ERROR: {shortname} has illegal multiplicator: '{factor}'")
    factor=float(factor)

    # Frequencies
    frequencies = list()
    frequency = False
    if entry[23].strip()=="0":
        fx=True
    else:
        fx = False
        subdaily  = translate_frequency(entry[15])
        subdaily2 = translate_frequency(entry[14])
        daily = entry[17].strip() == "1"
        monthly = entry[19].strip() == "1"
        seasonal = entry[21].strip() == "1"
        agg = entry[16].strip()

    if fx:
        frequency="fx"
        frequencies.append("fx")
    else:
        if subdaily:
            frequencies.append(subdaily)
            if not frequency: frequency = subdaily
        if subdaily2:
            frequencies.append(subdaily2)
            if not frequency: frequendy = subdaily2
        if subdaily and subdaily2: print("WARNING: Two subdaily frequencies specified:", subdaily, subdaily2)
        if subdaily2 and not subdaily: print("WARNING: Additional subdaily frequency but no subdaily frequency defined:", subdaily2, subdaily, shortname)
        if daily: frequencies.append("day")
        if monthly: frequencies.append("mon")
        if seasonal:
            raise ValueError(f"ERROR: Seasonal averages are not supported (affected variable: {shortname} (entry[1])).")
        if agg not in ["i", "a"]:
            raise ValueError(f"ERROR: Variable {shortname} ({entry[1]}) has illegal aggregation '{agg}'.")

    if frequency=="fx":
        agg_mapped=""
    elif agg=="i":
        agg_mapped="point"
    elif (agg=="a" and (factor<1 and factor>0  or factor>-1 and factor<0)) or entry[0] == "DURSUN":
        agg_mapped="sum"
    elif agg=="a" and "max" in entry[9]:
        agg_mapped="max"
    elif agg=="a" and "min" in entry[9]:
        agg_mapped="min"
    elif agg=="a":
        agg_mapped="mean"
    else:
        raise ValueError(f"ERROR: Mapped aggregation could not be determined (affected variable: {shortname} (entry[1])).")

    if frequency=="fx":
        lfreq=""
    else:
        lfreq=frequencies[0]

    if agg_mapped in ["min", "max", "sum"]:
        print(f"--- {agg_mapped}: {shortname} - {factor} - {entry[1]}")


    # Recipe part 2 -Get note and recipe, combine recipe and factor
    note        = ""
    comment=entry[25].strip()
    derived=entry[3].strip()
    if derived!="":
        if any([expression in derived for expression in ["model-level", "R-script"]]):
            note=derived
        elif any([op in derived for op in ["+", "-", "*", "/"]]) and diagnostics:
            recipe=f"{entry[1].strip()}="+re.sub(pattern, '', derived)
            vars=[x for x in re.findall(r"[\w']+", derived) if not RepresentsNumber(x) and not x in exprops]
            modvarnamenew=f"timeseries/{vars[0]}/{vars[0]}_DATE*.*|{vars[0]}"
            for var in vars[1:]:
                modvarnamenew+=f";timeseries/{var}/{var}_DATE*.*|{var}"
        elif derived.startswith("Sum("):
            k1="1"
            k2=entry[12].strip()
            var=entry[1].strip()
            recipe=f"vertsum(sellevidxrange({var},{k1},{k2}))"
        elif "(1:" in derived:
            k1=1
            k2=entry[12].strip()
            var=entry[1].strip()
            recipe=f"sellevidxrange({var},{k1},{k2})"
        #elif "daymax" in derived:
        #    recipe="CDOOP_daymax|"
        else:
            note = derived
    if factor!=1:
        recipeparts=recipe.split("|")
        if recipeparts[-1].strip()!="":
            recipeparts[-1]=str(factor)+"*"+recipeparts[-1].strip()
        else: recipeparts[-1]=str(factor)+"*"+entry[1].strip()
        recipe="|".join(recipeparts)
        print(f"  Recipe for {shortname} / {entry[1]}: '{recipe}'")

    if comment!="": print(shortname,"- comment:", comment)

    if modvarnamenew!="" and modvarnamenew!=modvarname:
        print(f"WARNING {shortname}: Altering modvarname from '{modvarname}' to '{modvarnamenew}' because of the following recipe:")
        print(f"     {derived}")
        print(f"     {recipe}")
        print()
        modvarname=modvarnamenew

    if shortname not in CMORvar:
        NotFoundCounter+=1
        print(f"WARNING {NotFoundCounter}: {shortname} cannot be found in the current Dreq Version! Affected frequencies: {', '.join(frequencies)}.")
        continue
        #print(", ".join(entry))
        #print()
        #cur.execute("INSERT INTO CMORvar_Mapping_"+model+"_delete (recipe, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid, id, tid, zaxis) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [recipe, modvarname, standardname, shortname, miptable,"", modvarcode, modvarunits,  "", note, username, changed, frequency, "", positive, availability, uid, str(uid)+str(phptime), VN+"_"+str(uid)+"_"+str(phptime), ""])
    else:
        FoundCounter+=1
        #print(f"+ {FoundCounter}: {shortname} ({', '.join([fi for fi in sorted(CMORvar[shortname].keys())])})")
        for f in frequencies:
            if f not in CMORvar[shortname]:
                print(f"WARNING: {f}_{shortname} cannot be found in the current Dreq Version (mapped for frequencies {', '.join(frequencies)}).")
        for f in CMORvar[shortname].keys():
            if not f in frequencies:
                mappingwarnings.append(f"WARNING for {shortname}: frequency {f} not part of Mapping (mapped only for frequencies {', '.join(frequencies)}).")

    for f in frequencies:
        if f in CMORvar[shortname]:
            cur.execute("INSERT INTO CMORvar_Mapping_"+model+" (recipe, modvarname, standardname, shortname, miptable, cell_methods, comment, modvarcode, modvarunits, note, username, changed, positive, availability, uid, frequency, frequency_mapped, aggregation_mapped) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        [recipe, modvarname, CMORvar[shortname][f]["standardname"], shortname,
                         CMORvar[shortname][f]["miptable"], CMORvar[shortname][f]["cell_methods"], comment, "", modvarunits, note, User, phptime, positive, "1",
                         CMORvar[shortname][f]["uid"], f, lfreq, agg_mapped])
            if CMORvar[shortname][f]["standardname"]!=standardname:
                print(f"WARNING: deviating standardnames for '{shortname}': {standardname} != {CMORvar[shortname][f]['standardname']}")
            if CMORvar[shortname][f]["units"]!=modvarunits:
                print(f"WARNING: deviating units for '{shortname}': {modvarunits} != {CMORvar[shortname][f]['units']}")
            if CMORvar[shortname][f]["positive"]!=positive:
                print(f"WARNING: deviating 'positive' for '{shortname}': {positive} != {CMORvar[shortname][f]['positive']}")
            if (agg_mapped in ["point", "max", "min"] and "time: mean" in CMORvar[shortname][f]["cell_methods"] and not "time: mean over" in CMORvar[shortname][f]["cell_methods"]) or (agg_mapped in ["mean", "sum"] and any([cmtx in CMORvar[shortname][f]["cell_methods"] for cmtx in ["max", "min", "point"]]) ):
                print(f"WARNING: deviating aggregation {shortname}_{f}: {lfreq} {agg_mapped} provided, {CMORvar[shortname][f]['cell_methods']} requested.")


con.commit()

for warning in sorted(mappingwarnings): print(warning)
for var in sorted(CMORvar.keys()):
    if not var in found:
        print(f"WARNING: Variable not part of Mapping: '{var}'")

print("\nDone.")
sys.exit(0)
