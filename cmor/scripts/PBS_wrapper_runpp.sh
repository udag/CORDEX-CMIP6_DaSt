#PBS -q rc_big
#PBS -l memsz_job=128gb
#PBS -l vmemsz_job=128gb
#PBS -j o
###PBS -l place=excl
###PBS -o $(JOB_NAME)_%J_%I.log
#PBS -l elapstim_req=06:00:00
###PBS -l walltime=06:00:00

cd $PBS_O_WORKDIR

ksh CORDEX-CMIP6.runpp $argv || echo ERROR running runpp script with arguments $argv
