#!/bin/bash

#
# Correct vertical hybrid-height axis
#   for 3D COSMO output rewritten with
#   cdo cmor
# - temporary workaround -
#

ifile=$1
ofile=$1_mod

# 8 uppermost levels
lev=0.005,0.025,0.07,0.16,0.34,0.7,1.42,2.86
lev_bnds_0=0.,0.01,0.04,0.1,0.22,0.46,0.94,1.9
lev_bnds_1=0.01,0.04,0.1,0.22,0.46,0.94,1.9,3.82

# all 10 levels
#lev=0.005,0.025,0.07,0.16,0.34,0.7,1.42,2.86,5.74,11.5
#lev_bnds_0=0.,0.01,0.04,0.1,0.22,0.46,0.94,1.9,3.82,7.66
#lev_bnds_1=0.01,0.04,0.1,0.22,0.46,0.94,1.9,3.82,7.66,15.34

add_bounds() {
  echo "$ifile"
  [[ -e ${ifile}.old ]] && {
    echo "$ifile already processed ..."
    exit 0
  }
  echo "Setting depth and depth_bnds ..."
  ncap2 -A -h -s "depth(:)={$lev};depth_bnds(:,0)={$lev_bnds_0};depth_bnds(:,1)={$lev_bnds_1};" $ifile $ofile && mv $ifile ${ifile}.old && mv $ofile $ifile || echo "ERROR: Overwriting z-axis coefficients failed!"
}

add_bounds

