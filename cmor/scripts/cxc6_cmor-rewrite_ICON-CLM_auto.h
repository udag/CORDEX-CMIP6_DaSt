#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='cin evspsbl evspsblpot mrfsos mrsos pr prc prsn'
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  ifile=${sdir}/out_diag/1hr_${var}_$period.nc
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='clt'
find_file "$sdir/ACLCT" "ACLCT_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='hfls'
find_file "$sdir/ALHFL_S" "ALHFL_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='hfss'
find_file "$sdir/ASHFL_S" "ASHFL_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rsus'
find_file "$sdir/ASODIFU_S" "ASODIFU_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rsdsdir'
find_file "$sdir/ASODIRD_S" "ASODIRD_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rsds'
find_file "$sdir/ASOD_S" "ASOD_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rsdt'
find_file "$sdir/ASOD_T" "ASOD_T_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rsut'
find_file "$sdir/ASOU_T" "ASOU_T_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rlut'
find_file "$sdir/ATHB_T" "ATHB_T_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rlds'
find_file "$sdir/ATHD_S" "ATHD_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='rlus'
find_file "$sdir/ATHU_S" "ATHU_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='tauu'
find_file "$sdir/AUMFL_S" "AUMFL_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='tauv'
find_file "$sdir/AVMFL_S" "AVMFL_S_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='cape'
find_file "$sdir/CAPE_ML" "CAPE_ML_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='zmla'
find_file "$sdir/HPBL" "HPBL_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='psl'
find_file "$sdir/PMSL" "PMSL_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ps'
find_file "$sdir/PS" "PS_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='hus50m'
find_file "$sdir/QV50z" "QV50z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='huss'
find_file "$sdir/QV_2M" "QV_2M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='hurs'
find_file "$sdir/RELHUM_2M" "RELHUM_2M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='li'
find_file "$sdir/SLI" "SLI_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='sfcWind'
find_file "$sdir/SP_10M" "SP_10M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ta50m'
find_file "$sdir/T50z" "T50z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='clivi'
find_file "$sdir/TQI" "TQI_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='prw'
find_file "$sdir/TQV" "TQV_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='clwvi'
find_file "$sdir/TQW" "TQW_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='tas'
find_file "$sdir/T_2M" "T_2M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ts'
find_file "$sdir/T_G" "T_G_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua100m'
find_file "$sdir/U100z" "U100z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua150m'
find_file "$sdir/U150z" "U150z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua200m'
find_file "$sdir/U200z" "U200z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua250m'
find_file "$sdir/U250z" "U250z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua300m'
find_file "$sdir/U300z" "U300z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='ua50m'
find_file "$sdir/U50z" "U50z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='uas'
find_file "$sdir/U_10M" "U_10M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va100m'
find_file "$sdir/V100z" "V100z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va150m'
find_file "$sdir/V150z" "V150z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va200m'
find_file "$sdir/V200z" "V200z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va250m'
find_file "$sdir/V250z" "V250z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va300m'
find_file "$sdir/V300z" "V300z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='va50m'
find_file "$sdir/V50z" "V50z_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 1hr
cn='vas'
find_file "$sdir/V_10M" "V_10M_${period}*.*" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.1hr 2>>$err.$var.1hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus925 mrfso mrro mrros mrsfl mrso mrsol rldscs rluscs rlutcs rsdscs rsuscs rsutcs snc snd snm snw ta925 tsl ua925 va925 wa925 zg1000 zg200 zg250 zg300 zg400 zg500 zg600 zg700 zg750 zg850 zg925'
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  ifile=${sdir}/out_diag/6hr_${var}_$period.nc
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='clh'
find_file "$sdir/ACLCH" "ACLCH_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='cll'
find_file "$sdir/ACLCL" "ACLCL_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='clm'
find_file "$sdir/ACLCM" "ACLCM_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus1000'
find_file "$sdir/QV1000p" "QV1000p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus200'
find_file "$sdir/QV200p" "QV200p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus250'
find_file "$sdir/QV250p" "QV250p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus300'
find_file "$sdir/QV300p" "QV300p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus400'
find_file "$sdir/QV400p" "QV400p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus500'
find_file "$sdir/QV500p" "QV500p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus600'
find_file "$sdir/QV600p" "QV600p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus700'
find_file "$sdir/QV700p" "QV700p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus750'
find_file "$sdir/QV750p" "QV750p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='hus850'
find_file "$sdir/QV850p" "QV850p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta1000'
find_file "$sdir/T1000p" "T1000p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta200'
find_file "$sdir/T200p" "T200p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta250'
find_file "$sdir/T250p" "T250p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta300'
find_file "$sdir/T300p" "T300p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta400'
find_file "$sdir/T400p" "T400p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta500'
find_file "$sdir/T500p" "T500p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta600'
find_file "$sdir/T600p" "T600p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta700'
find_file "$sdir/T700p" "T700p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta750'
find_file "$sdir/T750p" "T750p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ta850'
find_file "$sdir/T850p" "T850p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua1000'
find_file "$sdir/U1000p" "U1000p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua200'
find_file "$sdir/U200p" "U200p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua250'
find_file "$sdir/U250p" "U250p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua300'
find_file "$sdir/U300p" "U300p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua400'
find_file "$sdir/U400p" "U400p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua500'
find_file "$sdir/U500p" "U500p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua600'
find_file "$sdir/U600p" "U600p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua700'
find_file "$sdir/U700p" "U700p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua750'
find_file "$sdir/U750p" "U750p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='ua850'
find_file "$sdir/U850p" "U850p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va1000'
find_file "$sdir/V1000p" "V1000p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va200'
find_file "$sdir/V200p" "V200p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va250'
find_file "$sdir/V250p" "V250p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va300'
find_file "$sdir/V300p" "V300p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va400'
find_file "$sdir/V400p" "V400p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va500'
find_file "$sdir/V500p" "V500p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va600'
find_file "$sdir/V600p" "V600p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va700'
find_file "$sdir/V700p" "V700p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va750'
find_file "$sdir/V750p" "V750p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='va850'
find_file "$sdir/V850p" "V850p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa1000'
find_file "$sdir/W1000p" "W1000p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa200'
find_file "$sdir/W200p" "W200p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa250'
find_file "$sdir/W250p" "W250p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa300'
find_file "$sdir/W300p" "W300p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa400'
find_file "$sdir/W400p" "W400p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa500'
find_file "$sdir/W500p" "W500p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa600'
find_file "$sdir/W600p" "W600p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa700'
find_file "$sdir/W700p" "W700p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa750'
find_file "$sdir/W750p" "W750p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) 6hr
cn='wa850'
find_file "$sdir/W850p" "W850p_${period}*.*" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $rcmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},om=r,vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.6hr 2>>$err.$var.6hr
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='cape cin cinmax clh clivi cll clm clt clwvi evspsbl evspsblpot hfls hfss hurs hus1000 hus200 hus250 hus300 hus400 hus500 hus50m hus600 hus700 hus750 hus850 hus925 huss li mrfso mrfsos mrro mrros mrsfl mrso mrsol mrsos pr prc prhmax prsn prw ps psl rlds rldscs rlus rluscs rlut rlutcs rsds rsdscs rsdsdir rsdt rsus rsuscs rsut rsutcs sfcWind siconca snc snd snm snw ta1000 ta200 ta250 ta300 ta400 ta500 ta50m ta600 ta700 ta750 ta850 ta925 tas tauu tauv ts tsl ua1000 ua100m ua150m ua200 ua200m ua250 ua250m ua300 ua300m ua400 ua500 ua50m ua600 ua700 ua750 ua850 ua925 uas va1000 va100m va150m va200 va200m va250 va250m va300 va300m va400 va500 va50m va600 va700 va750 va850 va925 vas wa1000 wa200 wa250 wa300 wa400 wa500 wa600 wa700 wa750 wa850 wa925 zg1000 zg200 zg250 zg300 zg400 zg500 zg600 zg700 zg750 zg850 zg925 zmla'
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  ifile=${sdir}/out_diag/day_${var}_$period.nc
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='z0'
find_file "$sdir/AZ0" "AZ0_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='capemax'
find_file "$sdir/CAPE_MLMAX" "CAPE_MLMAX_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='sund'
find_file "$sdir/DURSUN" "DURSUN_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='limax'
find_file "$sdir/SLIMAX" "SLIMAX_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='wsgsmax'
find_file "$sdir/SPGUST_10M" "SPGUST_10M_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='sfcWindmax'
find_file "$sdir/SPMAX_10M" "SPMAX_10M_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='tasmax'
find_file "$sdir/TMAX_2M" "TMAX_2M_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) day
cn='tasmin'
find_file "$sdir/TMIN_2M" "TMIN_2M_${period}*.*" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $rcmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},om=${om_day},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.day 2>>$err.$var.day
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) fx
cn='areacella mrsofc rootd'
for var in $cn; do
  { (if_requested $member $rcmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.fx 2>>$err.$var.fx
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) fx
cn='sftgif sftlaf sfturf'
find_file "$sdir" "${EXPID}_c.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $rcmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.fx 2>>$err.$var.fx
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) fx
cn='sftlf'
find_file "$sdir/FR_LAND" "FR_LAND.*" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $rcmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.fx 2>>$err.$var.fx
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) fx
cn='orog'
find_file "$sdir/HSURF" "HSURF.*" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $rcmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.fx 2>>$err.$var.fx
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

#-- CMOR-rewrite for icon-clm (ESM: ICON-CLM) mon
cn='cape capemax cin cinmax clh clivi cll clm clt clwvi evspsbl evspsblpot hfls hfss hurs hus1000 hus200 hus250 hus300 hus400 hus500 hus50m hus600 hus700 hus750 hus850 hus925 huss li limax mrfso mrfsos mrro mrros mrsfl mrso mrsol mrsos od550aer pr prc prsn prw ps psl rlds rldscs rlus rluscs rlut rlutcs rsds rsdscs rsdsdir rsdt rsus rsuscs rsut rsutcs sfcWind sfcWindmax siconca snc snd snm snw sund ta1000 ta200 ta250 ta300 ta400 ta500 ta50m ta600 ta700 ta750 ta850 ta925 tas tasmax tasmin tauu tauv ts tsl ua1000 ua100m ua150m ua200 ua200m ua250 ua250m ua300 ua300m ua400 ua500 ua50m ua600 ua700 ua750 ua850 ua925 uas va1000 va100m va150m va200 va200m va250 va250m va300 va300m va400 va500 va50m va600 va700 va750 va850 va925 vas wa1000 wa200 wa250 wa300 wa400 wa500 wa600 wa700 wa750 wa850 wa925 wsgsmax z0 zg1000 zg200 zg250 zg300 zg400 zg500 zg600 zg700 zg750 zg850 zg925 zmla'
for var in $cn; do
  { (if_requested $member $rcmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  ifile=${sdir}/out_diag/mon_${var}_$period.nc
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},om=${om_mon},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},om=${om_mon},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR >&2
  )&; }>>$log.$var.mon 2>>$err.$var.mon
  if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
    wait
  fi
done

