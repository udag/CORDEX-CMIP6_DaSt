#!/bin/bash
#SBATCH -J checktime         # Specify job name
#SBATCH -p compute
#SBATCH --mem=250G
#SBATCH -N 1                 # Specify number of nodes
#SBATCH -t 08:00:00          # Set a limit on the total run time
#SBATCH -A bb1364            # Charge resources on this project account
####SBATCH -A bm0021
####SBATCH --qos=esgf
#SBATCH -o checktime.log     # File name for standard output
#SBATCH -e checktime.log     # File name for standard error output

# Default Experiment-ID
EXPID=C2I250c_f_rc

# List of variables and their frequency to be checked
#  Configured for CORDEX-CMIP6 contribution of UDAG
#  (Does not include fx variables)
declare -A vars

# Fully mapped variables
vars["ASWFLX_DN_CS"]="6hr mean" # only lvl 60 (SFC) required
vars["ALWFLX_DN_CS"]="6hr mean" # only lvl 60 (SFC) required
vars["ASWFLX_UP_CS"]="6hr mean" # lvl 1 (TOA) and 60 (SFC) required
vars["ALWFLX_UP_CS"]="6hr mean" # lvl 1 (TOA) and 60 (SFC) required
vars["AAOD_550NM"]="day mean" # mon mean required # add comment for WL band 442 625nm
vars["AFR_SEAICE"]="day mean" # possibly 1hr for 3km simulations
vars["SLI"]="1hr point"
vars["SLIMAX"]="day maximum"
vars["APOTEVAP_S"]="1hr sum"
vars["AZ0"]="day mean"
vars["FR_LAKE-${EXPID}_c"]="fx fx"
vars["FR_GLAC-${EXPID}_c"]="fx fx"
vars["FIELDCAP-${EXPID}_c"]="fx fx"
vars["FR_URB-${EXPID}_c"]="fx fx"
vars["ROOTDP-${EXPID}_c"]="fx fx"
vars["HSURF-${EXPID}_c"]="fx fx"
vars["FR_LAND-${EXPID}_c"]="fx fx"
#vars["ASOB_S"]="1hr mean" # required only to calculate ASOD_S = ASOB_S + ASODIFU_S
#vars["ASOB_T"]="1hr mean" # required only to calculate ASOU_T = ASOD_T - ASOB_T
vars["ASODIFU_S"]="1hr mean"
vars["ASODIRD_S"]="1hr mean"
vars["ASOD_S"]="1hr mean"
vars["ASOD_T"]="1hr mean"
vars["ASOU_T"]="1hr mean"
#vars["ATHB_S"]="1hr mean" # required only to calculate ATHD_S = ATHB_S + ATHU_S
vars["ATHB_T"]="1hr mean"
vars["ATHD_S"]="1hr mean"
vars["ATHU_S"]="1hr mean"
vars["ACLCH"]="6hr mean"
vars["ACLCL"]="6hr mean"
vars["ACLCM"]="6hr mean"
vars["ACLCT"]="1hr mean"
vars["AEVAP_S"]="1hr sum"
vars["ALHFL_S"]="1hr mean"
vars["ASHFL_S"]="1hr mean"
vars["AUMFL_S"]="1hr mean"
vars["AVMFL_S"]="1hr mean"
vars["CAPE_ML"]="1hr point"
vars["CAPE_MLMAX"]="day maximum"
vars["CIN_ML"]="1hr point"
vars["CIN_MLMAX"]="day maximum"
vars["DURSUN"]="day sum"
vars["FI1000p"]="6hr point"
vars["FI925p"]="1hr point"
vars["FI850p"]="6hr point"
vars["FI750p"]="6hr point"
vars["FI700p"]="6hr point"
vars["FI600p"]="6hr point"
vars["FI500p"]="6hr point"
vars["FI400p"]="6hr point"
vars["FI300p"]="6hr point"
vars["FI250p"]="6hr point"
vars["FI200p"]="6hr point"
vars["HPBL"]="1hr point"
vars["PMSL"]="1hr point"
vars["PS"]="1hr point"
vars["QV1000p"]="6hr point"
vars["QV925p"]="1hr point"
vars["QV850p"]="6hr point"
vars["QV750p"]="6hr point"
vars["QV700p"]="6hr point"
vars["QV600p"]="6hr point"
vars["QV500p"]="6hr point"
vars["QV400p"]="6hr point"
vars["QV300p"]="6hr point"
vars["QV250p"]="6hr point"
vars["QV200p"]="6hr point"
vars["QV_2M"]="1hr point"
vars["QV50z"]="1hr point"
vars["RELHUM_2M"]="1hr point"
#vars["RUNOFF_G"]="6hr sum" # required only to calculate RUNOFF_T = RUNOFF_S + RUNOFF_G
vars["RUNOFF_S"]="6hr sum"
vars["RUNOFF_T"]="6hr sum"
vars["SPGUST_10M"]="day maximum"
vars["SP_10M"]="1hr point"
vars["ASP_10M"]="1hr mean" #day mean required
vars["SPMAX_10M"]="day maximum"
vars["TOT_PREC"]="1hr sum"
vars["PREC_CON"]="1hr sum"
#vars["RAIN_CON"]="1hr sum" # required only to calculate PREC_CON = RAIN_CON + SNOW_CON, TOT_PRECIP
#vars["RAIN_GSP"]="1hr sum" # required only to calculate TOT_PRECIP
vars["TOT_SNOW"]="1hr sum"
#vars["SNOW_GSP"]="1hr sum" # required only to calculate TOT_SNOW = SNOW_GSP + SNOW_CON
#vars["SNOW_CON"]="1hr sum" # required only to calculate PREC_CON = RAIN_CON + SNOW_CON, TOT_SNOW = SNOW_GSP + SNOW_CON
vars["T1000p"]="6hr point"
vars["T925p"]="1hr point"
vars["T850p"]="6hr point"
vars["T750p"]="6hr point"
vars["T700p"]="6hr point"
vars["T600p"]="6hr point"
vars["T500p"]="6hr point"
vars["T400p"]="6hr point"
vars["T300p"]="6hr point"
vars["T250p"]="6hr point"
vars["T200p"]="6hr point"
vars["T_2M"]="1hr point"
vars["TMIN_2M"]="day minimum"
vars["TMAX_2M"]="day maximum"
vars["T50z"]="1hr point"
vars["T_SO"]="6hr point"
vars["T_G"]="1hr point"
#vars["TQC"]="1hr point" # required only to calculate TQW = TQC + TQI
vars["TQI"]="1hr point"
vars["TQV"]="1hr point"
vars["TQW"]="1hr point"
vars["U1000p"]="6hr point"
vars["U925p"]="1hr point"
vars["U850p"]="6hr point" # 3km: 1hr
vars["U750p"]="6hr point"
vars["U700p"]="6hr point"
vars["U600p"]="6hr point"
vars["U500p"]="6hr point"
vars["U400p"]="6hr point"
vars["U300p"]="6hr point"
vars["U250p"]="6hr point"
vars["U200p"]="6hr point"
vars["U_10M"]="1hr point"
vars["U50z"]="1hr point"
vars["U100z"]="1hr point"
vars["U150z"]="1hr point"
vars["U200z"]="1hr point"
vars["U250z"]="1hr point"
vars["U300z"]="1hr point"
vars["V1000p"]="6hr point"
vars["V925p"]="1hr point"
vars["V850p"]="6hr point" # 3km: 1hr
vars["V750p"]="6hr point"
vars["V700p"]="6hr point"
vars["V600p"]="6hr point"
vars["V500p"]="6hr point"
vars["V400p"]="6hr point"
vars["V300p"]="6hr point"
vars["V250p"]="6hr point"
vars["V200p"]="6hr point"
vars["V_10M"]="1hr point"
vars["V50z"]="1hr point"
vars["V100z"]="1hr point"
vars["V150z"]="1hr point"
vars["V200z"]="1hr point"
vars["V250z"]="1hr point"
vars["V300z"]="1hr point"
vars["W1000p"]="6hr point"
vars["W925p"]="1hr point"
vars["W850p"]="6hr point" # 3km: 1hr
vars["W750p"]="6hr point"
vars["W700p"]="6hr point"
vars["W600p"]="6hr point"
vars["W500p"]="6hr point"
vars["W400p"]="6hr point"
vars["W300p"]="6hr point"
vars["W250p"]="6hr point"
vars["W200p"]="6hr point"
vars["W_SO"]="1hr point"
vars["W_SO_ICE"]="1hr point"
vars["W_SNOW"]="6hr point"
vars["SNOW_MELT"]="6hr sum"
vars["H_SNOW"]="6hr point"
vars["FR_SNOW"]="6hr point"
#################################

#vars=()
#vars["AZ0"]="day mean"


# Command line arguments
function print_usage {
  echo "Run interactively as 'bash submit_checktime_post.sh [-e <expid>] [-c] [-t] [-w] [-o <output_dir>] <mon_post_dir>'"
  echo "or submit as 'sbatch -A <account> submit_checktime_post.sh [-e <expid>] [-c] [-t] [-w] [-o <output_dir>] <mon_post_dir>'"
  echo "  post_dir: path to a monthly post directory, eg. 'C2I250c_f_rc/post/2002_08'"
  echo "  account: levante project account"
  echo "  -e : Optional. Specify Experiment-ID. Default: C2I250c_f_rc"
  echo "  -c : Optional. Perform additional check for timesteps with constant field"
  echo "  -t : Optional. Perform additional check for the first and last timesteps of a file"
  echo "  -w : Optional. Display warnings"
  echo "  -o : Optional. Write results to output_dir instead of './'. Only of effect when submitting as a SLURM job."
  echo "       Will not create the target directory. Will create files of pattern 'checktime_<frequency>_<variable>.txt'."
  echo "       Will append to such files if applicable."
  echo "Checks timeaxis of timeseries post files. If run interactively, output is written to stdout."
  exit 1
}

RUN_CONST_CHECK=false
RUN_FIRSTLAST_CHECK=false
DISPLAY_WARNINGS=false
outputdir=
while true; do
    case "$1" in
        -h|--help) print_usage; exit 0;;
        -c|--const) RUN_CONST_CHECK=true; shift;;
        -e|--expid) EXPID=$2; shift 2;;
        -t|--firstlast) RUN_FIRSTLAST_CHECK=true; shift;;
        -o|--outputdir) outputdir=$2; shift 2;;
        -w|--warnings) DISPLAY_WARNINGS=true; shift;;
        -- ) shift; break;;
        * ) break;;
    esac
done

opts=""
[[ "$RUN_CONST_CHECK" == "true" ]] && opts="$opts -c"
[[ "$RUN_FIRSTLAST_CHECK" == "true" ]] && opts="$opts -t"
[[ "$DISPLAY_WARNINGS" == "true" ]] && opts="$opts -w"

dir=${1}
if [[ -z "$dir" ]]; then
  print_usage
else
  { [[ ! -d $dir ]] || [[ ! -e $dir/T_2M_ts.nc ]] ; } && {
      echo "The specified monthly post directory '$dir' does not exist."
      exit 1
  }
  [[ "$dir" != "/"* ]] && dir=$(realpath $dir)
fi

if [[ ! -z "$outputdir" ]]; then
    [[ ! -d $outputdir ]] && {
      echo "The given output directory '$outputdir' does not exist."
      exit 1
    }
    tty -s && echo "The specified output directory will be ignored, since the script is run interactively." && echo
    [[ "$outputdir" != "/"* ]] && outputdir=$(realpath $outputdir)
else
    outputdir="./"
    outputdir=$(realpath $outputdir)
fi

echo "Running timeseries checks for '$dir'"
[[ "$RUN_CONST_CHECK" == "true" ]] && echo "... incl. check for constant field"
[[ "$RUN_FIRSTLAST_CHECK" == "true" ]] && echo "... incl. check for first and last timestep of a file"
[[ "$DISPLAY_WARNINGS" == "true" ]] && echo "... displaying warnings."
tty -s || echo "Writing results to \"${outputdir}\" ..."

#Do not change
maxjobs=1
n=0
tty -s || maxjobs=12

# Determine script path
SOURCE=${BASH_SOURCE[0]}
tty -s || {
  if [ -n "${SLURM_JOB_ID:-}" ] ; then
    SOURCE=$(scontrol show job "$SLURM_JOB_ID" | awk -F= '/Command=/{print $2}')
  fi
}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPTDIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
echo $SCRIPTDIR
#exit 0

HOSTNAME=$( hostname )
echo
echo Running on $HOSTNAME
echo with PID $$

# Conduct the timeseries checks
# ->Loop in parallel over defined variables
for varx in ${!vars[@]}
do
  (
     freq=$(echo ${vars[${varx}]} | cut -d " " -f 1)
     agg=$(echo ${vars[${varx}]} | cut -d " " -f 2)
     var=$(echo $varx | cut -d "-" -f 1)
     pattern=$(echo $varx | cut -s -d "-" -f 2)
     if [[ ! -z "$pattern" ]]; then
         pattern="'${pattern}.nc*'"
     else
         pattern="'${var}_ts.nc*'"
     fi
     if [[ "$agg" == "fx" ]]; then
         dirfx="/.."
     else
         dirfx=
     fi
     echo "----------------------------------------------"
     echo "$(date) Starting ${varx} ($freq - $agg) ..."
     if [[ $maxjobs -eq 1 ]]; then
         python $SCRIPTDIR/checktime.py $opts -a $agg -f $freq -v $var -d ${dir}${dirfx} -p $pattern
     else
         python $SCRIPTDIR/checktime.py $opts -a $agg -f $freq -v $var -d ${dir}${dirfx} -p $pattern >> $outputdir/checktime_${freq}_$varx.txt 2>&1
     fi
     echo "... $(date) finished ${varx} ($freq)."
  )&
  # limit jobs
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
     wait # wait until all have finished (not optimal, but most times good enough)
     #echo $n wait
  fi
done
wait

exit 0
