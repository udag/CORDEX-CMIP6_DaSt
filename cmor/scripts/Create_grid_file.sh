#!/bin/bash
ifile=$1

var=$(echo $1 | rev | cut -d "/" -f 1 | rev | cut -d "_" -f1)

ncks -C -x -v ${var},time,time_bnds $1 gridfile.nc
ncrename -O -h -v rotated_latitude_longitude,crs gridfile.nc
ncap2 -s 'var[depth,rlat,rlon]=1.f' gridfile.nc out.nc
mv out.nc gridfile.nc
ncatted -O -h -a ,global,d,, -a ,vertices_lat,d,, -a ,vertices_lon,d,, gridfile.nc

# Part 2 - Fixing float->double conversion artifacts
cdo seltimestep,1 /work/bb1149/ESGF_Buff/CORDEX/CMIP6/DD/EUR-12/CLMcom-Hereon/ERA5/evaluation/r1i1p1f1/ICON-CLM-202407-1-1/v1-r1/mon/tsl/v20240920/tsl_EUR-12_ERA5_evaluation_r1i1p1f1_CLMcom-Hereon_ICON-CLM-202407-1-1_v1-r1_mon_195001-195012.nc gridfile_new.nc
cdo expr,"tsl=(tsl!=0)?missval(tsl):missval(tsl);" gridfile_new.nc gridfile_new_missing.nc
ncap2 -O -s '
    rlat=round(rlat*1e3)/1e3;
    rlon=round(rlon*1e3)/1e3;
    rlat_bnds=round(rlat_bnds*1e3)/1e3;
    rlon_bnds=round(rlon_bnds*1e3)/1e3;
    lat=(lat >= 10 || lat <= -10) ? round(lat*1e5)/1e5 : round(lat*1e6)/1e6;
    lon=(lon >= 10 || lon <= -10) ? round(lon*1e5)/1e5 : round(lon*1e6)/1e6;
    lat_bnds=(lat_bnds >= 10 || lat_bnds <= -10) ? round(lat_bnds*1e5)/1e5 : round(lat_bnds*1e6)/1e6;
    lon_bnds=(lon_bnds >= 10 || lon_bnds <= -10) ? round(lon_bnds*1e5)/1e5 : round(lon_bnds*1e6)/1e6;
' gridfile_new_missing.nc grid_ICON-CLM_EUR-12_corrected.nc
# Then create griddes for a source file, eg. PS_1950010100-1950123123.ncz as well as the created gridfile_new_missing.nc
# Copy the rlat_bnds and rlon_bnds as well as any differences (obviously not the xvals,yvals differences.. from the
# gridfile griddes to the PS griddes. Notably remove the datatype=float from the PS griddes.
# Apply the PS griddes to the gridfile and compress it.
cdo setgrid,griddes.txt grid_ICON-CLM_EUR-12_corrected.nc grid_ICON-CLM_EUR-12_corrected.nc_griddes
nccopy -d 1 -s grid_ICON-CLM_EUR-12_corrected.nc_griddes grid_ICON-CLM_EUR-12.nc

ncap2 -h -s "depth[depth]={0.5,2.5,7,16,34,70,142,286,574,1150};depth_bnds(:,0)={0,1,4,10,22,46,94,190,382,766};depth_bnds(:,1)={1,4,10,22,46,94,190,382,766,1534};" grid_ICON-CLM_EUR-12.nc grid_ICON-CLM_EUR-12.nc_cm
ncatted -O -h -a units,depth,c,c,"cm" -a standard_name,depth,c,c,"depth" -a long_name,depth,c,c,depth -a positive,depth,c,c,"down" -a axis,depth,c,c,"Z" -a bounds,depth,c,c,"depth_bnds" -a history,global,d,, grid_ICON-CLM_EUR-12.nc_cm
mv grid_ICON-CLM_EUR-12.nc_cm grid_ICON-CLM_EUR-12.nc

