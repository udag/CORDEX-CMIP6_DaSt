#!/bin/python

import json
import sys
import re
import os
tables="/work/bb1364/dkrz/CORDEX-CMIP6_DaSt/cordex-cmip6-cmor-tables/Tables"
cmorpath=str(sys.argv[1])

if not re.fullmatch(r"v\d{1,}-r\d{1,}$", cmorpath.rstrip("/").split("/")[-1]) or not os.path.exists(cmorpath):
  print("Please provide the CMOR path up to (and including) the version_realization, eg. '/work/ab1234/CORDEX/CMIP6/DD/EUR-12/CLMcom-DWD/MPI-ESM1-2-LR/historical/r1i1p1f1/ICON-CLM-202407-1-1/v1-r1'")
  sys.exit(1)

freqs=["fx", "mon", "day", "6hr", "1hr"]


nProv=["fx_dtb"]
nProv+=[fr+"_"+var+hPa for fr in ["mon", "day", "6hr"] for var in ["ta", "hus", "ua", "va", "wa", "zg"] for hPa in ["50", "70", "100", "150", "30", "20", "10"]]
for fr in freqs:
  with open(tables+"/CORDEX-CMIP6_"+fr+".json", "r") as f:
    vars=json.load(f)["variable_entry"]
  for var in vars.keys():
    if not os.path.exists(cmorpath+"/"+fr+"/"+var):
      if f"{fr}_{var}" not in nProv:
        print(f"Not found: {fr}_{var}") 

print()
print("For all variables besides the ones listed above, an entry in the DRS directory tree was found"
      f". Please run 'find {cmorpath} -type d -empty' to see if any of the directories does not include files.")

