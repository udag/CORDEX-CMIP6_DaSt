# PBS Header
#PBS -q rc_normal
###PBS -l memsz_job=64gb
###PBS -l vmemsz_job=64gb
#PBS -j o
###PBS -o cmor_ctl_%J.log
#PBS -l elapstim_req=08:00:00
###PBS -l walltime=06:00:00

cd $PBS_O_WORKDIR

ksh CORDEX-CMIP6.cmor_ctl $argv || echo ERROR running cmor_ctl script with arguments $argv
