#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cin / table 1hr
{ (if_requested $member $rcmod 1hr cin $chunk && {
  find_file -e            "$sdir/CIN_ML" "CIN_ML_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'cin=(CIN_ML==-999.9)?missval(CIN_ML):CIN_ML;' \
    $ifile ${sdir}/out_diag/1hr_cin_$period.nc || echo ERROR >&2
}; )&; }>$log.cin.1hr 2>$err.cin.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsbl / table 1hr
{ (if_requested $member $rcmod 1hr evspsbl $chunk && {
  find_file -e            "$sdir/AEVAP_S" "AEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 \
    -expr,'evspsbl=-AEVAP_S;' \
    $ifile ${sdir}/out_diag/1hr_evspsbl_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsbl.1hr 2>$err.evspsbl.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsblpot / table 1hr
{ (if_requested $member $rcmod 1hr evspsblpot $chunk && {
  find_file -e            "$sdir/APOTEVAP_S" "APOTEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 \
    -selname,APOTEVAP_S $ifile \
    ${sdir}/out_diag/1hr_evspsblpot_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsblpot.1hr 2>$err.evspsblpot.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfsos / table 1hr
{ (if_requested $member $rcmod 1hr mrfsos $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfsos=vertsum(sellevidxrange(W_SO_ICE,1,3))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/1hr_mrfsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfsos.1hr 2>$err.mrfsos.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsos / table 1hr
{ (if_requested $member $rcmod 1hr mrsos $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsos=vertsum(sellevidxrange(W_SO,1,3))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/1hr_mrsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsos.1hr 2>$err.mrsos.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable pr / table 1hr
{ (if_requested $member $rcmod 1hr pr $chunk && {
  find_file -e            "$sdir/TOT_PREC" "TOT_PREC_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 \
    -selname,TOT_PREC $ifile \
    ${sdir}/out_diag/1hr_pr_$period.nc || echo ERROR >&2
}; )&; }>$log.pr.1hr 2>$err.pr.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prc / table 1hr
# Editor Note: RAIN_CON + SNOW_CON
{ (if_requested $member $rcmod 1hr prc $chunk && {
  find_file -e            "$sdir/PREC_CON" "PREC_CON_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 \
    -selname,PREC_CON $ifile \
    ${sdir}/out_diag/1hr_prc_$period.nc || echo ERROR >&2
}; )&; }>$log.prc.1hr 2>$err.prc.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prsn / table 1hr
# Editor Note: SNOW_GSP + SNOW_CON
{ (if_requested $member $rcmod 1hr prsn $chunk && {
  find_file -e            "$sdir/TOT_SNOW" "TOT_SNOW_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 \
    -selname,TOT_SNOW $ifile \
    ${sdir}/out_diag/1hr_prsn_$period.nc || echo ERROR >&2
}; )&; }>$log.prsn.1hr 2>$err.prsn.1hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus925 / table 6hr
{ (if_requested $member $rcmod 6hr hus925 $chunk && {
  find_file -e            "$sdir/QV925p" "QV925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -selname,QV $ifile \
    ${sdir}/out_diag/6hr_hus925_$period.nc || echo ERROR >&2
}; )&; }>$log.hus925.6hr 2>$err.hus925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfso / table 6hr
{ (if_requested $member $rcmod 6hr mrfso $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfso=vertsum(sellevidxrange(W_SO_ICE,1,8))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrfso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfso.6hr 2>$err.mrfso.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrro / table 6hr
# Editor Note: RUNOFF_G + RUNOFF_S
{ (if_requested $member $rcmod 6hr mrro $chunk && {
  find_file -e            "$sdir/RUNOFF_T" "RUNOFF_T_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrro=RUNOFF_T/_mask;' \
    -merge -selname,RUNOFF_T $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrro_$period.nc || echo ERROR >&2
}; )&; }>$log.mrro.6hr 2>$err.mrro.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrros / table 6hr
{ (if_requested $member $rcmod 6hr mrros $chunk && {
  find_file -e            "$sdir/RUNOFF_S" "RUNOFF_S_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrros=RUNOFF_S/_mask;' \
    -merge -selname,RUNOFF_S $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrros_$period.nc || echo ERROR >&2
}; )&; }>$log.mrros.6hr 2>$err.mrros.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsfl / table 6hr
{ (if_requested $member $rcmod 6hr mrsfl $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsfl=W_SO_ICE/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrsfl_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsfl.6hr 2>$err.mrsfl.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrso / table 6hr
{ (if_requested $member $rcmod 6hr mrso $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrso=vertsum(sellevidxrange(W_SO,1,8))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrso.6hr 2>$err.mrso.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsol / table 6hr
{ (if_requested $member $rcmod 6hr mrsol $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsol=W_SO/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_mrsol_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsol.6hr 2>$err.mrsol.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rldscs / table 6hr
{ (if_requested $member $rcmod 6hr rldscs $chunk && {
  find_file -e            "$sdir/ALWFLX_DN_CS" "ALWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rldscs=sellevidx(ALWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/6hr_rldscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rldscs.6hr 2>$err.rldscs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rluscs / table 6hr
{ (if_requested $member $rcmod 6hr rluscs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rluscs=sellevidx(ALWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/6hr_rluscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rluscs.6hr 2>$err.rluscs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlutcs / table 6hr
{ (if_requested $member $rcmod 6hr rlutcs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rlutcs=sellevidx(ALWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/6hr_rlutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rlutcs.6hr 2>$err.rlutcs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdscs / table 6hr
{ (if_requested $member $rcmod 6hr rsdscs $chunk && {
  find_file -e            "$sdir/ASWFLX_DN_CS" "ASWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rsdscs=sellevidx(ASWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/6hr_rsdscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdscs.6hr 2>$err.rsdscs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsuscs / table 6hr
{ (if_requested $member $rcmod 6hr rsuscs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rsuscs=sellevidx(ASWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/6hr_rsuscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsuscs.6hr 2>$err.rsuscs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsutcs / table 6hr
{ (if_requested $member $rcmod 6hr rsutcs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'rsutcs=sellevidx(ASWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/6hr_rsutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsutcs.6hr 2>$err.rsutcs.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snc / table 6hr
{ (if_requested $member $rcmod 6hr snc $chunk && {
  find_file -e            "$sdir/FR_SNOW" "FR_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snc=FR_SNOW/_mask;' \
    -merge -selname,FR_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_snc_$period.nc || echo ERROR >&2
}; )&; }>$log.snc.6hr 2>$err.snc.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snd / table 6hr
{ (if_requested $member $rcmod 6hr snd $chunk && {
  find_file -e            "$sdir/H_SNOW" "H_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snd=H_SNOW/_mask;' \
    -merge -selname,H_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_snd_$period.nc || echo ERROR >&2
}; )&; }>$log.snd.6hr 2>$err.snd.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snm / table 6hr
{ (if_requested $member $rcmod 6hr snm $chunk && {
  find_file -e            "$sdir/SNOW_MELT" "SNOW_MELT_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snm=SNOW_MELT/_mask;' \
    -merge -selname,SNOW_MELT $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_snm_$period.nc || echo ERROR >&2
}; )&; }>$log.snm.6hr 2>$err.snm.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snw / table 6hr
{ (if_requested $member $rcmod 6hr snw $chunk && {
  find_file -e            "$sdir/W_SNOW" "W_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snw=W_SNOW/_mask;' \
    -merge -selname,W_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_snw_$period.nc || echo ERROR >&2
}; )&; }>$log.snw.6hr 2>$err.snw.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta925 / table 6hr
{ (if_requested $member $rcmod 6hr ta925 $chunk && {
  find_file -e            "$sdir/T925p" "T925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -selname,T $ifile \
    ${sdir}/out_diag/6hr_ta925_$period.nc || echo ERROR >&2
}; )&; }>$log.ta925.6hr 2>$err.ta925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tsl / table 6hr
{ (if_requested $member $rcmod 6hr tsl $chunk && {
  find_file -e            "$sdir/T_SO" "T_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);tsl=T_SO/_mask;' \
    -merge -selname,T_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/6hr_tsl_$period.nc || echo ERROR >&2
}; )&; }>$log.tsl.6hr 2>$err.tsl.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua925 / table 6hr
{ (if_requested $member $rcmod 6hr ua925 $chunk && {
  find_file -e            "$sdir/U925p" "U925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -selname,U $ifile \
    ${sdir}/out_diag/6hr_ua925_$period.nc || echo ERROR >&2
}; )&; }>$log.ua925.6hr 2>$err.ua925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va925 / table 6hr
{ (if_requested $member $rcmod 6hr va925 $chunk && {
  find_file -e            "$sdir/V925p" "V925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -selname,V $ifile \
    ${sdir}/out_diag/6hr_va925_$period.nc || echo ERROR >&2
}; )&; }>$log.va925.6hr 2>$err.va925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa925 / table 6hr
{ (if_requested $member $rcmod 6hr wa925 $chunk && {
  find_file -e            "$sdir/W925p" "W925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -selname,W $ifile \
    ${sdir}/out_diag/6hr_wa925_$period.nc || echo ERROR >&2
}; )&; }>$log.wa925.6hr 2>$err.wa925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg1000 / table 6hr
{ (if_requested $member $rcmod 6hr zg1000 $chunk && {
  find_file -e            "$sdir/FI1000p" "FI1000p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg1000=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg1000_$period.nc || echo ERROR >&2
}; )&; }>$log.zg1000.6hr 2>$err.zg1000.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg200 / table 6hr
{ (if_requested $member $rcmod 6hr zg200 $chunk && {
  find_file -e            "$sdir/FI200p" "FI200p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg200=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg200_$period.nc || echo ERROR >&2
}; )&; }>$log.zg200.6hr 2>$err.zg200.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg250 / table 6hr
{ (if_requested $member $rcmod 6hr zg250 $chunk && {
  find_file -e            "$sdir/FI250p" "FI250p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg250=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg250_$period.nc || echo ERROR >&2
}; )&; }>$log.zg250.6hr 2>$err.zg250.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg300 / table 6hr
{ (if_requested $member $rcmod 6hr zg300 $chunk && {
  find_file -e            "$sdir/FI300p" "FI300p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg300=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg300_$period.nc || echo ERROR >&2
}; )&; }>$log.zg300.6hr 2>$err.zg300.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg400 / table 6hr
{ (if_requested $member $rcmod 6hr zg400 $chunk && {
  find_file -e            "$sdir/FI400p" "FI400p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg400=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg400_$period.nc || echo ERROR >&2
}; )&; }>$log.zg400.6hr 2>$err.zg400.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg500 / table 6hr
{ (if_requested $member $rcmod 6hr zg500 $chunk && {
  find_file -e            "$sdir/FI500p" "FI500p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg500=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg500_$period.nc || echo ERROR >&2
}; )&; }>$log.zg500.6hr 2>$err.zg500.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg600 / table 6hr
{ (if_requested $member $rcmod 6hr zg600 $chunk && {
  find_file -e            "$sdir/FI600p" "FI600p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg600=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg600_$period.nc || echo ERROR >&2
}; )&; }>$log.zg600.6hr 2>$err.zg600.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg700 / table 6hr
{ (if_requested $member $rcmod 6hr zg700 $chunk && {
  find_file -e            "$sdir/FI700p" "FI700p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg700=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg700_$period.nc || echo ERROR >&2
}; )&; }>$log.zg700.6hr 2>$err.zg700.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg750 / table 6hr
{ (if_requested $member $rcmod 6hr zg750 $chunk && {
  find_file -e            "$sdir/FI750p" "FI750p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg750=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg750_$period.nc || echo ERROR >&2
}; )&; }>$log.zg750.6hr 2>$err.zg750.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg850 / table 6hr
{ (if_requested $member $rcmod 6hr zg850 $chunk && {
  find_file -e            "$sdir/FI850p" "FI850p_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'zg850=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg850_$period.nc || echo ERROR >&2
}; )&; }>$log.zg850.6hr 2>$err.zg850.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg925 / table 6hr
{ (if_requested $member $rcmod 6hr zg925 $chunk && {
  find_file -e            "$sdir/FI925p" "FI925p_${period}*.*" ifile
  $cdo $nth -f nc -O selhour,0,6,12,18 \
    -expr,'zg925=FI/9.80665;' \
    $ifile ${sdir}/out_diag/6hr_zg925_$period.nc || echo ERROR >&2
}; )&; }>$log.zg925.6hr 2>$err.zg925.6hr
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cape / table day
{ (if_requested $member $rcmod day cape $chunk && {
  find_file -e            "$sdir/CAPE_ML" "CAPE_ML_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,CAPE_ML $ifile \
    ${sdir}/out_diag/day_cape_$period.nc || echo ERROR >&2
}; )&; }>$log.cape.day 2>$err.cape.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cin / table day
{ (if_requested $member $rcmod day cin $chunk && {
  find_file -e            "$sdir/CIN_ML" "CIN_ML_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'cin=(CIN_ML==-999.9)?missval(CIN_ML):CIN_ML;' \
    $ifile ${sdir}/out_diag/day_cin_$period.nc || echo ERROR >&2
}; )&; }>$log.cin.day 2>$err.cin.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cinmax / table day
{ (if_requested $member $rcmod day cinmax $chunk && {
  find_file -e            "$sdir/CIN_MLMAX" "CIN_MLMAX_${period}*.*" ifile
  $cdo $nth -f nc -O \
    expr,'cinmax=(CIN_MLMAX==-999.9)?missval(CIN_MLMAX):CIN_MLMAX;' \
    $ifile ${sdir}/out_diag/day_cinmax_$period.nc || echo ERROR >&2
}; )&; }>$log.cinmax.day 2>$err.cinmax.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clh / table day
{ (if_requested $member $rcmod day clh $chunk && {
  find_file -e            "$sdir/ACLCH" "ACLCH_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ACLCH $ifile \
    ${sdir}/out_diag/day_clh_$period.nc || echo ERROR >&2
}; )&; }>$log.clh.day 2>$err.clh.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clivi / table day
{ (if_requested $member $rcmod day clivi $chunk && {
  find_file -e            "$sdir/TQI" "TQI_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,TQI $ifile \
    ${sdir}/out_diag/day_clivi_$period.nc || echo ERROR >&2
}; )&; }>$log.clivi.day 2>$err.clivi.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cll / table day
{ (if_requested $member $rcmod day cll $chunk && {
  find_file -e            "$sdir/ACLCL" "ACLCL_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ACLCL $ifile \
    ${sdir}/out_diag/day_cll_$period.nc || echo ERROR >&2
}; )&; }>$log.cll.day 2>$err.cll.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clm / table day
{ (if_requested $member $rcmod day clm $chunk && {
  find_file -e            "$sdir/ACLCM" "ACLCM_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ACLCM $ifile \
    ${sdir}/out_diag/day_clm_$period.nc || echo ERROR >&2
}; )&; }>$log.clm.day 2>$err.clm.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clt / table day
{ (if_requested $member $rcmod day clt $chunk && {
  find_file -e            "$sdir/ACLCT" "ACLCT_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ACLCT $ifile \
    ${sdir}/out_diag/day_clt_$period.nc || echo ERROR >&2
}; )&; }>$log.clt.day 2>$err.clt.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clwvi / table day
# Editor Note: TQC + TQI
{ (if_requested $member $rcmod day clwvi $chunk && {
  find_file -e            "$sdir/TQW" "TQW_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,TQW $ifile \
    ${sdir}/out_diag/day_clwvi_$period.nc || echo ERROR >&2
}; )&; }>$log.clwvi.day 2>$err.clwvi.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsbl / table day
{ (if_requested $member $rcmod day evspsbl $chunk && {
  find_file -e            "$sdir/AEVAP_S" "AEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -daymean \
    -expr,'evspsbl=-AEVAP_S;' \
    $ifile ${sdir}/out_diag/day_evspsbl_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsbl.day 2>$err.evspsbl.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsblpot / table day
{ (if_requested $member $rcmod day evspsblpot $chunk && {
  find_file -e            "$sdir/APOTEVAP_S" "APOTEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -daymean \
    -selname,APOTEVAP_S $ifile \
    ${sdir}/out_diag/day_evspsblpot_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsblpot.day 2>$err.evspsblpot.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hfls / table day
{ (if_requested $member $rcmod day hfls $chunk && {
  find_file -e            "$sdir/ALHFL_S" "ALHFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ALHFL_S $ifile \
    ${sdir}/out_diag/day_hfls_$period.nc || echo ERROR >&2
}; )&; }>$log.hfls.day 2>$err.hfls.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hfss / table day
{ (if_requested $member $rcmod day hfss $chunk && {
  find_file -e            "$sdir/ASHFL_S" "ASHFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASHFL_S $ifile \
    ${sdir}/out_diag/day_hfss_$period.nc || echo ERROR >&2
}; )&; }>$log.hfss.day 2>$err.hfss.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hurs / table day
{ (if_requested $member $rcmod day hurs $chunk && {
  find_file -e            "$sdir/RELHUM_2M" "RELHUM_2M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,RELHUM_2M $ifile \
    ${sdir}/out_diag/day_hurs_$period.nc || echo ERROR >&2
}; )&; }>$log.hurs.day 2>$err.hurs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus1000 / table day
{ (if_requested $member $rcmod day hus1000 $chunk && {
  find_file -e            "$sdir/QV1000p" "QV1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus1000_$period.nc || echo ERROR >&2
}; )&; }>$log.hus1000.day 2>$err.hus1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus200 / table day
{ (if_requested $member $rcmod day hus200 $chunk && {
  find_file -e            "$sdir/QV200p" "QV200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus200_$period.nc || echo ERROR >&2
}; )&; }>$log.hus200.day 2>$err.hus200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus250 / table day
{ (if_requested $member $rcmod day hus250 $chunk && {
  find_file -e            "$sdir/QV250p" "QV250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus250_$period.nc || echo ERROR >&2
}; )&; }>$log.hus250.day 2>$err.hus250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus300 / table day
{ (if_requested $member $rcmod day hus300 $chunk && {
  find_file -e            "$sdir/QV300p" "QV300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus300_$period.nc || echo ERROR >&2
}; )&; }>$log.hus300.day 2>$err.hus300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus400 / table day
{ (if_requested $member $rcmod day hus400 $chunk && {
  find_file -e            "$sdir/QV400p" "QV400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus400_$period.nc || echo ERROR >&2
}; )&; }>$log.hus400.day 2>$err.hus400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus500 / table day
{ (if_requested $member $rcmod day hus500 $chunk && {
  find_file -e            "$sdir/QV500p" "QV500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus500_$period.nc || echo ERROR >&2
}; )&; }>$log.hus500.day 2>$err.hus500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus50m / table day
{ (if_requested $member $rcmod day hus50m $chunk && {
  find_file -e            "$sdir/QV50z" "QV50z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus50m_$period.nc || echo ERROR >&2
}; )&; }>$log.hus50m.day 2>$err.hus50m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus600 / table day
{ (if_requested $member $rcmod day hus600 $chunk && {
  find_file -e            "$sdir/QV600p" "QV600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus600_$period.nc || echo ERROR >&2
}; )&; }>$log.hus600.day 2>$err.hus600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus700 / table day
{ (if_requested $member $rcmod day hus700 $chunk && {
  find_file -e            "$sdir/QV700p" "QV700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus700_$period.nc || echo ERROR >&2
}; )&; }>$log.hus700.day 2>$err.hus700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus750 / table day
{ (if_requested $member $rcmod day hus750 $chunk && {
  find_file -e            "$sdir/QV750p" "QV750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus750_$period.nc || echo ERROR >&2
}; )&; }>$log.hus750.day 2>$err.hus750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus850 / table day
{ (if_requested $member $rcmod day hus850 $chunk && {
  find_file -e            "$sdir/QV850p" "QV850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus850_$period.nc || echo ERROR >&2
}; )&; }>$log.hus850.day 2>$err.hus850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus925 / table day
{ (if_requested $member $rcmod day hus925 $chunk && {
  find_file -e            "$sdir/QV925p" "QV925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV $ifile \
    ${sdir}/out_diag/day_hus925_$period.nc || echo ERROR >&2
}; )&; }>$log.hus925.day 2>$err.hus925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable huss / table day
{ (if_requested $member $rcmod day huss $chunk && {
  find_file -e            "$sdir/QV_2M" "QV_2M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,QV_2M $ifile \
    ${sdir}/out_diag/day_huss_$period.nc || echo ERROR >&2
}; )&; }>$log.huss.day 2>$err.huss.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable li / table day
{ (if_requested $member $rcmod day li $chunk && {
  find_file -e            "$sdir/SLI" "SLI_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,SLI $ifile \
    ${sdir}/out_diag/day_li_$period.nc || echo ERROR >&2
}; )&; }>$log.li.day 2>$err.li.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfso / table day
{ (if_requested $member $rcmod day mrfso $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfso=vertsum(sellevidxrange(W_SO_ICE,1,8))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrfso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfso.day 2>$err.mrfso.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfsos / table day
{ (if_requested $member $rcmod day mrfsos $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfsos=vertsum(sellevidxrange(W_SO_ICE,1,3))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrfsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfsos.day 2>$err.mrfsos.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrro / table day
# Editor Note: RUNOFF_G + RUNOFF_S
{ (if_requested $member $rcmod day mrro $chunk && {
  find_file -e            "$sdir/RUNOFF_T" "RUNOFF_T_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrro=RUNOFF_T/_mask;' \
    -merge -selname,RUNOFF_T $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrro_$period.nc || echo ERROR >&2
}; )&; }>$log.mrro.day 2>$err.mrro.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrros / table day
{ (if_requested $member $rcmod day mrros $chunk && {
  find_file -e            "$sdir/RUNOFF_S" "RUNOFF_S_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrros=RUNOFF_S/_mask;' \
    -merge -selname,RUNOFF_S $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrros_$period.nc || echo ERROR >&2
}; )&; }>$log.mrros.day 2>$err.mrros.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsfl / table day
{ (if_requested $member $rcmod day mrsfl $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsfl=W_SO_ICE/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrsfl_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsfl.day 2>$err.mrsfl.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrso / table day
{ (if_requested $member $rcmod day mrso $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrso=vertsum(sellevidxrange(W_SO,1,8))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrso.day 2>$err.mrso.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsol / table day
{ (if_requested $member $rcmod day mrsol $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsol=W_SO/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrsol_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsol.day 2>$err.mrsol.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsos / table day
{ (if_requested $member $rcmod day mrsos $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsos=vertsum(sellevidxrange(W_SO,1,3))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_mrsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsos.day 2>$err.mrsos.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable pr / table day
{ (if_requested $member $rcmod day pr $chunk && {
  find_file -e            "$sdir/TOT_PREC" "TOT_PREC_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -daymean \
    -selname,TOT_PREC $ifile \
    ${sdir}/out_diag/day_pr_$period.nc || echo ERROR >&2
}; )&; }>$log.pr.day 2>$err.pr.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prc / table day
# Editor Note: RAIN_CON + SNOW_CON
{ (if_requested $member $rcmod day prc $chunk && {
  find_file -e            "$sdir/PREC_CON" "PREC_CON_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -daymean \
    -selname,PREC_CON $ifile \
    ${sdir}/out_diag/day_prc_$period.nc || echo ERROR >&2
}; )&; }>$log.prc.day 2>$err.prc.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prhmax / table day
{ (if_requested $member $rcmod day prhmax $chunk && {
  find_file -e            "$sdir/TOT_PREC" "TOT_PREC_${period}*.*" ifile
  $cdo $nth -f nc -O daymax -divc,3600 \
    -selname,TOT_PREC $ifile \
    ${sdir}/out_diag/day_prhmax_$period.nc || echo ERROR >&2
}; )&; }>$log.prhmax.day 2>$err.prhmax.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prsn / table day
# Editor Note: SNOW_GSP + SNOW_CON
{ (if_requested $member $rcmod day prsn $chunk && {
  find_file -e            "$sdir/TOT_SNOW" "TOT_SNOW_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -daymean \
    -selname,TOT_SNOW $ifile \
    ${sdir}/out_diag/day_prsn_$period.nc || echo ERROR >&2
}; )&; }>$log.prsn.day 2>$err.prsn.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prw / table day
{ (if_requested $member $rcmod day prw $chunk && {
  find_file -e            "$sdir/TQV" "TQV_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,TQV $ifile \
    ${sdir}/out_diag/day_prw_$period.nc || echo ERROR >&2
}; )&; }>$log.prw.day 2>$err.prw.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ps / table day
{ (if_requested $member $rcmod day ps $chunk && {
  find_file -e            "$sdir/PS" "PS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,PS $ifile \
    ${sdir}/out_diag/day_ps_$period.nc || echo ERROR >&2
}; )&; }>$log.ps.day 2>$err.ps.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable psl / table day
{ (if_requested $member $rcmod day psl $chunk && {
  find_file -e            "$sdir/PMSL" "PMSL_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,PMSL $ifile \
    ${sdir}/out_diag/day_psl_$period.nc || echo ERROR >&2
}; )&; }>$log.psl.day 2>$err.psl.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlds / table day
# Editor Note: ATHB_S + ATHU_S
{ (if_requested $member $rcmod day rlds $chunk && {
  find_file -e            "$sdir/ATHD_S" "ATHD_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ATHD_S $ifile \
    ${sdir}/out_diag/day_rlds_$period.nc || echo ERROR >&2
}; )&; }>$log.rlds.day 2>$err.rlds.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rldscs / table day
{ (if_requested $member $rcmod day rldscs $chunk && {
  find_file -e            "$sdir/ALWFLX_DN_CS" "ALWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rldscs=sellevidx(ALWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/day_rldscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rldscs.day 2>$err.rldscs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlus / table day
{ (if_requested $member $rcmod day rlus $chunk && {
  find_file -e            "$sdir/ATHU_S" "ATHU_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ATHU_S $ifile \
    ${sdir}/out_diag/day_rlus_$period.nc || echo ERROR >&2
}; )&; }>$log.rlus.day 2>$err.rlus.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rluscs / table day
{ (if_requested $member $rcmod day rluscs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rluscs=sellevidx(ALWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/day_rluscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rluscs.day 2>$err.rluscs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlut / table day
{ (if_requested $member $rcmod day rlut $chunk && {
  find_file -e            "$sdir/ATHB_T" "ATHB_T_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ATHB_T $ifile \
    ${sdir}/out_diag/day_rlut_$period.nc || echo ERROR >&2
}; )&; }>$log.rlut.day 2>$err.rlut.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlutcs / table day
{ (if_requested $member $rcmod day rlutcs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rlutcs=sellevidx(ALWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/day_rlutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rlutcs.day 2>$err.rlutcs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsds / table day
# Editor Note: ASOB_S + ASODIFU_S
{ (if_requested $member $rcmod day rsds $chunk && {
  find_file -e            "$sdir/ASOD_S" "ASOD_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASOD_S $ifile \
    ${sdir}/out_diag/day_rsds_$period.nc || echo ERROR >&2
}; )&; }>$log.rsds.day 2>$err.rsds.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdscs / table day
{ (if_requested $member $rcmod day rsdscs $chunk && {
  find_file -e            "$sdir/ASWFLX_DN_CS" "ASWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rsdscs=sellevidx(ASWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/day_rsdscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdscs.day 2>$err.rsdscs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdsdir / table day
{ (if_requested $member $rcmod day rsdsdir $chunk && {
  find_file -e            "$sdir/ASODIRD_S" "ASODIRD_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASODIRD_S $ifile \
    ${sdir}/out_diag/day_rsdsdir_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdsdir.day 2>$err.rsdsdir.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdt / table day
{ (if_requested $member $rcmod day rsdt $chunk && {
  find_file -e            "$sdir/ASOD_T" "ASOD_T_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASOD_T $ifile \
    ${sdir}/out_diag/day_rsdt_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdt.day 2>$err.rsdt.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsus / table day
{ (if_requested $member $rcmod day rsus $chunk && {
  find_file -e            "$sdir/ASODIFU_S" "ASODIFU_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASODIFU_S $ifile \
    ${sdir}/out_diag/day_rsus_$period.nc || echo ERROR >&2
}; )&; }>$log.rsus.day 2>$err.rsus.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsuscs / table day
{ (if_requested $member $rcmod day rsuscs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rsuscs=sellevidx(ASWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/day_rsuscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsuscs.day 2>$err.rsuscs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsut / table day
# Editor Note: ASOD_T - ASOB_T
{ (if_requested $member $rcmod day rsut $chunk && {
  find_file -e            "$sdir/ASOU_T" "ASOU_T_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASOU_T $ifile \
    ${sdir}/out_diag/day_rsut_$period.nc || echo ERROR >&2
}; )&; }>$log.rsut.day 2>$err.rsut.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsutcs / table day
{ (if_requested $member $rcmod day rsutcs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'rsutcs=sellevidx(ASWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/day_rsutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsutcs.day 2>$err.rsutcs.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable sfcWind / table day
{ (if_requested $member $rcmod day sfcWind $chunk && {
  find_file -e            "$sdir/ASP_10M" "ASP_10M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,ASP_10M $ifile \
    ${sdir}/out_diag/day_sfcWind_$period.nc || echo ERROR >&2
}; )&; }>$log.sfcWind.day 2>$err.sfcWind.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable siconca / table day
# Editor Note: For the 3km simulations, output will likely be hourly. FR_SEAICE has to be set to zero at grid points where FR_LAND + FR_LAKE > frsea_thrhld. With frsea_thrhld being 0.05 for this simulations.
{ (if_requested $member $rcmod day siconca $chunk && {
  find_file -e            "$sdir/AFR_SEAICE" "AFR_SEAICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND+FR_LAKE>0.05)?0:1;siconca=AFR_SEAICE*_mask;' \
    -merge -selname,AFR_SEAICE $ifile1 -selname,FR_LAKE,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_siconca_$period.nc || echo ERROR >&2
}; )&; }>$log.siconca.day 2>$err.siconca.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snc / table day
{ (if_requested $member $rcmod day snc $chunk && {
  find_file -e            "$sdir/FR_SNOW" "FR_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snc=FR_SNOW/_mask;' \
    -merge -selname,FR_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_snc_$period.nc || echo ERROR >&2
}; )&; }>$log.snc.day 2>$err.snc.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snd / table day
{ (if_requested $member $rcmod day snd $chunk && {
  find_file -e            "$sdir/H_SNOW" "H_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snd=H_SNOW/_mask;' \
    -merge -selname,H_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_snd_$period.nc || echo ERROR >&2
}; )&; }>$log.snd.day 2>$err.snd.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snm / table day
{ (if_requested $member $rcmod day snm $chunk && {
  find_file -e            "$sdir/SNOW_MELT" "SNOW_MELT_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snm=SNOW_MELT/_mask;' \
    -merge -selname,SNOW_MELT $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_snm_$period.nc || echo ERROR >&2
}; )&; }>$log.snm.day 2>$err.snm.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snw / table day
{ (if_requested $member $rcmod day snw $chunk && {
  find_file -e            "$sdir/W_SNOW" "W_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snw=W_SNOW/_mask;' \
    -merge -selname,W_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_snw_$period.nc || echo ERROR >&2
}; )&; }>$log.snw.day 2>$err.snw.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta1000 / table day
{ (if_requested $member $rcmod day ta1000 $chunk && {
  find_file -e            "$sdir/T1000p" "T1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta1000_$period.nc || echo ERROR >&2
}; )&; }>$log.ta1000.day 2>$err.ta1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta200 / table day
{ (if_requested $member $rcmod day ta200 $chunk && {
  find_file -e            "$sdir/T200p" "T200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta200_$period.nc || echo ERROR >&2
}; )&; }>$log.ta200.day 2>$err.ta200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta250 / table day
{ (if_requested $member $rcmod day ta250 $chunk && {
  find_file -e            "$sdir/T250p" "T250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta250_$period.nc || echo ERROR >&2
}; )&; }>$log.ta250.day 2>$err.ta250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta300 / table day
{ (if_requested $member $rcmod day ta300 $chunk && {
  find_file -e            "$sdir/T300p" "T300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta300_$period.nc || echo ERROR >&2
}; )&; }>$log.ta300.day 2>$err.ta300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta400 / table day
{ (if_requested $member $rcmod day ta400 $chunk && {
  find_file -e            "$sdir/T400p" "T400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta400_$period.nc || echo ERROR >&2
}; )&; }>$log.ta400.day 2>$err.ta400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta500 / table day
{ (if_requested $member $rcmod day ta500 $chunk && {
  find_file -e            "$sdir/T500p" "T500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta500_$period.nc || echo ERROR >&2
}; )&; }>$log.ta500.day 2>$err.ta500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta50m / table day
{ (if_requested $member $rcmod day ta50m $chunk && {
  find_file -e            "$sdir/T50z" "T50z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta50m_$period.nc || echo ERROR >&2
}; )&; }>$log.ta50m.day 2>$err.ta50m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta600 / table day
{ (if_requested $member $rcmod day ta600 $chunk && {
  find_file -e            "$sdir/T600p" "T600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta600_$period.nc || echo ERROR >&2
}; )&; }>$log.ta600.day 2>$err.ta600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta700 / table day
{ (if_requested $member $rcmod day ta700 $chunk && {
  find_file -e            "$sdir/T700p" "T700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta700_$period.nc || echo ERROR >&2
}; )&; }>$log.ta700.day 2>$err.ta700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta750 / table day
{ (if_requested $member $rcmod day ta750 $chunk && {
  find_file -e            "$sdir/T750p" "T750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta750_$period.nc || echo ERROR >&2
}; )&; }>$log.ta750.day 2>$err.ta750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta850 / table day
{ (if_requested $member $rcmod day ta850 $chunk && {
  find_file -e            "$sdir/T850p" "T850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta850_$period.nc || echo ERROR >&2
}; )&; }>$log.ta850.day 2>$err.ta850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta925 / table day
{ (if_requested $member $rcmod day ta925 $chunk && {
  find_file -e            "$sdir/T925p" "T925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T $ifile \
    ${sdir}/out_diag/day_ta925_$period.nc || echo ERROR >&2
}; )&; }>$log.ta925.day 2>$err.ta925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tas / table day
{ (if_requested $member $rcmod day tas $chunk && {
  find_file -e            "$sdir/T_2M" "T_2M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T_2M $ifile \
    ${sdir}/out_diag/day_tas_$period.nc || echo ERROR >&2
}; )&; }>$log.tas.day 2>$err.tas.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tauu / table day
{ (if_requested $member $rcmod day tauu $chunk && {
  find_file -e            "$sdir/AUMFL_S" "AUMFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,AUMFL_S $ifile \
    ${sdir}/out_diag/day_tauu_$period.nc || echo ERROR >&2
}; )&; }>$log.tauu.day 2>$err.tauu.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tauv / table day
{ (if_requested $member $rcmod day tauv $chunk && {
  find_file -e            "$sdir/AVMFL_S" "AVMFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,AVMFL_S $ifile \
    ${sdir}/out_diag/day_tauv_$period.nc || echo ERROR >&2
}; )&; }>$log.tauv.day 2>$err.tauv.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ts / table day
{ (if_requested $member $rcmod day ts $chunk && {
  find_file -e            "$sdir/T_G" "T_G_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,T_G $ifile \
    ${sdir}/out_diag/day_ts_$period.nc || echo ERROR >&2
}; )&; }>$log.ts.day 2>$err.ts.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tsl / table day
{ (if_requested $member $rcmod day tsl $chunk && {
  find_file -e            "$sdir/T_SO" "T_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O daymean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);tsl=T_SO/_mask;' \
    -merge -selname,T_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/day_tsl_$period.nc || echo ERROR >&2
}; )&; }>$log.tsl.day 2>$err.tsl.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua1000 / table day
{ (if_requested $member $rcmod day ua1000 $chunk && {
  find_file -e            "$sdir/U1000p" "U1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua1000_$period.nc || echo ERROR >&2
}; )&; }>$log.ua1000.day 2>$err.ua1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua100m / table day
{ (if_requested $member $rcmod day ua100m $chunk && {
  find_file -e            "$sdir/U100z" "U100z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua100m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua100m.day 2>$err.ua100m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua150m / table day
{ (if_requested $member $rcmod day ua150m $chunk && {
  find_file -e            "$sdir/U150z" "U150z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua150m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua150m.day 2>$err.ua150m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua200 / table day
{ (if_requested $member $rcmod day ua200 $chunk && {
  find_file -e            "$sdir/U200p" "U200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua200_$period.nc || echo ERROR >&2
}; )&; }>$log.ua200.day 2>$err.ua200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua200m / table day
{ (if_requested $member $rcmod day ua200m $chunk && {
  find_file -e            "$sdir/U200z" "U200z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua200m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua200m.day 2>$err.ua200m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua250 / table day
{ (if_requested $member $rcmod day ua250 $chunk && {
  find_file -e            "$sdir/U250p" "U250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua250_$period.nc || echo ERROR >&2
}; )&; }>$log.ua250.day 2>$err.ua250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua250m / table day
{ (if_requested $member $rcmod day ua250m $chunk && {
  find_file -e            "$sdir/U250z" "U250z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua250m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua250m.day 2>$err.ua250m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua300 / table day
{ (if_requested $member $rcmod day ua300 $chunk && {
  find_file -e            "$sdir/U300p" "U300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua300_$period.nc || echo ERROR >&2
}; )&; }>$log.ua300.day 2>$err.ua300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua300m / table day
{ (if_requested $member $rcmod day ua300m $chunk && {
  find_file -e            "$sdir/U300z" "U300z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua300m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua300m.day 2>$err.ua300m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua400 / table day
{ (if_requested $member $rcmod day ua400 $chunk && {
  find_file -e            "$sdir/U400p" "U400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua400_$period.nc || echo ERROR >&2
}; )&; }>$log.ua400.day 2>$err.ua400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua500 / table day
{ (if_requested $member $rcmod day ua500 $chunk && {
  find_file -e            "$sdir/U500p" "U500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua500_$period.nc || echo ERROR >&2
}; )&; }>$log.ua500.day 2>$err.ua500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua50m / table day
{ (if_requested $member $rcmod day ua50m $chunk && {
  find_file -e            "$sdir/U50z" "U50z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua50m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua50m.day 2>$err.ua50m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua600 / table day
{ (if_requested $member $rcmod day ua600 $chunk && {
  find_file -e            "$sdir/U600p" "U600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua600_$period.nc || echo ERROR >&2
}; )&; }>$log.ua600.day 2>$err.ua600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua700 / table day
{ (if_requested $member $rcmod day ua700 $chunk && {
  find_file -e            "$sdir/U700p" "U700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua700_$period.nc || echo ERROR >&2
}; )&; }>$log.ua700.day 2>$err.ua700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua750 / table day
{ (if_requested $member $rcmod day ua750 $chunk && {
  find_file -e            "$sdir/U750p" "U750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua750_$period.nc || echo ERROR >&2
}; )&; }>$log.ua750.day 2>$err.ua750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua850 / table day
{ (if_requested $member $rcmod day ua850 $chunk && {
  find_file -e            "$sdir/U850p" "U850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua850_$period.nc || echo ERROR >&2
}; )&; }>$log.ua850.day 2>$err.ua850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua925 / table day
{ (if_requested $member $rcmod day ua925 $chunk && {
  find_file -e            "$sdir/U925p" "U925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U $ifile \
    ${sdir}/out_diag/day_ua925_$period.nc || echo ERROR >&2
}; )&; }>$log.ua925.day 2>$err.ua925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable uas / table day
{ (if_requested $member $rcmod day uas $chunk && {
  find_file -e            "$sdir/U_10M" "U_10M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,U_10M $ifile \
    ${sdir}/out_diag/day_uas_$period.nc || echo ERROR >&2
}; )&; }>$log.uas.day 2>$err.uas.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va1000 / table day
{ (if_requested $member $rcmod day va1000 $chunk && {
  find_file -e            "$sdir/V1000p" "V1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va1000_$period.nc || echo ERROR >&2
}; )&; }>$log.va1000.day 2>$err.va1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va100m / table day
{ (if_requested $member $rcmod day va100m $chunk && {
  find_file -e            "$sdir/V100z" "V100z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va100m_$period.nc || echo ERROR >&2
}; )&; }>$log.va100m.day 2>$err.va100m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va150m / table day
{ (if_requested $member $rcmod day va150m $chunk && {
  find_file -e            "$sdir/V150z" "V150z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va150m_$period.nc || echo ERROR >&2
}; )&; }>$log.va150m.day 2>$err.va150m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va200 / table day
{ (if_requested $member $rcmod day va200 $chunk && {
  find_file -e            "$sdir/V200p" "V200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va200_$period.nc || echo ERROR >&2
}; )&; }>$log.va200.day 2>$err.va200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va200m / table day
{ (if_requested $member $rcmod day va200m $chunk && {
  find_file -e            "$sdir/V200z" "V200z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va200m_$period.nc || echo ERROR >&2
}; )&; }>$log.va200m.day 2>$err.va200m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va250 / table day
{ (if_requested $member $rcmod day va250 $chunk && {
  find_file -e            "$sdir/V250p" "V250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va250_$period.nc || echo ERROR >&2
}; )&; }>$log.va250.day 2>$err.va250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va250m / table day
{ (if_requested $member $rcmod day va250m $chunk && {
  find_file -e            "$sdir/V250z" "V250z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va250m_$period.nc || echo ERROR >&2
}; )&; }>$log.va250m.day 2>$err.va250m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va300 / table day
{ (if_requested $member $rcmod day va300 $chunk && {
  find_file -e            "$sdir/V300p" "V300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va300_$period.nc || echo ERROR >&2
}; )&; }>$log.va300.day 2>$err.va300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va300m / table day
{ (if_requested $member $rcmod day va300m $chunk && {
  find_file -e            "$sdir/V300z" "V300z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va300m_$period.nc || echo ERROR >&2
}; )&; }>$log.va300m.day 2>$err.va300m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va400 / table day
{ (if_requested $member $rcmod day va400 $chunk && {
  find_file -e            "$sdir/V400p" "V400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va400_$period.nc || echo ERROR >&2
}; )&; }>$log.va400.day 2>$err.va400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va500 / table day
{ (if_requested $member $rcmod day va500 $chunk && {
  find_file -e            "$sdir/V500p" "V500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va500_$period.nc || echo ERROR >&2
}; )&; }>$log.va500.day 2>$err.va500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va50m / table day
{ (if_requested $member $rcmod day va50m $chunk && {
  find_file -e            "$sdir/V50z" "V50z_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va50m_$period.nc || echo ERROR >&2
}; )&; }>$log.va50m.day 2>$err.va50m.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va600 / table day
{ (if_requested $member $rcmod day va600 $chunk && {
  find_file -e            "$sdir/V600p" "V600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va600_$period.nc || echo ERROR >&2
}; )&; }>$log.va600.day 2>$err.va600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va700 / table day
{ (if_requested $member $rcmod day va700 $chunk && {
  find_file -e            "$sdir/V700p" "V700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va700_$period.nc || echo ERROR >&2
}; )&; }>$log.va700.day 2>$err.va700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va750 / table day
{ (if_requested $member $rcmod day va750 $chunk && {
  find_file -e            "$sdir/V750p" "V750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va750_$period.nc || echo ERROR >&2
}; )&; }>$log.va750.day 2>$err.va750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va850 / table day
{ (if_requested $member $rcmod day va850 $chunk && {
  find_file -e            "$sdir/V850p" "V850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va850_$period.nc || echo ERROR >&2
}; )&; }>$log.va850.day 2>$err.va850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va925 / table day
{ (if_requested $member $rcmod day va925 $chunk && {
  find_file -e            "$sdir/V925p" "V925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V $ifile \
    ${sdir}/out_diag/day_va925_$period.nc || echo ERROR >&2
}; )&; }>$log.va925.day 2>$err.va925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable vas / table day
{ (if_requested $member $rcmod day vas $chunk && {
  find_file -e            "$sdir/V_10M" "V_10M_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,V_10M $ifile \
    ${sdir}/out_diag/day_vas_$period.nc || echo ERROR >&2
}; )&; }>$log.vas.day 2>$err.vas.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa1000 / table day
{ (if_requested $member $rcmod day wa1000 $chunk && {
  find_file -e            "$sdir/W1000p" "W1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa1000_$period.nc || echo ERROR >&2
}; )&; }>$log.wa1000.day 2>$err.wa1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa200 / table day
{ (if_requested $member $rcmod day wa200 $chunk && {
  find_file -e            "$sdir/W200p" "W200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa200_$period.nc || echo ERROR >&2
}; )&; }>$log.wa200.day 2>$err.wa200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa250 / table day
{ (if_requested $member $rcmod day wa250 $chunk && {
  find_file -e            "$sdir/W250p" "W250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa250_$period.nc || echo ERROR >&2
}; )&; }>$log.wa250.day 2>$err.wa250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa300 / table day
{ (if_requested $member $rcmod day wa300 $chunk && {
  find_file -e            "$sdir/W300p" "W300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa300_$period.nc || echo ERROR >&2
}; )&; }>$log.wa300.day 2>$err.wa300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa400 / table day
{ (if_requested $member $rcmod day wa400 $chunk && {
  find_file -e            "$sdir/W400p" "W400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa400_$period.nc || echo ERROR >&2
}; )&; }>$log.wa400.day 2>$err.wa400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa500 / table day
{ (if_requested $member $rcmod day wa500 $chunk && {
  find_file -e            "$sdir/W500p" "W500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa500_$period.nc || echo ERROR >&2
}; )&; }>$log.wa500.day 2>$err.wa500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa600 / table day
{ (if_requested $member $rcmod day wa600 $chunk && {
  find_file -e            "$sdir/W600p" "W600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa600_$period.nc || echo ERROR >&2
}; )&; }>$log.wa600.day 2>$err.wa600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa700 / table day
{ (if_requested $member $rcmod day wa700 $chunk && {
  find_file -e            "$sdir/W700p" "W700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa700_$period.nc || echo ERROR >&2
}; )&; }>$log.wa700.day 2>$err.wa700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa750 / table day
{ (if_requested $member $rcmod day wa750 $chunk && {
  find_file -e            "$sdir/W750p" "W750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa750_$period.nc || echo ERROR >&2
}; )&; }>$log.wa750.day 2>$err.wa750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa850 / table day
{ (if_requested $member $rcmod day wa850 $chunk && {
  find_file -e            "$sdir/W850p" "W850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa850_$period.nc || echo ERROR >&2
}; )&; }>$log.wa850.day 2>$err.wa850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa925 / table day
{ (if_requested $member $rcmod day wa925 $chunk && {
  find_file -e            "$sdir/W925p" "W925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,W $ifile \
    ${sdir}/out_diag/day_wa925_$period.nc || echo ERROR >&2
}; )&; }>$log.wa925.day 2>$err.wa925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg1000 / table day
{ (if_requested $member $rcmod day zg1000 $chunk && {
  find_file -e            "$sdir/FI1000p" "FI1000p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg1000=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg1000_$period.nc || echo ERROR >&2
}; )&; }>$log.zg1000.day 2>$err.zg1000.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg200 / table day
{ (if_requested $member $rcmod day zg200 $chunk && {
  find_file -e            "$sdir/FI200p" "FI200p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg200=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg200_$period.nc || echo ERROR >&2
}; )&; }>$log.zg200.day 2>$err.zg200.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg250 / table day
{ (if_requested $member $rcmod day zg250 $chunk && {
  find_file -e            "$sdir/FI250p" "FI250p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg250=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg250_$period.nc || echo ERROR >&2
}; )&; }>$log.zg250.day 2>$err.zg250.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg300 / table day
{ (if_requested $member $rcmod day zg300 $chunk && {
  find_file -e            "$sdir/FI300p" "FI300p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg300=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg300_$period.nc || echo ERROR >&2
}; )&; }>$log.zg300.day 2>$err.zg300.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg400 / table day
{ (if_requested $member $rcmod day zg400 $chunk && {
  find_file -e            "$sdir/FI400p" "FI400p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg400=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg400_$period.nc || echo ERROR >&2
}; )&; }>$log.zg400.day 2>$err.zg400.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg500 / table day
{ (if_requested $member $rcmod day zg500 $chunk && {
  find_file -e            "$sdir/FI500p" "FI500p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg500=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg500_$period.nc || echo ERROR >&2
}; )&; }>$log.zg500.day 2>$err.zg500.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg600 / table day
{ (if_requested $member $rcmod day zg600 $chunk && {
  find_file -e            "$sdir/FI600p" "FI600p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg600=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg600_$period.nc || echo ERROR >&2
}; )&; }>$log.zg600.day 2>$err.zg600.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg700 / table day
{ (if_requested $member $rcmod day zg700 $chunk && {
  find_file -e            "$sdir/FI700p" "FI700p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg700=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg700_$period.nc || echo ERROR >&2
}; )&; }>$log.zg700.day 2>$err.zg700.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg750 / table day
{ (if_requested $member $rcmod day zg750 $chunk && {
  find_file -e            "$sdir/FI750p" "FI750p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg750=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg750_$period.nc || echo ERROR >&2
}; )&; }>$log.zg750.day 2>$err.zg750.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg850 / table day
{ (if_requested $member $rcmod day zg850 $chunk && {
  find_file -e            "$sdir/FI850p" "FI850p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg850=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg850_$period.nc || echo ERROR >&2
}; )&; }>$log.zg850.day 2>$err.zg850.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg925 / table day
{ (if_requested $member $rcmod day zg925 $chunk && {
  find_file -e            "$sdir/FI925p" "FI925p_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -expr,'zg925=FI/9.80665;' \
    $ifile ${sdir}/out_diag/day_zg925_$period.nc || echo ERROR >&2
}; )&; }>$log.zg925.day 2>$err.zg925.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zmla / table day
{ (if_requested $member $rcmod day zmla $chunk && {
  find_file -e            "$sdir/HPBL" "HPBL_${period}*.*" ifile
  $cdo $nth -f nc -O daymean \
    -selname,HPBL $ifile \
    ${sdir}/out_diag/day_zmla_$period.nc || echo ERROR >&2
}; )&; }>$log.zmla.day 2>$err.zmla.day
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable areacella / table fx
{ (if_requested $member $rcmod fx areacella $chunk && {
  find_file -e            "$sdir/HSURF" "HSURF.*" ifile
  $cdo $nth -f nc -O \
    expr,'areacella=gridarea(HSURF);' \
    $ifile ${sdir}/out_diag/fx_areacella.nc || echo ERROR >&2
}; )&; }>$log.areacella.fx 2>$err.areacella.fx
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsofc / table fx
{ (if_requested $member $rcmod fx mrsofc $chunk && {
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsofc=3.82*1000*FIELDCAP/_mask;' \
    $ifile ${sdir}/out_diag/fx_mrsofc.nc || echo ERROR >&2
}; )&; }>$log.mrsofc.fx 2>$err.mrsofc.fx
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rootd / table fx
{ (if_requested $member $rcmod fx rootd $chunk && {
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile
  $cdo $nth -f nc -O \
    expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);rootd=ROOTDP/_mask;' \
    $ifile ${sdir}/out_diag/fx_rootd.nc || echo ERROR >&2
}; )&; }>$log.rootd.fx 2>$err.rootd.fx
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cape / table mon
{ (if_requested $member $rcmod mon cape $chunk && {
  find_file -e            "$sdir/CAPE_ML" "CAPE_ML_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,CAPE_ML $ifile \
    ${sdir}/out_diag/mon_cape_$period.nc || echo ERROR >&2
}; )&; }>$log.cape.mon 2>$err.cape.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable capemax / table mon
{ (if_requested $member $rcmod mon capemax $chunk && {
  find_file -e            "$sdir/CAPE_MLMAX" "CAPE_MLMAX_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,CAPE_MLMAX $ifile \
    ${sdir}/out_diag/mon_capemax_$period.nc || echo ERROR >&2
}; )&; }>$log.capemax.mon 2>$err.capemax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cin / table mon
{ (if_requested $member $rcmod mon cin $chunk && {
  find_file -e            "$sdir/CIN_ML" "CIN_ML_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'cin=(CIN_ML==-999.9)?missval(CIN_ML):CIN_ML;' \
    $ifile ${sdir}/out_diag/mon_cin_$period.nc || echo ERROR >&2
}; )&; }>$log.cin.mon 2>$err.cin.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cinmax / table mon
{ (if_requested $member $rcmod mon cinmax $chunk && {
  find_file -e            "$sdir/CIN_MLMAX" "CIN_MLMAX_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'cinmax=(CIN_MLMAX==-999.9)?missval(CIN_MLMAX):CIN_MLMAX;' \
    $ifile ${sdir}/out_diag/mon_cinmax_$period.nc || echo ERROR >&2
}; )&; }>$log.cinmax.mon 2>$err.cinmax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clh / table mon
{ (if_requested $member $rcmod mon clh $chunk && {
  find_file -e            "$sdir/ACLCH" "ACLCH_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ACLCH $ifile \
    ${sdir}/out_diag/mon_clh_$period.nc || echo ERROR >&2
}; )&; }>$log.clh.mon 2>$err.clh.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clivi / table mon
{ (if_requested $member $rcmod mon clivi $chunk && {
  find_file -e            "$sdir/TQI" "TQI_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,TQI $ifile \
    ${sdir}/out_diag/mon_clivi_$period.nc || echo ERROR >&2
}; )&; }>$log.clivi.mon 2>$err.clivi.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable cll / table mon
{ (if_requested $member $rcmod mon cll $chunk && {
  find_file -e            "$sdir/ACLCL" "ACLCL_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ACLCL $ifile \
    ${sdir}/out_diag/mon_cll_$period.nc || echo ERROR >&2
}; )&; }>$log.cll.mon 2>$err.cll.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clm / table mon
{ (if_requested $member $rcmod mon clm $chunk && {
  find_file -e            "$sdir/ACLCM" "ACLCM_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ACLCM $ifile \
    ${sdir}/out_diag/mon_clm_$period.nc || echo ERROR >&2
}; )&; }>$log.clm.mon 2>$err.clm.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clt / table mon
{ (if_requested $member $rcmod mon clt $chunk && {
  find_file -e            "$sdir/ACLCT" "ACLCT_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ACLCT $ifile \
    ${sdir}/out_diag/mon_clt_$period.nc || echo ERROR >&2
}; )&; }>$log.clt.mon 2>$err.clt.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable clwvi / table mon
# Editor Note: TQC + TQI
{ (if_requested $member $rcmod mon clwvi $chunk && {
  find_file -e            "$sdir/TQW" "TQW_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,TQW $ifile \
    ${sdir}/out_diag/mon_clwvi_$period.nc || echo ERROR >&2
}; )&; }>$log.clwvi.mon 2>$err.clwvi.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsbl / table mon
{ (if_requested $member $rcmod mon evspsbl $chunk && {
  find_file -e            "$sdir/AEVAP_S" "AEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -monmean \
    -expr,'evspsbl=-AEVAP_S;' \
    $ifile ${sdir}/out_diag/mon_evspsbl_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsbl.mon 2>$err.evspsbl.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable evspsblpot / table mon
{ (if_requested $member $rcmod mon evspsblpot $chunk && {
  find_file -e            "$sdir/APOTEVAP_S" "APOTEVAP_S_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -monmean \
    -selname,APOTEVAP_S $ifile \
    ${sdir}/out_diag/mon_evspsblpot_$period.nc || echo ERROR >&2
}; )&; }>$log.evspsblpot.mon 2>$err.evspsblpot.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hfls / table mon
{ (if_requested $member $rcmod mon hfls $chunk && {
  find_file -e            "$sdir/ALHFL_S" "ALHFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ALHFL_S $ifile \
    ${sdir}/out_diag/mon_hfls_$period.nc || echo ERROR >&2
}; )&; }>$log.hfls.mon 2>$err.hfls.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hfss / table mon
{ (if_requested $member $rcmod mon hfss $chunk && {
  find_file -e            "$sdir/ASHFL_S" "ASHFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASHFL_S $ifile \
    ${sdir}/out_diag/mon_hfss_$period.nc || echo ERROR >&2
}; )&; }>$log.hfss.mon 2>$err.hfss.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hurs / table mon
{ (if_requested $member $rcmod mon hurs $chunk && {
  find_file -e            "$sdir/RELHUM_2M" "RELHUM_2M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,RELHUM_2M $ifile \
    ${sdir}/out_diag/mon_hurs_$period.nc || echo ERROR >&2
}; )&; }>$log.hurs.mon 2>$err.hurs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus1000 / table mon
{ (if_requested $member $rcmod mon hus1000 $chunk && {
  find_file -e            "$sdir/QV1000p" "QV1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus1000_$period.nc || echo ERROR >&2
}; )&; }>$log.hus1000.mon 2>$err.hus1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus200 / table mon
{ (if_requested $member $rcmod mon hus200 $chunk && {
  find_file -e            "$sdir/QV200p" "QV200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus200_$period.nc || echo ERROR >&2
}; )&; }>$log.hus200.mon 2>$err.hus200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus250 / table mon
{ (if_requested $member $rcmod mon hus250 $chunk && {
  find_file -e            "$sdir/QV250p" "QV250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus250_$period.nc || echo ERROR >&2
}; )&; }>$log.hus250.mon 2>$err.hus250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus300 / table mon
{ (if_requested $member $rcmod mon hus300 $chunk && {
  find_file -e            "$sdir/QV300p" "QV300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus300_$period.nc || echo ERROR >&2
}; )&; }>$log.hus300.mon 2>$err.hus300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus400 / table mon
{ (if_requested $member $rcmod mon hus400 $chunk && {
  find_file -e            "$sdir/QV400p" "QV400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus400_$period.nc || echo ERROR >&2
}; )&; }>$log.hus400.mon 2>$err.hus400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus500 / table mon
{ (if_requested $member $rcmod mon hus500 $chunk && {
  find_file -e            "$sdir/QV500p" "QV500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus500_$period.nc || echo ERROR >&2
}; )&; }>$log.hus500.mon 2>$err.hus500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus50m / table mon
{ (if_requested $member $rcmod mon hus50m $chunk && {
  find_file -e            "$sdir/QV50z" "QV50z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus50m_$period.nc || echo ERROR >&2
}; )&; }>$log.hus50m.mon 2>$err.hus50m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus600 / table mon
{ (if_requested $member $rcmod mon hus600 $chunk && {
  find_file -e            "$sdir/QV600p" "QV600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus600_$period.nc || echo ERROR >&2
}; )&; }>$log.hus600.mon 2>$err.hus600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus700 / table mon
{ (if_requested $member $rcmod mon hus700 $chunk && {
  find_file -e            "$sdir/QV700p" "QV700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus700_$period.nc || echo ERROR >&2
}; )&; }>$log.hus700.mon 2>$err.hus700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus750 / table mon
{ (if_requested $member $rcmod mon hus750 $chunk && {
  find_file -e            "$sdir/QV750p" "QV750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus750_$period.nc || echo ERROR >&2
}; )&; }>$log.hus750.mon 2>$err.hus750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus850 / table mon
{ (if_requested $member $rcmod mon hus850 $chunk && {
  find_file -e            "$sdir/QV850p" "QV850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus850_$period.nc || echo ERROR >&2
}; )&; }>$log.hus850.mon 2>$err.hus850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable hus925 / table mon
{ (if_requested $member $rcmod mon hus925 $chunk && {
  find_file -e            "$sdir/QV925p" "QV925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV $ifile \
    ${sdir}/out_diag/mon_hus925_$period.nc || echo ERROR >&2
}; )&; }>$log.hus925.mon 2>$err.hus925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable huss / table mon
{ (if_requested $member $rcmod mon huss $chunk && {
  find_file -e            "$sdir/QV_2M" "QV_2M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,QV_2M $ifile \
    ${sdir}/out_diag/mon_huss_$period.nc || echo ERROR >&2
}; )&; }>$log.huss.mon 2>$err.huss.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable li / table mon
{ (if_requested $member $rcmod mon li $chunk && {
  find_file -e            "$sdir/SLI" "SLI_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,SLI $ifile \
    ${sdir}/out_diag/mon_li_$period.nc || echo ERROR >&2
}; )&; }>$log.li.mon 2>$err.li.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable limax / table mon
{ (if_requested $member $rcmod mon limax $chunk && {
  find_file -e            "$sdir/SLIMAX" "SLIMAX_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,SLIMAX $ifile \
    ${sdir}/out_diag/mon_limax_$period.nc || echo ERROR >&2
}; )&; }>$log.limax.mon 2>$err.limax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfso / table mon
{ (if_requested $member $rcmod mon mrfso $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfso=vertsum(sellevidxrange(W_SO_ICE,1,8))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrfso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfso.mon 2>$err.mrfso.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrfsos / table mon
{ (if_requested $member $rcmod mon mrfsos $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrfsos=vertsum(sellevidxrange(W_SO_ICE,1,3))/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrfsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrfsos.mon 2>$err.mrfsos.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrro / table mon
# Editor Note: RUNOFF_G + RUNOFF_S
{ (if_requested $member $rcmod mon mrro $chunk && {
  find_file -e            "$sdir/RUNOFF_T" "RUNOFF_T_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrro=RUNOFF_T/_mask;' \
    -merge -selname,RUNOFF_T $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrro_$period.nc || echo ERROR >&2
}; )&; }>$log.mrro.mon 2>$err.mrro.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrros / table mon
{ (if_requested $member $rcmod mon mrros $chunk && {
  find_file -e            "$sdir/RUNOFF_S" "RUNOFF_S_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrros=RUNOFF_S/_mask;' \
    -merge -selname,RUNOFF_S $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrros_$period.nc || echo ERROR >&2
}; )&; }>$log.mrros.mon 2>$err.mrros.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsfl / table mon
{ (if_requested $member $rcmod mon mrsfl $chunk && {
  find_file -e            "$sdir/W_SO_ICE" "W_SO_ICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsfl=W_SO_ICE/_mask;' \
    -merge -selname,W_SO_ICE $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrsfl_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsfl.mon 2>$err.mrsfl.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrso / table mon
{ (if_requested $member $rcmod mon mrso $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrso=vertsum(sellevidxrange(W_SO,1,8))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrso_$period.nc || echo ERROR >&2
}; )&; }>$log.mrso.mon 2>$err.mrso.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsol / table mon
{ (if_requested $member $rcmod mon mrsol $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsol=W_SO/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrsol_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsol.mon 2>$err.mrsol.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable mrsos / table mon
{ (if_requested $member $rcmod mon mrsos $chunk && {
  find_file -e            "$sdir/W_SO" "W_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);mrsos=vertsum(sellevidxrange(W_SO,1,3))/_mask;' \
    -merge -selname,W_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_mrsos_$period.nc || echo ERROR >&2
}; )&; }>$log.mrsos.mon 2>$err.mrsos.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable od550aer / table mon
{ (if_requested $member $rcmod mon od550aer $chunk && {
  find_file -e            "$sdir/AAOD_550NM" "AAOD_550NM_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,AAOD_550NM $ifile \
    ${sdir}/out_diag/mon_od550aer_$period.nc || echo ERROR >&2
}; )&; }>$log.od550aer.mon 2>$err.od550aer.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable pr / table mon
{ (if_requested $member $rcmod mon pr $chunk && {
  find_file -e            "$sdir/TOT_PREC" "TOT_PREC_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -monmean \
    -selname,TOT_PREC $ifile \
    ${sdir}/out_diag/mon_pr_$period.nc || echo ERROR >&2
}; )&; }>$log.pr.mon 2>$err.pr.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prc / table mon
# Editor Note: RAIN_CON + SNOW_CON
{ (if_requested $member $rcmod mon prc $chunk && {
  find_file -e            "$sdir/PREC_CON" "PREC_CON_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -monmean \
    -selname,PREC_CON $ifile \
    ${sdir}/out_diag/mon_prc_$period.nc || echo ERROR >&2
}; )&; }>$log.prc.mon 2>$err.prc.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prsn / table mon
# Editor Note: SNOW_GSP + SNOW_CON
{ (if_requested $member $rcmod mon prsn $chunk && {
  find_file -e            "$sdir/TOT_SNOW" "TOT_SNOW_${period}*.*" ifile
  $cdo $nth -f nc -O divc,3600 -monmean \
    -selname,TOT_SNOW $ifile \
    ${sdir}/out_diag/mon_prsn_$period.nc || echo ERROR >&2
}; )&; }>$log.prsn.mon 2>$err.prsn.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable prw / table mon
{ (if_requested $member $rcmod mon prw $chunk && {
  find_file -e            "$sdir/TQV" "TQV_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,TQV $ifile \
    ${sdir}/out_diag/mon_prw_$period.nc || echo ERROR >&2
}; )&; }>$log.prw.mon 2>$err.prw.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ps / table mon
{ (if_requested $member $rcmod mon ps $chunk && {
  find_file -e            "$sdir/PS" "PS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,PS $ifile \
    ${sdir}/out_diag/mon_ps_$period.nc || echo ERROR >&2
}; )&; }>$log.ps.mon 2>$err.ps.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable psl / table mon
{ (if_requested $member $rcmod mon psl $chunk && {
  find_file -e            "$sdir/PMSL" "PMSL_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,PMSL $ifile \
    ${sdir}/out_diag/mon_psl_$period.nc || echo ERROR >&2
}; )&; }>$log.psl.mon 2>$err.psl.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlds / table mon
# Editor Note: ATHB_S + ATHU_S
{ (if_requested $member $rcmod mon rlds $chunk && {
  find_file -e            "$sdir/ATHD_S" "ATHD_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ATHD_S $ifile \
    ${sdir}/out_diag/mon_rlds_$period.nc || echo ERROR >&2
}; )&; }>$log.rlds.mon 2>$err.rlds.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rldscs / table mon
{ (if_requested $member $rcmod mon rldscs $chunk && {
  find_file -e            "$sdir/ALWFLX_DN_CS" "ALWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rldscs=sellevidx(ALWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/mon_rldscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rldscs.mon 2>$err.rldscs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlus / table mon
{ (if_requested $member $rcmod mon rlus $chunk && {
  find_file -e            "$sdir/ATHU_S" "ATHU_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ATHU_S $ifile \
    ${sdir}/out_diag/mon_rlus_$period.nc || echo ERROR >&2
}; )&; }>$log.rlus.mon 2>$err.rlus.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rluscs / table mon
{ (if_requested $member $rcmod mon rluscs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rluscs=sellevidx(ALWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/mon_rluscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rluscs.mon 2>$err.rluscs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlut / table mon
{ (if_requested $member $rcmod mon rlut $chunk && {
  find_file -e            "$sdir/ATHB_T" "ATHB_T_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ATHB_T $ifile \
    ${sdir}/out_diag/mon_rlut_$period.nc || echo ERROR >&2
}; )&; }>$log.rlut.mon 2>$err.rlut.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rlutcs / table mon
{ (if_requested $member $rcmod mon rlutcs $chunk && {
  find_file -e            "$sdir/ALWFLX_UP_CS" "ALWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rlutcs=sellevidx(ALWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/mon_rlutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rlutcs.mon 2>$err.rlutcs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsds / table mon
# Editor Note: ASOB_S + ASODIFU_S
{ (if_requested $member $rcmod mon rsds $chunk && {
  find_file -e            "$sdir/ASOD_S" "ASOD_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASOD_S $ifile \
    ${sdir}/out_diag/mon_rsds_$period.nc || echo ERROR >&2
}; )&; }>$log.rsds.mon 2>$err.rsds.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdscs / table mon
{ (if_requested $member $rcmod mon rsdscs $chunk && {
  find_file -e            "$sdir/ASWFLX_DN_CS" "ASWFLX_DN_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rsdscs=sellevidx(ASWFLX_DN_CS,2);' \
    $ifile ${sdir}/out_diag/mon_rsdscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdscs.mon 2>$err.rsdscs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdsdir / table mon
{ (if_requested $member $rcmod mon rsdsdir $chunk && {
  find_file -e            "$sdir/ASODIRD_S" "ASODIRD_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASODIRD_S $ifile \
    ${sdir}/out_diag/mon_rsdsdir_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdsdir.mon 2>$err.rsdsdir.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsdt / table mon
{ (if_requested $member $rcmod mon rsdt $chunk && {
  find_file -e            "$sdir/ASOD_T" "ASOD_T_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASOD_T $ifile \
    ${sdir}/out_diag/mon_rsdt_$period.nc || echo ERROR >&2
}; )&; }>$log.rsdt.mon 2>$err.rsdt.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsus / table mon
{ (if_requested $member $rcmod mon rsus $chunk && {
  find_file -e            "$sdir/ASODIFU_S" "ASODIFU_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASODIFU_S $ifile \
    ${sdir}/out_diag/mon_rsus_$period.nc || echo ERROR >&2
}; )&; }>$log.rsus.mon 2>$err.rsus.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsuscs / table mon
{ (if_requested $member $rcmod mon rsuscs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rsuscs=sellevidx(ASWFLX_UP_CS,2);' \
    $ifile ${sdir}/out_diag/mon_rsuscs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsuscs.mon 2>$err.rsuscs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsut / table mon
# Editor Note: ASOD_T - ASOB_T
{ (if_requested $member $rcmod mon rsut $chunk && {
  find_file -e            "$sdir/ASOU_T" "ASOU_T_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASOU_T $ifile \
    ${sdir}/out_diag/mon_rsut_$period.nc || echo ERROR >&2
}; )&; }>$log.rsut.mon 2>$err.rsut.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable rsutcs / table mon
{ (if_requested $member $rcmod mon rsutcs $chunk && {
  find_file -e            "$sdir/ASWFLX_UP_CS" "ASWFLX_UP_CS_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'rsutcs=sellevidx(ASWFLX_UP_CS,1);' \
    $ifile ${sdir}/out_diag/mon_rsutcs_$period.nc || echo ERROR >&2
}; )&; }>$log.rsutcs.mon 2>$err.rsutcs.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable sfcWind / table mon
{ (if_requested $member $rcmod mon sfcWind $chunk && {
  find_file -e            "$sdir/ASP_10M" "ASP_10M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,ASP_10M $ifile \
    ${sdir}/out_diag/mon_sfcWind_$period.nc || echo ERROR >&2
}; )&; }>$log.sfcWind.mon 2>$err.sfcWind.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable sfcWindmax / table mon
{ (if_requested $member $rcmod mon sfcWindmax $chunk && {
  find_file -e            "$sdir/SPMAX_10M" "SPMAX_10M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,SPMAX_10M $ifile \
    ${sdir}/out_diag/mon_sfcWindmax_$period.nc || echo ERROR >&2
}; )&; }>$log.sfcWindmax.mon 2>$err.sfcWindmax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable siconca / table mon
# Editor Note: For the 3km simulations, output will likely be hourly. FR_SEAICE has to be set to zero at grid points where FR_LAND + FR_LAKE > frsea_thrhld. With frsea_thrhld being 0.05 for this simulations.
{ (if_requested $member $rcmod mon siconca $chunk && {
  find_file -e            "$sdir/AFR_SEAICE" "AFR_SEAICE_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND+FR_LAKE>0.05)?0:1;siconca=AFR_SEAICE*_mask;' \
    -merge -selname,AFR_SEAICE $ifile1 -selname,FR_LAKE,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_siconca_$period.nc || echo ERROR >&2
}; )&; }>$log.siconca.mon 2>$err.siconca.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snc / table mon
{ (if_requested $member $rcmod mon snc $chunk && {
  find_file -e            "$sdir/FR_SNOW" "FR_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snc=FR_SNOW/_mask;' \
    -merge -selname,FR_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_snc_$period.nc || echo ERROR >&2
}; )&; }>$log.snc.mon 2>$err.snc.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snd / table mon
{ (if_requested $member $rcmod mon snd $chunk && {
  find_file -e            "$sdir/H_SNOW" "H_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snd=H_SNOW/_mask;' \
    -merge -selname,H_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_snd_$period.nc || echo ERROR >&2
}; )&; }>$log.snd.mon 2>$err.snd.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snm / table mon
{ (if_requested $member $rcmod mon snm $chunk && {
  find_file -e            "$sdir/SNOW_MELT" "SNOW_MELT_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O divc,21600 -monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snm=SNOW_MELT/_mask;' \
    -merge -selname,SNOW_MELT $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_snm_$period.nc || echo ERROR >&2
}; )&; }>$log.snm.mon 2>$err.snm.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable snw / table mon
{ (if_requested $member $rcmod mon snw $chunk && {
  find_file -e            "$sdir/W_SNOW" "W_SNOW_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);snw=W_SNOW/_mask;' \
    -merge -selname,W_SNOW $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_snw_$period.nc || echo ERROR >&2
}; )&; }>$log.snw.mon 2>$err.snw.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable sund / table mon
{ (if_requested $member $rcmod mon sund $chunk && {
  find_file -e            "$sdir/DURSUN" "DURSUN_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,DURSUN $ifile \
    ${sdir}/out_diag/mon_sund_$period.nc || echo ERROR >&2
}; )&; }>$log.sund.mon 2>$err.sund.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta1000 / table mon
{ (if_requested $member $rcmod mon ta1000 $chunk && {
  find_file -e            "$sdir/T1000p" "T1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta1000_$period.nc || echo ERROR >&2
}; )&; }>$log.ta1000.mon 2>$err.ta1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta200 / table mon
{ (if_requested $member $rcmod mon ta200 $chunk && {
  find_file -e            "$sdir/T200p" "T200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta200_$period.nc || echo ERROR >&2
}; )&; }>$log.ta200.mon 2>$err.ta200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta250 / table mon
{ (if_requested $member $rcmod mon ta250 $chunk && {
  find_file -e            "$sdir/T250p" "T250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta250_$period.nc || echo ERROR >&2
}; )&; }>$log.ta250.mon 2>$err.ta250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta300 / table mon
{ (if_requested $member $rcmod mon ta300 $chunk && {
  find_file -e            "$sdir/T300p" "T300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta300_$period.nc || echo ERROR >&2
}; )&; }>$log.ta300.mon 2>$err.ta300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta400 / table mon
{ (if_requested $member $rcmod mon ta400 $chunk && {
  find_file -e            "$sdir/T400p" "T400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta400_$period.nc || echo ERROR >&2
}; )&; }>$log.ta400.mon 2>$err.ta400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta500 / table mon
{ (if_requested $member $rcmod mon ta500 $chunk && {
  find_file -e            "$sdir/T500p" "T500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta500_$period.nc || echo ERROR >&2
}; )&; }>$log.ta500.mon 2>$err.ta500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta50m / table mon
{ (if_requested $member $rcmod mon ta50m $chunk && {
  find_file -e            "$sdir/T50z" "T50z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta50m_$period.nc || echo ERROR >&2
}; )&; }>$log.ta50m.mon 2>$err.ta50m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta600 / table mon
{ (if_requested $member $rcmod mon ta600 $chunk && {
  find_file -e            "$sdir/T600p" "T600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta600_$period.nc || echo ERROR >&2
}; )&; }>$log.ta600.mon 2>$err.ta600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta700 / table mon
{ (if_requested $member $rcmod mon ta700 $chunk && {
  find_file -e            "$sdir/T700p" "T700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta700_$period.nc || echo ERROR >&2
}; )&; }>$log.ta700.mon 2>$err.ta700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta750 / table mon
{ (if_requested $member $rcmod mon ta750 $chunk && {
  find_file -e            "$sdir/T750p" "T750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta750_$period.nc || echo ERROR >&2
}; )&; }>$log.ta750.mon 2>$err.ta750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta850 / table mon
{ (if_requested $member $rcmod mon ta850 $chunk && {
  find_file -e            "$sdir/T850p" "T850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta850_$period.nc || echo ERROR >&2
}; )&; }>$log.ta850.mon 2>$err.ta850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ta925 / table mon
{ (if_requested $member $rcmod mon ta925 $chunk && {
  find_file -e            "$sdir/T925p" "T925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T $ifile \
    ${sdir}/out_diag/mon_ta925_$period.nc || echo ERROR >&2
}; )&; }>$log.ta925.mon 2>$err.ta925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tas / table mon
{ (if_requested $member $rcmod mon tas $chunk && {
  find_file -e            "$sdir/T_2M" "T_2M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T_2M $ifile \
    ${sdir}/out_diag/mon_tas_$period.nc || echo ERROR >&2
}; )&; }>$log.tas.mon 2>$err.tas.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tasmax / table mon
{ (if_requested $member $rcmod mon tasmax $chunk && {
  find_file -e            "$sdir/TMAX_2M" "TMAX_2M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,TMAX_2M $ifile \
    ${sdir}/out_diag/mon_tasmax_$period.nc || echo ERROR >&2
}; )&; }>$log.tasmax.mon 2>$err.tasmax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tasmin / table mon
{ (if_requested $member $rcmod mon tasmin $chunk && {
  find_file -e            "$sdir/TMIN_2M" "TMIN_2M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,TMIN_2M $ifile \
    ${sdir}/out_diag/mon_tasmin_$period.nc || echo ERROR >&2
}; )&; }>$log.tasmin.mon 2>$err.tasmin.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tauu / table mon
{ (if_requested $member $rcmod mon tauu $chunk && {
  find_file -e            "$sdir/AUMFL_S" "AUMFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,AUMFL_S $ifile \
    ${sdir}/out_diag/mon_tauu_$period.nc || echo ERROR >&2
}; )&; }>$log.tauu.mon 2>$err.tauu.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tauv / table mon
{ (if_requested $member $rcmod mon tauv $chunk && {
  find_file -e            "$sdir/AVMFL_S" "AVMFL_S_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,AVMFL_S $ifile \
    ${sdir}/out_diag/mon_tauv_$period.nc || echo ERROR >&2
}; )&; }>$log.tauv.mon 2>$err.tauv.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ts / table mon
{ (if_requested $member $rcmod mon ts $chunk && {
  find_file -e            "$sdir/T_G" "T_G_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,T_G $ifile \
    ${sdir}/out_diag/mon_ts_$period.nc || echo ERROR >&2
}; )&; }>$log.ts.mon 2>$err.ts.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable tsl / table mon
{ (if_requested $member $rcmod mon tsl $chunk && {
  find_file -e            "$sdir/T_SO" "T_SO_${period}*.*" ifile1
  find_file -e            "$sdir" "${EXPID}_c.nc" ifile2
  $cdo $nth -f nc -O monmean \
    -expr,'_mask=(FR_LAND>=0.05)?1:missval(FR_LAND);tsl=T_SO/_mask;' \
    -merge -selname,T_SO $ifile1 -selname,FR_LAND $ifile2 \
    ${sdir}/out_diag/mon_tsl_$period.nc || echo ERROR >&2
}; )&; }>$log.tsl.mon 2>$err.tsl.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua1000 / table mon
{ (if_requested $member $rcmod mon ua1000 $chunk && {
  find_file -e            "$sdir/U1000p" "U1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua1000_$period.nc || echo ERROR >&2
}; )&; }>$log.ua1000.mon 2>$err.ua1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua100m / table mon
{ (if_requested $member $rcmod mon ua100m $chunk && {
  find_file -e            "$sdir/U100z" "U100z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua100m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua100m.mon 2>$err.ua100m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua150m / table mon
{ (if_requested $member $rcmod mon ua150m $chunk && {
  find_file -e            "$sdir/U150z" "U150z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua150m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua150m.mon 2>$err.ua150m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua200 / table mon
{ (if_requested $member $rcmod mon ua200 $chunk && {
  find_file -e            "$sdir/U200p" "U200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua200_$period.nc || echo ERROR >&2
}; )&; }>$log.ua200.mon 2>$err.ua200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua200m / table mon
{ (if_requested $member $rcmod mon ua200m $chunk && {
  find_file -e            "$sdir/U200z" "U200z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua200m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua200m.mon 2>$err.ua200m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua250 / table mon
{ (if_requested $member $rcmod mon ua250 $chunk && {
  find_file -e            "$sdir/U250p" "U250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua250_$period.nc || echo ERROR >&2
}; )&; }>$log.ua250.mon 2>$err.ua250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua250m / table mon
{ (if_requested $member $rcmod mon ua250m $chunk && {
  find_file -e            "$sdir/U250z" "U250z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua250m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua250m.mon 2>$err.ua250m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua300 / table mon
{ (if_requested $member $rcmod mon ua300 $chunk && {
  find_file -e            "$sdir/U300p" "U300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua300_$period.nc || echo ERROR >&2
}; )&; }>$log.ua300.mon 2>$err.ua300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua300m / table mon
{ (if_requested $member $rcmod mon ua300m $chunk && {
  find_file -e            "$sdir/U300z" "U300z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua300m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua300m.mon 2>$err.ua300m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua400 / table mon
{ (if_requested $member $rcmod mon ua400 $chunk && {
  find_file -e            "$sdir/U400p" "U400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua400_$period.nc || echo ERROR >&2
}; )&; }>$log.ua400.mon 2>$err.ua400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua500 / table mon
{ (if_requested $member $rcmod mon ua500 $chunk && {
  find_file -e            "$sdir/U500p" "U500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua500_$period.nc || echo ERROR >&2
}; )&; }>$log.ua500.mon 2>$err.ua500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua50m / table mon
{ (if_requested $member $rcmod mon ua50m $chunk && {
  find_file -e            "$sdir/U50z" "U50z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua50m_$period.nc || echo ERROR >&2
}; )&; }>$log.ua50m.mon 2>$err.ua50m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua600 / table mon
{ (if_requested $member $rcmod mon ua600 $chunk && {
  find_file -e            "$sdir/U600p" "U600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua600_$period.nc || echo ERROR >&2
}; )&; }>$log.ua600.mon 2>$err.ua600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua700 / table mon
{ (if_requested $member $rcmod mon ua700 $chunk && {
  find_file -e            "$sdir/U700p" "U700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua700_$period.nc || echo ERROR >&2
}; )&; }>$log.ua700.mon 2>$err.ua700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua750 / table mon
{ (if_requested $member $rcmod mon ua750 $chunk && {
  find_file -e            "$sdir/U750p" "U750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua750_$period.nc || echo ERROR >&2
}; )&; }>$log.ua750.mon 2>$err.ua750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua850 / table mon
{ (if_requested $member $rcmod mon ua850 $chunk && {
  find_file -e            "$sdir/U850p" "U850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua850_$period.nc || echo ERROR >&2
}; )&; }>$log.ua850.mon 2>$err.ua850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable ua925 / table mon
{ (if_requested $member $rcmod mon ua925 $chunk && {
  find_file -e            "$sdir/U925p" "U925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U $ifile \
    ${sdir}/out_diag/mon_ua925_$period.nc || echo ERROR >&2
}; )&; }>$log.ua925.mon 2>$err.ua925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable uas / table mon
{ (if_requested $member $rcmod mon uas $chunk && {
  find_file -e            "$sdir/U_10M" "U_10M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,U_10M $ifile \
    ${sdir}/out_diag/mon_uas_$period.nc || echo ERROR >&2
}; )&; }>$log.uas.mon 2>$err.uas.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va1000 / table mon
{ (if_requested $member $rcmod mon va1000 $chunk && {
  find_file -e            "$sdir/V1000p" "V1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va1000_$period.nc || echo ERROR >&2
}; )&; }>$log.va1000.mon 2>$err.va1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va100m / table mon
{ (if_requested $member $rcmod mon va100m $chunk && {
  find_file -e            "$sdir/V100z" "V100z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va100m_$period.nc || echo ERROR >&2
}; )&; }>$log.va100m.mon 2>$err.va100m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va150m / table mon
{ (if_requested $member $rcmod mon va150m $chunk && {
  find_file -e            "$sdir/V150z" "V150z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va150m_$period.nc || echo ERROR >&2
}; )&; }>$log.va150m.mon 2>$err.va150m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va200 / table mon
{ (if_requested $member $rcmod mon va200 $chunk && {
  find_file -e            "$sdir/V200p" "V200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va200_$period.nc || echo ERROR >&2
}; )&; }>$log.va200.mon 2>$err.va200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va200m / table mon
{ (if_requested $member $rcmod mon va200m $chunk && {
  find_file -e            "$sdir/V200z" "V200z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va200m_$period.nc || echo ERROR >&2
}; )&; }>$log.va200m.mon 2>$err.va200m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va250 / table mon
{ (if_requested $member $rcmod mon va250 $chunk && {
  find_file -e            "$sdir/V250p" "V250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va250_$period.nc || echo ERROR >&2
}; )&; }>$log.va250.mon 2>$err.va250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va250m / table mon
{ (if_requested $member $rcmod mon va250m $chunk && {
  find_file -e            "$sdir/V250z" "V250z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va250m_$period.nc || echo ERROR >&2
}; )&; }>$log.va250m.mon 2>$err.va250m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va300 / table mon
{ (if_requested $member $rcmod mon va300 $chunk && {
  find_file -e            "$sdir/V300p" "V300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va300_$period.nc || echo ERROR >&2
}; )&; }>$log.va300.mon 2>$err.va300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va300m / table mon
{ (if_requested $member $rcmod mon va300m $chunk && {
  find_file -e            "$sdir/V300z" "V300z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va300m_$period.nc || echo ERROR >&2
}; )&; }>$log.va300m.mon 2>$err.va300m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va400 / table mon
{ (if_requested $member $rcmod mon va400 $chunk && {
  find_file -e            "$sdir/V400p" "V400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va400_$period.nc || echo ERROR >&2
}; )&; }>$log.va400.mon 2>$err.va400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va500 / table mon
{ (if_requested $member $rcmod mon va500 $chunk && {
  find_file -e            "$sdir/V500p" "V500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va500_$period.nc || echo ERROR >&2
}; )&; }>$log.va500.mon 2>$err.va500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va50m / table mon
{ (if_requested $member $rcmod mon va50m $chunk && {
  find_file -e            "$sdir/V50z" "V50z_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va50m_$period.nc || echo ERROR >&2
}; )&; }>$log.va50m.mon 2>$err.va50m.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va600 / table mon
{ (if_requested $member $rcmod mon va600 $chunk && {
  find_file -e            "$sdir/V600p" "V600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va600_$period.nc || echo ERROR >&2
}; )&; }>$log.va600.mon 2>$err.va600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va700 / table mon
{ (if_requested $member $rcmod mon va700 $chunk && {
  find_file -e            "$sdir/V700p" "V700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va700_$period.nc || echo ERROR >&2
}; )&; }>$log.va700.mon 2>$err.va700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va750 / table mon
{ (if_requested $member $rcmod mon va750 $chunk && {
  find_file -e            "$sdir/V750p" "V750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va750_$period.nc || echo ERROR >&2
}; )&; }>$log.va750.mon 2>$err.va750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va850 / table mon
{ (if_requested $member $rcmod mon va850 $chunk && {
  find_file -e            "$sdir/V850p" "V850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va850_$period.nc || echo ERROR >&2
}; )&; }>$log.va850.mon 2>$err.va850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable va925 / table mon
{ (if_requested $member $rcmod mon va925 $chunk && {
  find_file -e            "$sdir/V925p" "V925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V $ifile \
    ${sdir}/out_diag/mon_va925_$period.nc || echo ERROR >&2
}; )&; }>$log.va925.mon 2>$err.va925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable vas / table mon
{ (if_requested $member $rcmod mon vas $chunk && {
  find_file -e            "$sdir/V_10M" "V_10M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,V_10M $ifile \
    ${sdir}/out_diag/mon_vas_$period.nc || echo ERROR >&2
}; )&; }>$log.vas.mon 2>$err.vas.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa1000 / table mon
{ (if_requested $member $rcmod mon wa1000 $chunk && {
  find_file -e            "$sdir/W1000p" "W1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa1000_$period.nc || echo ERROR >&2
}; )&; }>$log.wa1000.mon 2>$err.wa1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa200 / table mon
{ (if_requested $member $rcmod mon wa200 $chunk && {
  find_file -e            "$sdir/W200p" "W200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa200_$period.nc || echo ERROR >&2
}; )&; }>$log.wa200.mon 2>$err.wa200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa250 / table mon
{ (if_requested $member $rcmod mon wa250 $chunk && {
  find_file -e            "$sdir/W250p" "W250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa250_$period.nc || echo ERROR >&2
}; )&; }>$log.wa250.mon 2>$err.wa250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa300 / table mon
{ (if_requested $member $rcmod mon wa300 $chunk && {
  find_file -e            "$sdir/W300p" "W300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa300_$period.nc || echo ERROR >&2
}; )&; }>$log.wa300.mon 2>$err.wa300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa400 / table mon
{ (if_requested $member $rcmod mon wa400 $chunk && {
  find_file -e            "$sdir/W400p" "W400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa400_$period.nc || echo ERROR >&2
}; )&; }>$log.wa400.mon 2>$err.wa400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa500 / table mon
{ (if_requested $member $rcmod mon wa500 $chunk && {
  find_file -e            "$sdir/W500p" "W500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa500_$period.nc || echo ERROR >&2
}; )&; }>$log.wa500.mon 2>$err.wa500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa600 / table mon
{ (if_requested $member $rcmod mon wa600 $chunk && {
  find_file -e            "$sdir/W600p" "W600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa600_$period.nc || echo ERROR >&2
}; )&; }>$log.wa600.mon 2>$err.wa600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa700 / table mon
{ (if_requested $member $rcmod mon wa700 $chunk && {
  find_file -e            "$sdir/W700p" "W700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa700_$period.nc || echo ERROR >&2
}; )&; }>$log.wa700.mon 2>$err.wa700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa750 / table mon
{ (if_requested $member $rcmod mon wa750 $chunk && {
  find_file -e            "$sdir/W750p" "W750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa750_$period.nc || echo ERROR >&2
}; )&; }>$log.wa750.mon 2>$err.wa750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa850 / table mon
{ (if_requested $member $rcmod mon wa850 $chunk && {
  find_file -e            "$sdir/W850p" "W850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa850_$period.nc || echo ERROR >&2
}; )&; }>$log.wa850.mon 2>$err.wa850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wa925 / table mon
{ (if_requested $member $rcmod mon wa925 $chunk && {
  find_file -e            "$sdir/W925p" "W925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,W $ifile \
    ${sdir}/out_diag/mon_wa925_$period.nc || echo ERROR >&2
}; )&; }>$log.wa925.mon 2>$err.wa925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable wsgsmax / table mon
{ (if_requested $member $rcmod mon wsgsmax $chunk && {
  find_file -e            "$sdir/SPGUST_10M" "SPGUST_10M_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,SPGUST_10M $ifile \
    ${sdir}/out_diag/mon_wsgsmax_$period.nc || echo ERROR >&2
}; )&; }>$log.wsgsmax.mon 2>$err.wsgsmax.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable z0 / table mon
{ (if_requested $member $rcmod mon z0 $chunk && {
  find_file -e            "$sdir/AZ0" "AZ0_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,AZ0 $ifile \
    ${sdir}/out_diag/mon_z0_$period.nc || echo ERROR >&2
}; )&; }>$log.z0.mon 2>$err.z0.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg1000 / table mon
{ (if_requested $member $rcmod mon zg1000 $chunk && {
  find_file -e            "$sdir/FI1000p" "FI1000p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg1000=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg1000_$period.nc || echo ERROR >&2
}; )&; }>$log.zg1000.mon 2>$err.zg1000.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg200 / table mon
{ (if_requested $member $rcmod mon zg200 $chunk && {
  find_file -e            "$sdir/FI200p" "FI200p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg200=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg200_$period.nc || echo ERROR >&2
}; )&; }>$log.zg200.mon 2>$err.zg200.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg250 / table mon
{ (if_requested $member $rcmod mon zg250 $chunk && {
  find_file -e            "$sdir/FI250p" "FI250p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg250=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg250_$period.nc || echo ERROR >&2
}; )&; }>$log.zg250.mon 2>$err.zg250.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg300 / table mon
{ (if_requested $member $rcmod mon zg300 $chunk && {
  find_file -e            "$sdir/FI300p" "FI300p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg300=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg300_$period.nc || echo ERROR >&2
}; )&; }>$log.zg300.mon 2>$err.zg300.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg400 / table mon
{ (if_requested $member $rcmod mon zg400 $chunk && {
  find_file -e            "$sdir/FI400p" "FI400p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg400=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg400_$period.nc || echo ERROR >&2
}; )&; }>$log.zg400.mon 2>$err.zg400.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg500 / table mon
{ (if_requested $member $rcmod mon zg500 $chunk && {
  find_file -e            "$sdir/FI500p" "FI500p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg500=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg500_$period.nc || echo ERROR >&2
}; )&; }>$log.zg500.mon 2>$err.zg500.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg600 / table mon
{ (if_requested $member $rcmod mon zg600 $chunk && {
  find_file -e            "$sdir/FI600p" "FI600p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg600=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg600_$period.nc || echo ERROR >&2
}; )&; }>$log.zg600.mon 2>$err.zg600.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg700 / table mon
{ (if_requested $member $rcmod mon zg700 $chunk && {
  find_file -e            "$sdir/FI700p" "FI700p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg700=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg700_$period.nc || echo ERROR >&2
}; )&; }>$log.zg700.mon 2>$err.zg700.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg750 / table mon
{ (if_requested $member $rcmod mon zg750 $chunk && {
  find_file -e            "$sdir/FI750p" "FI750p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg750=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg750_$period.nc || echo ERROR >&2
}; )&; }>$log.zg750.mon 2>$err.zg750.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg850 / table mon
{ (if_requested $member $rcmod mon zg850 $chunk && {
  find_file -e            "$sdir/FI850p" "FI850p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg850=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg850_$period.nc || echo ERROR >&2
}; )&; }>$log.zg850.mon 2>$err.zg850.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zg925 / table mon
{ (if_requested $member $rcmod mon zg925 $chunk && {
  find_file -e            "$sdir/FI925p" "FI925p_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -expr,'zg925=FI/9.80665;' \
    $ifile ${sdir}/out_diag/mon_zg925_$period.nc || echo ERROR >&2
}; )&; }>$log.zg925.mon 2>$err.zg925.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

#-- Diagnostic for icon-clm (ESM: ICON-CLM) variable zmla / table mon
{ (if_requested $member $rcmod mon zmla $chunk && {
  find_file -e            "$sdir/HPBL" "HPBL_${period}*.*" ifile
  $cdo $nth -f nc -O monmean \
    -selname,HPBL $ifile \
    ${sdir}/out_diag/mon_zmla_$period.nc || echo ERROR >&2
}; )&; }>$log.zmla.mon 2>$err.zmla.mon
if [[ $maxtasks -gt 0 ]] && (( $(($((++tasknum)) % $maxtasks)) == 0 )); then
  wait
fi

