#! /usr/bin/env python
'''\
Check timeseries netCDF ICON-CLM datasets for gaps in time axis,
for correct first and last time value of a timeseries,
for time steps the entire field has a constant value.

In parts a quick and dirty replacement for QA-DKRZ time checks
 and CCLM2CMOR time checks.
Does not:
- check time bounds
- check timestamps of the filename
- discover all-NaN slices (xarray will prompt a warning though in that case)

Written by Martin Schupfner, 2023/2024
'''

import cftime, glob, netCDF4, os, sys
from datetime import timedelta
import argparse as ap
import numpy as np
import xarray as xr



get_tseconds = lambda t: t.total_seconds()
get_tseconds_vector = np.vectorize(get_tseconds)

def printtimedelta(d):
    'Return timedelta (s) as either min, hours, days, whatever fits best.'
    if d > 86000:
        return f"{d/86400.} days"
    if d > 3500:
        return f"{d/3600.} hours"
    if d > 50:
        return f"{d/60.} minutes"
    else: return f"{d} seconds"

def getmax(obj):
    'Return timeseries of field maxima'
    return xr.apply_ufunc(np.nanmax, obj, input_core_dims=[["rlat", "rlon"]], kwargs={"axis": (-2,-1)})

def getmin(obj):
    'Return timeseries of field minima'
    return xr.apply_ufunc(np.nanmin, obj, input_core_dims=[["rlat", "rlon"]], kwargs={"axis": (-2,-1)})

def getmainvar(ds, var):
    'Get variable name if infile variable name is without suffix (such as V and V925p).'
    match=[]
    if not var in ds.data_vars:
        for v in ds.data_vars:
            if var.startswith(v): match.append(v)
        if len(match)==1: return match[0]
    return var

def checkaggregation(aggr, cm, ifile, errors):
    'Check time cell_method / aggregation'
    if not f"time: {aggr}" in cm:
        errors.append(f"    {ifile}: ERROR: Incorrect aggregation/cell_methods '{cm}' but expected 'time: {aggr}'.")
    return errors

def checkformat(ifile, errors):
    ncds = netCDF4.Dataset(ifile, 'r')
    disk_format = ncds.disk_format
    data_model = ncds.data_model

    # Expected for raw model output
    disk_format_expected = ["NETCDF3", "HDF5"]
    data_model_expected = ["NETCDF3_64BIT_OFFSET", "NETCDF4_CLASSIC", "NETCDF4"]

    if disk_format not in disk_format_expected or data_model not in data_model_expected:
        errors.append(f"    {ifile}: ERROR: File format differs from expectation: '{data_model}/{disk_format}'.")
    return errors

def firstlastcheck(t, n, cm, freq, cal, units, deltdic, ifile, errors):
    'Check first or last timestep of a timeseries'
    t0=cftime.num2date(t.values[0], calendar=cal, units=units)
    tn=cftime.num2date(t.values[-1], calendar=cal, units=units)
    start=cftime.datetime(t0.year, 1, 1, calendar=cal)
    end=cftime.datetime(tn.year+1, 1, 1, calendar=cal)
    #print("----------")
    #print(t0, tn)
    #print(start, end)

    # Calendar dependent adjsutments
    offset=0
    if cal == "360_day" and freq == "mon":
        offset=timedelta(hours=12).total_seconds()

    # Calculate expected start and end dates
    if "time: point" in cm:
        end=end-timedelta(seconds=deltdic[freq]-offset-offset)
    elif any([cmi in cm for cmi in ["time: mean", "time: sum", "time: maximum", "time: minimum"]]):
        start=start+timedelta(seconds=deltdic[freq]/2.-offset)
        end=end-timedelta(seconds=deltdic[freq]/2.-offset)
    else:
        errors.append(f"    {ifile}: ERROR: Cannot interprete cell_methods '{cm}'.")
        return errors
    #print(start, end)
    if t0 != start:
        errors.append(f"    {ifile}: ERROR: First timestep differs from expectation ({start}): '{t0}'.")
    if tn != end:
        errors.append(f"    {ifile}: ERROR: Last timestep differs from expectation ({end}): '{tn}'.")
    return errors



# Definition of maximum deviations from the given frequency
deltdic={}
deltdic["monmax"]=timedelta(days=31.5).total_seconds()
deltdic["monmin"]=timedelta(days=27.5).total_seconds()
deltdic["mon"]=timedelta(days=31).total_seconds()
deltdic["daymax"]=timedelta(days=1.1).total_seconds()
deltdic["daymin"]=timedelta(days=0.9).total_seconds()
deltdic["day"]=timedelta(days=1).total_seconds()
deltdic["1hrmin"]=timedelta(hours=0.9).total_seconds()
deltdic["1hrmax"]=timedelta(hours=1.1).total_seconds()
deltdic["1hr"]=timedelta(hours=1).total_seconds()
deltdic["3hrmin"]=timedelta(hours=2.9).total_seconds()
deltdic["3hrmax"]=timedelta(hours=3.1).total_seconds()
deltdic["3hr"]=timedelta(hours=3).total_seconds()
deltdic["6hrmin"]=timedelta(hours=5.9).total_seconds()
deltdic["6hrmax"]=timedelta(hours=6.1).total_seconds()
deltdic["6hr"]=timedelta(hours=6).total_seconds()
deltdic["yrmax"]=timedelta(days=366.1).total_seconds()
deltdic["yrmin"]=timedelta(days=359.9).total_seconds()
deltdic["yr"]=timedelta(days=360).total_seconds()
parentdir=""
vars=[]
freq=""
checkfl=False

# Deal with command line attributes
cl = ap.ArgumentParser(description="Check for gaps in time axis and time steps with the field being constant.", prefix_chars='-+')
cl.add_argument('-v', '--variables', type=str, nargs='+', required=True, dest="vars", help='space separated list of variables to process, eg. pr prc snd')
cl.add_argument('-f', '--frequency', type=str, nargs=1, required=True, dest="freq", help='frequency, eg. 1hrPt')
cl.add_argument('-a', '--aggregation', type=str, nargs=1, required=True, dest="aggr", help='aggregation/time cell_method, eg. point')
cl.add_argument('-d', '--directory', type=str, nargs=1, required=True, dest="dir", help='parent directory, eg. /work/bb1364/<USER>/<SIMULATION>/post/timeseries')
cl.add_argument('-c', '--constcheck', required=False, dest="const_check", action='store_true', help='perform check for constant fields')
cl.add_argument('-w', '--warnings', required=False, dest="warn", action='store_true', help='turn on warnings, eg. from xarray')
cl.add_argument('-t', '--firstlast', required=False, dest="checkfl", action='store_true', help='enable check for first and last timestep of each file')
cl.add_argument('-p', '--pattern', required=False, dest="pattern", help='Provide the glob pattern of the filename. If not provided, will search for \'*<var>*.nc*\'.')

if len(sys.argv)==1:
    cl.print_help(sys.stderr)
    sys.exit(1)
args = cl.parse_args()
if args.vars:
    vars=list(args.vars)
if args.freq:
    freq=str(args.freq[0])
    if not freq in ["fx", "1hr", "3hr", "6hr", "mon", "day"]:
        sys.stderr.write(f"ERROR: Frequency '{freq}' not supported.\n")
        sys.exit(1)
if args.aggr:
    aggr=str(args.aggr[0])
    if not aggr in ["fx", "point", "sum", "maximum", "minimum", "mean"]:
        sys.stderr.write(f"ERROR: Aggregation '{aggr}' not supported.\n")
        sys.exit(1)
if args.dir:
    parentdir=str(args.dir[0])
    if not os.path.isdir(parentdir):
        sys.stderr.write(f"ERROR: Specified directory '{parentdir}' does not exist.\n")
        sys.exit(1)
if not args.warn:
    import warnings
    warnings.simplefilter("ignore")
if args.checkfl:
    if freq in ["1hr", "3hr", "6hr", "mon", "day"]:
        checkfl=True
    else:
        print(f"INFO: 'firstlast' check not supported for frequency '{freq}'.")
if args.pattern:
    pattern = args.pattern.replace("'", "")
else: pattern = False
if vars==[]:
    sys.stderr.write("ERROR: No variable(s) specified.\n")
    sys.exit(1)


# Perform checks
for varx in vars:
    if len(vars)>1:
        print("--"*50)
        print(varx)

    pardir=f"{parentdir}/" #{varx}/"
    try:
        os.chdir(pardir)
    except:
        print(f"    ERROR: Folder '{pardir}' not found!")
        continue
    if pattern:
        files=sorted(list(glob.glob(f"{pattern}")))
    else:
        files=sorted(list(glob.glob(f"*{varx}*.nc*")))
    errors=[]


    if len(files) == 0:
        print(f"    ERROR: {len(files)} files found for variable {varx} ({freq}).")
    else:
        print(f"    {len(files)} files found for variable {varx} ({freq}).")

    prevt=""
    currt=""
    cm=False
    cal=False
    units=False
    prevunits=False
    prevcal=False
    prevcm=False

    i_f=0
    for f in files:
        i_f+=1
        # Check file format
        errors=checkformat(f, errors)
        # Read dataset file
        d=xr.open_dataset(f, decode_times=False)
        # Identify main variable
        var=getmainvar(d, varx)
        if var!=varx: print(f"    {f}: '{varx}' apparently called '{var}'.")
        if var not in d.data_vars:
            print(f"    {f}: ERROR: '{var}' not found in file.")
            continue
        # Check time units and cell_methods
        if freq != 'fx':
            if not 'time' in d:
                errors.append(f"    {f}: ERROR: Cannot read time axis.")
                continue
            if 'units' not in d['time'].attrs:
                errors.append(f"    {f}: ERROR: Cannot read time units.")
                units=False
            else:
                units = d['time'].attrs['units']
                if prevunits is False: prevunits=units
            if 'calendar' not in d['time'].attrs:
                errors.append(f"    {f}: ERROR: Cannot read calendar.")
                cal=False
            else:
                cal = d['time'].attrs['calendar']
                if prevcal is False: prevcal = cal
        else:
            if len(files)>1 and i_f==1:
                errors.append(f"    ERROR: more than one file found for variable {var} despite time invariance.")
            if 'time' in d and 'time' in d[var].dims:
                if len(d['time'])<=1:
                    if args.warn:
                        errors.append(f"    {f}: Warning: time dimension (of length 1) found despite time invariance.")
                else:
                    errors.append(f"    {f}: ERROR: time dimension found despite time invariance.")
            continue
        if 'cell_methods' not in d[var].attrs:
            errors.append(f"    {f}: ERROR: Cannot read cell_methods.")
            if checkfl and units and cal:
                errors[-1]+=" Cannot perform 'firstlast' check."
            cm=False
        else:
            cm = cm=d[var].attrs["cell_methods"]
            if prevcm is False: prevcm = cm
        if units is False or cal is False:
            continue
        if cm:
            if prevcm != cm:
                errors.append(f"    {f}: ERROR: cell_methods vary within the same dataset: {prevcm} != {cm}")
            else: prevcm = cm
        if prevcal != cal:
            errors.append(f"    {f}: ERROR: calendar varies within the same dataset: {prevcal} != {cal}")
            prevcal=cal
            prevt = None
        else: prevcal = cal
        if prevunits != units:
            errors.append(f"    {f}: ERROR: time units vary within the same dataset: {prevunits} != {units}")
            prevunits=units
        else: prevunits = units

        # Perform time axis checks
        if d.time.size == 0:
            errors.append(f"    {f}: ERROR: time axis of length 0")
        elif d.time.size == 1:
            if checkfl and cm: errors=firstlastcheck(d.time, i_f, cm, freq, cal, units, deltdic, f, errors)
            if cm:
                errors=checkaggregation(aggr, cm, f, errors)
            currt = cftime.num2date(d.time[0], units=units, calendar=cal)
            if prevt:
                delt = currt - prevt
                delts = delt.total_seconds()
                if delts > deltdic[freq+"max"] or delts < deltdic[freq+"min"]:
                    errors.append(f"    {f}: ERROR: Irregularity in time axis - gap between files - previous {prevt} - current {currt} - delta-t {printtimedelta(delts)}")
            prevt = currt
        else:
            if checkfl and cm: errors=firstlastcheck(d.time, i_f, cm, freq, cal, units, deltdic, f, errors)
            if cm:
                errors=checkaggregation(aggr, cm, f, errors)
            # gaps between files
            currt = cftime.num2date(d.time[0], units=units, calendar=cal)
            if prevt:
                delt = currt - prevt
                delts = delt.total_seconds()
                if delts > deltdic[freq+"max"] or delts < deltdic[freq+"min"]:
                    errors.append(f"    {f}: ERROR: Irregularity in time axis - gap between files - previous {prevt} - current {currt} - delta-t {printtimedelta(delts)}")
            prevt = cftime.num2date(d.time[-1], units=units, calendar=cal)
            # gaps within files
            deltfs = cftime.num2date(d.time.values[1:], units=units, calendar=cal) - cftime.num2date(d.time.values[:-1], units=units, calendar=cal)
            deltfs = get_tseconds_vector(deltfs)
            ta = np.ones(len(deltfs)+1, np.float64)
            ta[:-1]=deltfs[:]
            ta[-1]=deltdic[freq+"min"]
            #print(ta)
            tb = xr.DataArray(data=ta,
                              dims=["time"],
                              coords=dict(time=d.time))
            #print(tb)
            tc = xr.where(tb<deltdic[freq+"min"], 1, 0)
            te = xr.where(tb>deltdic[freq+"max"], 1, 0)
            tf = tc + te
            tg = tb.time.where(tf>0, drop=True)
            th = tb.where(tf>0, drop=True)
            #print(tc)
            #print(te)
            #print(tf)
            #print(tg)
            #print(th)
            #for tstep in range(0, th.size):
            #    errors.append(f"    {f}: ERROR: Irregularity in time axis  - {cftime.num2date(tg.values[tstep], calendar=cal, units=units)} delta-t {printtimedelta(th.values[tstep])} from next timestep!")
            if len(th)>0:
                errors.append(f"    {f}: ERROR: Irregularity in time axis - {len(th)} occurences  - Example: {cftime.num2date(tg.values[0], calendar=cal, units=units)} delta-t {printtimedelta(th.values[0])} from next timestep!")

        if not args.const_check:
            continue
        # check for constant fields
        # Not necessarily an error - eg. timesteps when no rainfall or snowfall happens etc.
        # >3D data or 1D data (time, ?, ?, lat, lon or time, lat or time, lon)
        if len(d[var].dims)>4 or len(d[var].dims)<3:
            errors.append(f"    {f}: INFO: Skipping Constant field check - {len(d[var].dims)} dimensions not supported.")
        # 3D data (time, height/depth, lat, lon)
        elif len(d[var].dims)==4:
            #Test: d[var][5,5,:,:]=3.
            ccmax=getmax(d[var])
            ccmin=getmin(d[var])
            cc=xr.where(ccmin==ccmax, 1, 0)
            for depthi in range(0, d[d[var].dims[1]].size):
                if "depth" in d.dims:
                    cctimes = d.time.where(cc.isel(depth=depthi, drop=True)>0, drop=True)
                    ccvals = ccmax.isel(depth=depthi, drop=True).where(cc.isel(depth=depthi, drop=True)>0, drop=True)
                elif "lev" in d.dims:
                    cctimes = d.time.where(cc.isel(lev=depthi, drop=True)>0, drop=True)
                    ccvals = ccmax.isel(lev=depthi, drop=True).where(cc.isel(lev=depthi, drop=True)>0, drop=True)
                elif "plev" in d.dims:
                    cctimes = d.time.where(cc.isel(plev=depthi, drop=True)>0, drop=True)
                    ccvals = ccmax.isel(plev=depthi, drop=True).where(cc.isel(plev=depthi, drop=True)>0, drop=True)
                else:
                    errors.append(f"    {f}: INFO: Skipping Constant field check for variable with vertical dimension. Only 'lev', 'plev' and 'depth' supported (given one of {', '.join([dimi for dimi in d.dims])}).")
                    continue
                #print(cctimes.values)
                for tstep in range(0, cctimes.size):
                    errors.append(f"    {f}: INFO: Constant field for time {cftime.num2date(cctimes.values[tstep], calendar=cal, units=units)} ({ccvals.values[tstep]}) at level {depthi}.")
        # 2D data (time, lat, lon)
        else:
            ccmax=getmax(d[var])
            ccmin=getmin(d[var])
            cc=xr.where(ccmin==ccmax, 1, 0)
            cctimes = d.time.where(cc>0, drop=True)
            ccvals = ccmax.where(cc>0, drop=True)
            #print(cctimes.values)
            for tstep in range(0, cctimes.size):
                errors.append(f"    {f}: INFO: Constant field for time {cftime.num2date(cctimes.values[tstep], calendar=cal, units=units)} ({ccvals.values[tstep]}).")

    for e in errors: print(e)


# TODO: find all-nan time slices
#def _nan_argminmax_object(func, fill_value, value, axis=None, **kwargs):
#    """In house nanargmin, nanargmax for object arrays. Always return integer
#    type
#    """
#    valid_count = count(value, axis=axis)
#    value = fillna(value, fill_value)
#    data = getattr(np, func)(value, axis=axis, **kwargs)
#
#    # TODO This will evaluate dask arrays and might be costly.
#    if (valid_count == 0).any():
#        raise ValueError("All-NaN slice encountered")
#
#    return data

