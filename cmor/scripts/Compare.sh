#!/bin/bash

var=${1:-}
f=${2:-}
[[ -z "$var" ]] && { echo "provide a CMOR variable name as first argument" && exit 1; }
[[ -z "$f" ]] && { echo "provide a frequency as second argument" && exit 1; }

recipes=/work/bb1364/dkrz/CORDEX-CMIP6_DaSt/cmor/scripts/tables/ICON-CLM_CORDEX-CMIP6_recipes.json
year=1940
expid=IAEVALL00

CMORpath=/work/bb1149/ESGF_Buff/$expid/CORDEX
RAWpath=/work/bb1364/production_runs/work/$expid/post/yearly
FXpath=/work/bb1364/production_runs/work/$expid/post/${expid}_c.nc
AGGpath=/work/bb1149/ESGF_Buff/$expid/processed/out_diag
#agg: day_wa925_2002.nc
#CMOR: CORDEX/CMIP6/DD/EUR-12/CLMcom-DWD/MPI-ESM1-2-LR/historical/r1i1p1f1/ICON-CLM-202407-1-1/v1-r1/6hr/ta850/v20240701/ta850_EUR-12_MPI-ESM1-2-LR_historical_r1i1p1f1_CLMcom-DWD_ICON-CLM-202407-1-1_v1-r1_6hr_200201010000-200312311800.nc
#RAW:/work/bg1155/COPAT2/work/C2I250c_f_rc/post/yearly/T_2M/T_2M_2002010100-2002123123.ncz
get_mapped_var()
{
  python - <<EOF
import json
with open("$recipes") as f:
  recipes=json.load(f)
try:
  print([i["names"].split("|")[-1].split(",")[-1].rstrip("'") for i in recipes if i["uid"]=="'$1'"][0])
except IndexError:
  print("None")
EOF
}

# Get original variable
# if pressure or height level specified
suffix=""
lvlinfo=$(echo $var | tr -d -c 0-9)
[[ ! -z "$lvlinfo" ]] && [[ "$var" =~ ^.*${lvlinfo} ]] && suffix=${lvlinfo}p
[[ ! -z "$lvlinfo" ]] && [[ "$var" =~ ^.*${lvlinfo}m ]] && suffix=${lvlinfo}z
[[ " z0 od550aer " == *" $var "* ]] && suffix=""

echo "Searching variable '$var' for frequency '$f'..."
orig_var=$(get_mapped_var ${f}_${var})
[[ "$orig_var" == "None" ]] && { echo "No ICON-CLM variable could be found for \"$f\" \"$var\"." && exit 1; }
[[ -z "$orig_var" ]] && { echo "No ICON-CLM variable could be found for \"$f\" \"$var\"." && exit 1; }
echo "  Original variable: '${orig_var}$suffix'"
echo "... opening:"
if [[ "$f" == "fx" ]]; then
  cmorfile=$(find $CMORpath -name "${var}_*_${f}.nc")
  rawfile=$FXpath
  echo CMOR: $cmorfile
  echo Raw: $rawfile
  echo
  echo
  #exit 0
  [[ ! -z "$cmorfile" ]] && ncview $cmorfile &
  [[ ! -z "$rawfile" ]] && ncview $rawfile &
  exit 0
else
  cmorfile=$(find $CMORpath -name "${var}_*_${f}_$year*-*.nc")
  rawfile=$(find $RAWpath/${orig_var}$suffix -name "${orig_var}${suffix}_${year}*.*")
  aggfile=$(find $AGGpath -name "${f}_${var}_${year}.nc")
  echo CMOR: $cmorfile
  echo Aggregated: $aggfile
  echo Raw: $rawfile
  echo
  echo
  #exit 0
  [[ ! -z "$cmorfile" ]] && ncview $cmorfile &
  [[ ! -z "$rawfile" ]] && ncview $rawfile &
  [[ ! -z "$aggfile" ]] && ncview $aggfile &
  exit 0
fi

