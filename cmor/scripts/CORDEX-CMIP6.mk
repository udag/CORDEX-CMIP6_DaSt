EXPID =
INSTITUTIONID =
MODELID =
CDOEXE =
NCATTEDEXE =
FIRST_YEAR =
LAST_YEAR =
EXP_DIR_TRUNK =
RAW_EXP_DIR_TRUNK =
VERSIONDATE =
GIT_DIR =
JAILMODE =
EXTSUFFIX =
MAXTASKS =
SCHEDULER =

CHUNK_SIZE = 10

TARGET_STEP =
ACCOUNT =
QOS =

DIAGS_TIME = 00:30:00
CMOR_TIME = 01:00:00
CMOR_TIME_SUBDAILY = 00:30:00

ifeq ($(SCHEDULER), PBS)
	override DIAGS_TIME := 03:00:00
	override CMOR_TIME := 02:00:00
	override CMOR_TIME_SUBDAILY := 02:00:00
endif

SBATCHFLAGS = -W
PBSFLAGS =

# MAXJOBS for qsub - qsub does not support make -j
MAXJOBS := $(shell echo $(MAKEFLAGS) | sed -n 's/.*\b-j\([0-9]*\).*/\1/p')
MAXJOBS := $(if $(MAXJOBS),$(MAXJOBS),9)  # Default to 1 if unspecified
MAXJOBS := $(shell echo $$(($(MAXJOBS) + 1)))

$(info $(shell echo "Submitting with max jobs: $(MAXJOBS)"))
$(info $(shell echo "MAKEFLAGS: $(MAKEFLAGS)"))
$(info $(shell echo "MFLAGS: $(MFLAGS)"))

#Add underscore if EXTSUFFIX is not empty
ifneq ($(EXTSUFFIX),)
    ifeq ($(firstword $(filter _%,$(EXTSUFFIX))),)
        override EXTSUFFIX := _$(EXTSUFFIX)
    endif
endif

# Command line arguments for runpp script
CLARGS = -e $(EXPID) -i $(INSTITUTIONID) -m $(MODELID) -x $(CDOEXE) -n $(NCATTEDEXE) -o $(EXP_DIR_TRUNK) -r $(RAW_EXP_DIR_TRUNK) -g $(GIT_DIR) $(JAILMODE) -a $(FIRST_YEAR) -z $(LAST_YEAR) -v $(VERSIONDATE) -t $(MAXTASKS)

MESSAGE_diag = $$(date +%Y-%m-%dT%H:%M:%S): CORDEX-CMIP6 aggregation & diagnostics
MESSAGE_cmor = $$(date +%Y-%m-%dT%H:%M:%S): CORDEX-CMIP6 CMOR rewriting (daily + monthly)
MESSAGE_cmorsubdaily = $$(date +%Y-%m-%dT%H:%M:%S): CORDEX-CMIP6 CMOR rewriting (subdaily)

.PHONY: all diag cmor cmorsubdaily
.PRECIOUS: targets/$(EXPID).diag.% targets/$(EXPID).cmor.% targets/$(EXPID).cmorsubdaily.%

define first_chunk_end
$(eval remainder=$(shell echo $$(( ( $(FIRST_YEAR) - 1 ) % $(CHUNK_SIZE) )) ))
$(if $(remainder),$(shell echo $$(( $(FIRST_YEAR) + $(CHUNK_SIZE) - $(remainder) - 1 ))),$(shell echo $$(($(FIRST_YEAR) + $(CHUNK_SIZE) - 1 > $(LAST_YEAR) ? $(LAST_YEAR) : $(FIRST_YEAR) + $(CHUNK_SIZE) - 1))))
endef

define second_chunk_start
$(eval remainder=$(shell echo $$(( ( $(FIRST_YEAR) - 1 ) % $(CHUNK_SIZE) )) ))
$(if $(remainder),$(shell echo $$(( $(FIRST_YEAR) + $(CHUNK_SIZE) - $(remainder) ))),$(shell echo $$(( $(FIRST_YEAR) + $(CHUNK_SIZE) - 1 ))))
endef

define chunk_end
$(eval y=$(1))
$(if $(filter $(y),$(FIRST_YEAR)),$(call first_chunk_end),$(shell echo $$(($(y) + $(CHUNK_SIZE) - 1 > $(LAST_YEAR) ? $(LAST_YEAR) : $(y) + $(CHUNK_SIZE) - 1))))
endef

# Default target
#
all: diag cmor cmorsubdaily

diag:
	@echo "$(MESSAGE_diag) started"
	mkdir -vp targets
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) targets/$(EXPID).diag.$(FIRST_YEAR)
	@if [ "$(FIRST_YEAR)" -lt "$(LAST_YEAR)" ]; then \
		$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(addprefix targets/$(EXPID).diag.,$(shell seq $(shell echo $(FIRST_YEAR) + 1 | bc) $(LAST_YEAR))); \
	fi
	@echo "$(MESSAGE_diag) finished"

cmor: diag
	@echo "$(MESSAGE_cmor) started"
	mkdir -vp targets
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) targets/$(EXPID).cmor.$(FIRST_YEAR)
	@if [ "$(FIRST_YEAR)" -lt "$(LAST_YEAR)" ]; then \
		$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(addprefix targets/$(EXPID).cmor.,$(shell seq $(call second_chunk_start) $(CHUNK_SIZE) $(LAST_YEAR))); \
	fi
	@echo "$(MESSAGE_cmor) finished"

cmorsubdaily: diag
	@echo "$(MESSAGE_cmorsubdaily) started"
	mkdir -vp targets
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(addprefix targets/$(EXPID).cmorsubdaily.,$(shell seq $(FIRST_YEAR) $(LAST_YEAR)))
	@echo "$(MESSAGE_cmorsubdaily) finished"

targets/$(EXPID).diag.%:
	if [ "$(SCHEDULER)" = "SLURM" ]; then \
		sbatch $(SBATCHFLAGS) --time=$(DIAGS_TIME) --qos=$(QOS) -A $(ACCOUNT) --job-name=$(EXPID)_runpp_diag --output=%x_%j.log --comment=$* CORDEX-CMIP6.runpp $(CLARGS) -s TOTAL$(EXTSUFFIX) -d $* $* ; \
		touch $@ ; \
	elif [ "$(SCHEDULER)" = "PBS" ]; then \
		qsub $(PBSFLAGS) -l elapstim_req=$(DIAGS_TIME) -N $(EXPID)_runpp_diag PBS_wrapper_runpp.sh -v argv="$(CLARGS) -s TOTAL$(EXTSUFFIX) -d -f $@ $* $*" ; \
		while true; do \
			if [ -f $@.fail ] ; then \
				echo "ERROR: Job for $@ failed, see logs for more information." ; \
				break ; \
			elif [ -f $@ ] ; then \
				break ; \
			else \
				sleep 10 ; \
			fi ; \
		done ; \
	else \
		echo "Unknown scheduler. Please use SLURM or PBS." ; \
		exit 1 ; \
	fi

targets/$(EXPID).cmor.%: targets/$(EXPID).diag.%
	if [ "$(SCHEDULER)" = "SLURM" ]; then \
		sbatch $(SBATCHFLAGS) --time=$(CMOR_TIME) --qos=$(QOS) -A $(ACCOUNT) --job-name=$(EXPID)_runpp_cmor --output=%x_%j.log --comment=$* CORDEX-CMIP6.runpp $(CLARGS) -s $(EXTSUFFIX) -c $* $(shell echo $(call chunk_end,$*)) ; \
		touch $@ ; \
	elif [ "$(SCHEDULER)" = "PBS" ]; then \
		qsub $(PBSFLAGS) -l elapstim_req=$(CMOR_TIME) -N $(EXPID)_runpp_cmor PBS_wrapper_runpp.sh -v argv="$(CLARGS) -s $(EXTSUFFIX) -c -f $@ $* $(shell echo $(call chunk_end,$*))" ; \
		while true; do \
			if [ -f $@.fail ] ; then \
				echo "ERROR: Job for $@ failed, see logs for more information." ; \
				break ; \
			elif [ -f $@ ] ; then \
				break ; \
			else \
				sleep 10 ; \
			fi ; \
		done ; \
	else \
		echo "Unknown scheduler. Please use SLURM or PBS."; \
		exit 1; \
	fi

targets/$(EXPID).cmorsubdaily.%: targets/$(EXPID).diag.%
	if [ "$(SCHEDULER)" = "SLURM" ]; then \
		sbatch $(SBATCHFLAGS) --time=$(CMOR_TIME_SUBDAILY) --qos=$(QOS) -A $(ACCOUNT) --job-name=$(EXPID)_runpp_cmorsubdaily --output=%x_%j.log --comment=$* CORDEX-CMIP6.runpp $(CLARGS) -s subdaily$(EXTSUFFIX) -c $* $* ; \
		touch $@ ; \
	elif [ "$(SCHEDULER)" = "PBS" ]; then \
		qsub $(PBSFLAGS) -l elapstim_req=$(CMOR_TIME_SUBDAILY) -N $(EXPID)_runpp_cmorsubdaily PBS_wrapper_runpp.sh -v argv="$(CLARGS) -s subdaily$(EXTSUFFIX) -c -f $@ $* $*" ; \
		while true; do \
			if [ -f $@.fail ] ; then \
				echo "ERROR: Job for $@ failed, see logs for more information." ; \
				break ; \
			elif [ -f $@ ] ; then \
				break ; \
			else \
				sleep 10 ; \
			fi ; \
		done ; \
	else \
		echo "Unknown scheduler. Please use SLURM or PBS."; \
		exit 1; \
	fi
