########################
# NIMDI Script Creator #
########################

The Mapping Tables are included in the "tables/" directory.

Three functions (if_requested, Read_request_config and find_file) are included in the functions/ directory
and are used by the script fragments to test whether a variable has to be processed according to the data request
and user specifications (see info about the "conf/" directory below). find_file indentifies the input file for every chunk of data.

"incl_mod_*/" directories, if included, contain the placeholder scripts in case of CUSTOM diagnostic.
In those placeholder scripts you can enter the custom diagnostic or call diagnostic scripts.

The "conf/" directory holds data request configuration files ("CORDEX-CMIP6_requested_vars_<EXPT>.conf"). 
More information about this configuration files can be obtained from "functions/Readme_request_config.txt". 
