######################################################
# TimeSlices
######################################################
TimeSlices+=([_slice_historical]=(1950010100-2014123124))
TimeSlices+=([_slice_scenario]=(2015010100-2100123124))

#TimeSlices for 1st year only
TimeSlices+=([first_year]=(${iniyear}010100-${iniyear}123124))
TimeSlices+=([first_step]=(${iniyear}010100-${iniyear}013124))
#TimeSlice for default climatology (entire experiment time span)
TimeSlices+=([DefaultClim]=(${iniyear}010100-${finyear}013124))

###########################
# User defined TimeSlices #
###########################
# Examples #
############
#-- additional data for MIP table 'day'
#TimeSlices+=([historical_day]=(2000010100-2005123124))
#-- additional data for MIP table '6hrPlev'
#TimeSlices+=([historical_6hrPlev]=(2009010100-2038123124))
#-- years for a climatology
#TimeSlices+=([climatology1]=(1950010100-1979123124))
#TimeSlices+=([climatology2]=(1980010100-1999123124))
