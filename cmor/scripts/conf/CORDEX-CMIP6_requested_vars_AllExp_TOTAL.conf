# ##################################################
# ##################################################
# CMIP6 PostProcessing Workflow Configuration File
# ##################################################
#
# This is a special configuration file template
# that will cause all variables to be processed
# that are included in the script fragments.
#
# Other configuration files with suffixes "_<EXPERIMENT_NAME>"
# or "_AllExp" will exclude variables from being processed
# that are not requested by the respective experiment(s)
# (with custom options being possible, see USERSETTINGS below).
#
####################################################
# This configuration file specifies which variables will be
#  processed for which time intervals.
# It contains + DREQSETTINGS (computed from the CMIP6-DataRequest) and
#             + USERSETTINGS (specified by the user)
# for each experiment.
#
# There are 3 types of settings: Standard, TimeSlice, GridAndTimeSlice.
#
# Standard: 'Field:Key=True/False'
#  Fields and Keys can be labels of realisations (eg. 'r1i1p1'),
#  MIPtables (eg. 'Amon'), Variable Blocks (eg. 'Amon_ECHAM_1'),
#   CMOR Variables (eg. 'tas', 'o3') or grids (eg. '1deg', 'native').
#  Amon:tas=False means for example: For field 'Amon' do not process key 'tas'!
#  Thus, for MIP-table Amon, variable tas will not be processed.
#
# TimeSlice: 'Field:Key=slabel1,slabel2' with slabel being
#   TimeSlice-labels as defined in the experiment configuration file.
#  This means: For field, key will only be processed for the
#   time intervals slabel1 and slabel2!
#  Use 'TOTAL' as slabel to refer to the entire experiment time line.
#  The script contains all variables which are  possible to process
#   for your climate model. If no TimeSlice setting can be found for one of those variables,
#    it will be processed for the entire experiment timeline!
#
# GridAndTimeSlice: 'Field:Key=grid:glabel1|slabel1,glabel1|slabel2,glabel2|slabel
#   with glabel being grid labels as defined in the CMIP6-DataRequest (1deg, 2deg, ...).
#  Settings refering to the 'native' grid should be specified as TimeSlice-Settings.
#  If no GridAndTimeSlice-Setting can be found, the variable will not be processed.
#
# Priority of Settings:
# - TimeSlice and GridAndTimeSlice specify WHEN a variable is processed.
#   if Standard-Settings do not turn off the processing at all!
# - Standard: USERSETTINGS have priority over DREQSETTINGS.
# - TimeSlice/Grid: USERSETTINGS will be appended to DREQSETTINGS.
# Examples:
# + TimeSlice/GridAndTimeSlice may say 'No Processing',
#   also when Standard says 'Process'.
# + Standard may say 'No Processing', also when TimeSlice/Grid say 'Process'.
# + User may say 'Process' or 'No Processing' when Dreq says the opposite.
# + User says: timeslice1, Dreq says: timeslice2.
#   Thus variable(s) will be processed for both slices!
#
# Priority inside of Settings:
# - Priority Order is (high to low): GRID VAR VARBLOCK MIPTABLE REALISATION
# - Settings will be searched through by field and then by key,
#   starting from the highest priority.
# - The first matching setting will be applied!
# Examples:
# + 'Grid:Varblock=False' has higher priority than 'Varblock:Grid=True'
# + 'Varblock:Var=True' has higher priority than 'Varblock:Realisation=False'
# + 'Var:MIPtable=False' has higher priority than 'Varblock:MIPtable=True'
#
# Format:
# -Use format for settings as specified above.
# -TimeSlice- and Grid-labels may not contain blanks!
# -You may use blanks/tabs to format the settings,
#  BUT the timeslice and grid identifier have to be 'grid:' and 'slice:'
#  and not 'grid :', for example.
# -The automatic update routine of the DREQSETTINGS overwrites  everything between
#  the line #DREQSETTINGS and the line #USERSETTINGS
#  (or the next Experiment specification, if no USERSETTINGS are specified).
# -Use '#' for comments (also inline).
# ##################################################


# Supported MIPs: CORDEX


##################################################
EXP=AllExp
##################################################

DREQSETTINGS
fx            : fx            = slice: first_step
Ofx           : Ofx           = slice: first_step

USERSETTINGS
# ---> Specify your settings for Experiment TOTAL here
#<---- Specify your settings for Experiment TOTAL here
